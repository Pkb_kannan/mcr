package com.mcr.active.ui.fragment.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.mcr.active.ConnectionLiveData
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.databinding.LoginBinding
import com.mcr.active.model.ConnectionModel
import com.orhanobut.hawk.Hawk


class Login : Fragment() {

    var binding: LoginBinding? = null
    var viewmodel: LoginViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Util?.hawk(this.context!!)
        Hawk.put("url", "imin")
//        Log.e("url0", Hawk.get("url","imin"))
        viewmodel = ViewModelProviders.of(activity!!).get(LoginViewModel::class.java)
        var connectionLiveData = context?.let { ConnectionLiveData(it) }
        connectionLiveData?.observe(this,
            Observer<ConnectionModel> { connection ->
                if (connection.isConnected) {
                    when (connection.type) {
                        0 -> {
                            Toast.makeText(
                                context,
                                String.format("Connection turned OFF"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else -> {
                            // wifi=1  mobile=2
//                            Toast.makeText(
//                                context,
//                                String.format("Connection Available"),
//                                Toast.LENGTH_SHORT
//                            ).show()
                            // Do some code here
                        }
                    }
                } else {

                    Toast.makeText(
                        context,
                        String.format("Connection Error"),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })


    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.login, container, false)
        binding?.viewModel = viewmodel

        binding?.txtForgotPass?.setOnClickListener {
            Navigation.findNavController(requireView()).navigate(R.id.forgotpass)
        }

        initObservables()
        return binding?.root

    }


    private fun initObservables() {
        viewmodel?.launchMainActivity?.observe(this, Observer {
            if (it!!) {
                Hawk.put("userData", viewmodel?.userData)
                Hawk.put("LoginStatus", true)
                Hawk.put("FirstLogin", true)
                Hawk.put("Kudos", viewmodel?.userData?.totalKudos.toString())
                Hawk.put("profil_pic", viewmodel?.userData?.profiPic)
//                Navigation.findNavController(requireView()).navigate(R.id.nav_google_fit)
                Navigation.findNavController(requireView()).navigate(R.id.nav_home)

            }
        })
        binding?.txtSignUp?.setOnClickListener {
            Navigation.findNavController(requireView()).navigate(R.id.register)
        }
    }

//    private fun initObservables() {
//        viewmodel.validateCredentials.
//    }
}

