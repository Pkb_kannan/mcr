package com.mcr.active.model.favouritModel.favLeisureModel


import com.google.gson.annotations.SerializedName


data class FavLeisureListModel(

	@field:SerializedName("offers")
	val offers: List<OffersItem?>? = null,

	@field:SerializedName("identifier")
	val identifier: String? = null,

	@field:SerializedName("imin:aggregateOffer")
	val iminAggregateOffer: IminAggregateOffer? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("@context")
	val context: List<String?>? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("provider")
	val provider: Provider? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("beta:offerValidityPeriod")
	val betaOfferValidityPeriod: String? = null,

	@field:SerializedName("hoursAvailable")
	val hoursAvailable: List<HoursAvailableItem?>? = null,

	@field:SerializedName("location")
	val location: Location? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("category")
	val category: List<String?>? = null,

	@field:SerializedName("event")
	val event: List<EventItem?>? = null,

	@field:SerializedName("imin:dataSource")
	val iminDataSource: IminDataSource? = null
)