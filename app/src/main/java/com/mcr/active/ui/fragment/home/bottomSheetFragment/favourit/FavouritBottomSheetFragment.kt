package com.mcr.active.ui.fragment.home.bottomSheetFragment.favourit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mcr.active.R
import com.mcr.active.databinding.FavouritBootomSheetBinding
import com.mcr.active.ui.fragment.home.HomeViewmodel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.favourit.FavouritViewModel

class FavouritBottomSheetFragment : BottomSheetDialogFragment() {

    private var fragmentView: View? = null
    var viewmodel: FavouritViewModel? = null
    var binding: FavouritBootomSheetBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CoffeeDialog)
        viewmodel = ViewModelProviders.of(activity!!).get(FavouritViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding  = DataBindingUtil.inflate(inflater,R.layout.favourit_bootom_sheet,container,false)

        binding?.viewModel = viewmodel
        initObservables()
        return binding?.getRoot()
    }

    private fun initObservables() {
        binding?.btLocation?.setOnClickListener(View.OnClickListener {

            dismiss()
            fragmentManager?.let { it1 -> viewmodel?.showLocationDialogFragment(it1) }

        })
        binding?.btSearch?.setOnClickListener(View.OnClickListener {
            dismiss()
            fragmentManager?.let { it1 -> viewmodel?.showSearchDialogFragment(it1) }
        })
        binding?.btStar?.setOnClickListener(View.OnClickListener {
            dismiss()

        })
    }

}