package com.mcr.active.model.LocationDetailModel


import com.google.gson.annotations.SerializedName


data class BetaSportsActivityLocationItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)