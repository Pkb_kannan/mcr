package com.mcr.active.model.facilityModel


import com.google.gson.annotations.SerializedName


data class Location(

	@field:SerializedName("geo")
	val geo: Geo? = null,

	@field:SerializedName("address")
	val address: Address? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("telephone")
	val telephone: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("url")
	val url: String? = null


)