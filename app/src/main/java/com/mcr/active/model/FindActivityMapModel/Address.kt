package com.mcr.active.model.FindActivityMapModel


import com.google.gson.annotations.SerializedName


data class Address(

	@field:SerializedName("addressCountry")
	val addressCountry: String? = null,

	@field:SerializedName("imin:fullAddress")
	val iminFullAddress: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)