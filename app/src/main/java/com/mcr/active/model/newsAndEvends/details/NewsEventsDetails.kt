package com.mcr.active.model.newsAndEvends.details


import com.google.gson.annotations.SerializedName


data class NewsEventsDetails(

	@field:SerializedName("news")
	val news: News? = null,

	@field:SerializedName("news_archives")
	val newsArchives: List<NewsArchivesItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)