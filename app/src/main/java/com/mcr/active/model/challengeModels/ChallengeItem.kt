package com.mcr.active.model.challengeModels

import com.google.gson.annotations.SerializedName


data class ChallengeItem(

	@field:SerializedName("challenge_duration")
	val challengeDuration: String? = null,

	@field:SerializedName("number_of_tasks")
	val numberOfTasks: String? = null,

	@field:SerializedName("challenge_id")
	val challengeId: String? = null,

	@field:SerializedName("create_datetime")
	val createDatetime: String? = null,

	@field:SerializedName("challenge_name")
	val challengeName: String? = null,

	@field:SerializedName("task_required")
	val taskRequired: String? = null,

	@field:SerializedName("challenge_level")
	val challengeLevel: String? = null,

	@field:SerializedName("task_list")
	val taskList: List<TaskListItem?>? = null,

	@field:SerializedName("client_id")
	val clientId: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)