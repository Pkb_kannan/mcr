package com.mcr.active.ui.fragment.membership

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.mcr.active.R

class MembershipCard : Fragment() {

    companion object {
        fun newInstance() = MembershipCard()
    }


    private lateinit var viewModel: MembershipCardViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.membership_card, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MembershipCardViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
