package com.mcr.active.model.favouritModel.favLeisureModel


import com.google.gson.annotations.SerializedName


data class OffersItem(

	@field:SerializedName("priceCurrency")
	val priceCurrency: String? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("type")
	val type: String? = null
)