package com.mcr.active.model.EventDetailsModel


import com.google.gson.annotations.SerializedName


data class SubEvent1Item(

	@field:SerializedName("duration")
	val duration: String? = null,

	@field:SerializedName("endDate")
	val endDate: String? = null,

	@field:SerializedName("maximumAttendeeCapacity")
	val maximumAttendeeCapacity: Int? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("remainingAttendeeCapacity")
	val remainingAttendeeCapacity: Int? = null,

	@field:SerializedName("startDate")
	val startDate: String? = null,

	@field:SerializedName("imin:checkoutUrlTemplate")
	val iminCheckoutUrlTemplate: String? = null
)