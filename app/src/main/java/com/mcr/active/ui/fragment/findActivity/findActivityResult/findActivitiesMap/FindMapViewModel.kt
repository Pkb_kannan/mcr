package com.mcr.active.ui.fragment.findActivity.findActivityResult.findActivitiesMap

import android.app.Application
import android.location.Geocoder
import android.util.Log
import android.view.View
import androidx.databinding.ObservableInt
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.model.FindActivityMapModel.FindMapModel
import com.mcr.active.model.FindActivityMapModel.IminItemItem
import com.mcr.active.network.Api
import com.orhanobut.hawk.Hawk
import com.mcr.active.base.BaseViewModel
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import javax.inject.Inject


class FindMapViewModel(application: Application) : BaseViewModel(application) {

    @Inject
    lateinit var api: Api
    var loadComplect: SingleLiveEvent<Boolean>? = null
    var filterList = ArrayList<HashMap<String, Any>>()
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    var filterMap: HashMap<String, Any> = HashMap<String, Any>()
    private var LocationiList: List<IminItemItem> = ArrayList<IminItemItem>()

    init {
        loadComplect = SingleLiveEvent<Boolean>()
        loadComplect?.value = false

    }

    fun positions(): List<IminItemItem> {
        return LocationiList
    }

    fun search() {
        loadComplect?.value = false
        var selectedItem: ArrayList<String>? = ArrayList()
        var call: Call<FindMapModel>
        val list = filterMap["timeRange"].toString().split("-")
        isProgress.set(View.VISIBLE)
        selectedItem = filterMap["days"] as ArrayList<String>?

        if (!filterMap["sportName"].toString()?.equals("") && selectedItem?.size != 0 && !filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && !filterMap["timeRange"].toString().equals("TIME")
        ) {

            if (filterMap["actvityType"].toString()?.equals("Male Only Sessions") || filterMap["actvityType"].toString()?.equals(
                    "Female Only Sessions"
                )
            ) {
                var genter = ""
                if (filterMap["actvityType"].toString()?.equals("Male Only Sessions")) {
                    genter = "oa:MaleOnly"
                } else {
                    genter = "oa:FemaleOnly"
                }
                call = api.getPlaceLocationFilteronlygenter(
                    filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()),
                    "1",
                    "20",
                    list[0].replace(" ", ""),
                    list[1].replace(" ", ""),
                    selectedItem!!,
                    filterMap["sportName"].toString(),
                    genter,
                    Util.getIminToken()
                )

            } else {


                call = api.getPlaceLocationFilteronlystarttime(
                    filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()),
                    "1",
                    "20",
                    list[0].replace(" ", ""),
                    list[1].replace(" ", ""),
                    selectedItem!!,
                    filterMap["sportName"].toString(), Util.getIminToken()
                )
            }
        } else if (!filterMap["sportName"].toString()?.equals("") && selectedItem?.size == 0 && filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && filterMap["timeRange"].toString().equals("TIME")
        ) {
            call = api.getPlaceLocationFilterUrl(
                Util.getIminUrl() + "geo[radial]=" +
                        filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                        "&page=1&limit=20&activityPrefLabel=" +
                        filterMap["sportName"].toString(),
                Util.getIminToken()
            )
        } else if (!filterMap["sportName"].toString()?.equals("") && selectedItem?.size != 0 && filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && filterMap["timeRange"].toString().equals("TIME")
        ) {
            call = api.getPlaceLocationFilterdayandactiviy(
                filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()),
                "1",
                "20",
                selectedItem!!,
                filterMap["sportName"].toString(),
                Util.getIminToken()
            )
        } else if (!filterMap["sportName"].toString()?.equals("") && selectedItem?.size != 0 && !filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && filterMap["timeRange"].toString().equals("TIME")
        ) {
            var genter = ""
            if (filterMap["actvityType"].toString()?.equals("Male Only Sessions")) {
                genter = "oa:MaleOnly"
            } else {
                genter = "oa:FemaleOnly"
            }
            call = api.getPlc_day_activiy_actTyp_ma_fe(
                filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()),
                "1",
                "20",
                selectedItem!!,
                filterMap["sportName"].toString(),
                genter,
                Util.getIminToken()
            )
        } else if (!filterMap["sportName"].toString()?.equals("") && selectedItem?.size != 0 && filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && !filterMap["timeRange"].toString().equals("TIME")
        ) {
            call = api.getPlc_day_activiy_time(
                filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()),
                "1",
                "20",
                selectedItem!!,
                filterMap["sportName"].toString(),
                list[0].replace(" ", ""),
                list[1].replace(" ", ""),
                Util.getIminToken()
            )
        } else if (filterMap["sportName"].toString()?.equals("") && selectedItem?.size != 0 && filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && filterMap["timeRange"].toString().equals("TIME")
        ) {
            call = api.getPlc_day(
                filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()),
                "1",
                "20",
                selectedItem!!,
                Util.getIminToken()
            )
        } else if (filterMap["sportName"].toString()?.equals("") && selectedItem?.size == 0 && !filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && filterMap["timeRange"].toString().equals("TIME")
        ) {

//            Log.e("activityType","."+filterMap["actvityType"].toString())
            var genter = ""
            if (filterMap["actvityType"].toString()?.equals("Male Only Sessions") || filterMap["actvityType"].toString()?.equals(
                    "Female Only Sessions"
                )
            ) {

                if (filterMap["actvityType"].toString()?.equals("Male Only Sessions")) {
                    genter = "oa:MaleOnly"
                } else {
                    genter = "oa:FemaleOnly"
                }
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&genderRestriction=" +
                            genter,
                    Util.getIminToken()
                )
            } else if (filterMap["actvityType"].toString()?.equals("Sessions for Beginners")) {
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&levelType=imin:BeginnerLevel",
                    Util.getIminToken()
                )
            } else if (filterMap["actvityType"].toString()?.equals("Junior Sessions")) {
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&ageRange[includeRange]=15,20",
                    Util.getIminToken()
                )
            } else {
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&ageRange[includeRange]=15,20",
                    Util.getIminToken()
                )
            }
        }
        //*********************************ActivityType Time*************************************
        else if (filterMap["sportName"].toString()?.equals("") && selectedItem?.size == 0 && !filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && !filterMap["timeRange"].toString().equals("TIME")
        ) {

//            Log.e("activityType","."+filterMap["actvityType"].toString())
            var genter = ""
            if (filterMap["actvityType"].toString()?.equals("Male Only Sessions") || filterMap["actvityType"].toString()?.equals(
                    "Female Only Sessions"
                )
            ) {

                if (filterMap["actvityType"].toString()?.equals("Male Only Sessions")) {
                    genter = "oa:MaleOnly"
                } else {
                    genter = "oa:FemaleOnly"
                }
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&genderRestriction=" +
                            genter + "&startTime[gte]=" +
                            list[0].replace(" ", "") +
                            "&startTime[lte]=" + list[1].replace(" ", ""),
                    Util.getIminToken()
                )
            } else if (filterMap["actvityType"].toString()?.equals("Sessions for Beginners")) {
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&levelType=imin:BeginnerLevel&startTime[gte]=" +
                            list[0].replace(" ", "") +
                            "&startTime[lte]=" + list[1].replace(" ", ""),
                    Util.getIminToken()
                )
            } else if (filterMap["actvityType"].toString()?.equals("Junior Sessions")) {
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&ageRange[includeRange]=15,20&startTime[gte]=" +
                            list[0].replace(" ", "") +
                            "&startTime[lte]=" + list[1].replace(" ", ""),
                    Util.getIminToken()
                )
            } else {
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&ageRange[includeRange]=15,20&startTime[gte]=" +
                            list[0].replace(" ", "") +
                            "&startTime[lte]=" + list[1].replace(" ", ""),
                    Util.getIminToken()
                )
            }
        }

        //*********************************ActivityName Time*************************************
        else if (!filterMap["sportName"].toString()?.equals("") && selectedItem?.size == 0 && filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && !filterMap["timeRange"].toString().equals("TIME")
        ) {

            call = api.getPlaceLocationFilterUrl(
                Util.getIminUrl() + "geo[radial]=" +
                        filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                        "&page=1&limit=20" +
                        "&activityPrefLabel=" + filterMap["sportName"].toString() +
                        "&startTime[gte]=" + list[0].replace(" ", "") +
                        "&startTime[lte]=" + list[1].replace(" ", ""),
                Util.getIminToken()
            )
        }

        //*********************************ActivityName ActivityType Time*************************************
        else if (!filterMap["sportName"].toString()?.equals("") && selectedItem?.size == 0 && !filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && !filterMap["timeRange"].toString().equals("TIME")
        ) {

//            Log.e("activityType","."+filterMap["actvityType"].toString())
            var genter = ""
            if (filterMap["actvityType"].toString()?.equals("Male Only Sessions") || filterMap["actvityType"].toString()?.equals(
                    "Female Only Sessions"
                )
            ) {

                if (filterMap["actvityType"].toString()?.equals("Male Only Sessions")) {
                    genter = "oa:MaleOnly"
                } else {
                    genter = "oa:FemaleOnly"
                }
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&genderRestriction=" + genter +
                            "&activityPrefLabel=" + filterMap["sportName"].toString() +
                            "&startTime[gte]=" + list[0].replace(" ", "") +
                            "&startTime[lte]=" + list[1].replace(" ", ""),
                    Util.getIminToken()
                )
            } else if (filterMap["actvityType"].toString()?.equals("Sessions for Beginners")) {
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&levelType=imin:BeginnerLevel" +
                            "&activityPrefLabel=" + filterMap["sportName"].toString() +
                            "&startTime[gte]=" + list[0].replace(" ", "") +
                            "&startTime[lte]=" + list[1].replace(" ", ""),
                    Util.getIminToken()
                )
            } else if (filterMap["actvityType"].toString()?.equals("Junior Sessions")) {
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&ageRange[includeRange]=15,20" +
                            "&activityPrefLabel=" + filterMap["sportName"].toString() +
                            "&startTime[gte]=" + list[0].replace(" ", "") +
                            "&startTime[lte]=" + list[1].replace(" ", ""),
                    Util.getIminToken()
                )
            } else {
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&ageRange[includeRange]=15,20" +
                            "&activityPrefLabel=" + filterMap["sportName"].toString() +
                            "&startTime[gte]=" + list[0].replace(" ", "") +
                            "&startTime[lte]=" + list[1].replace(" ", ""),
                    Util.getIminToken()
                )
            }
        }
        //*********************************ActivityName ActivityType *************************************
        else if (!filterMap["sportName"].toString()?.equals("") && selectedItem?.size == 0 && !filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && filterMap["timeRange"].toString().equals("TIME")
        ) {

//            Log.e("activityType","."+filterMap["actvityType"].toString())
            var genter = ""
            if (filterMap["actvityType"].toString()?.equals("Male Only Sessions") || filterMap["actvityType"].toString()?.equals(
                    "Female Only Sessions"
                )
            ) {

                if (filterMap["actvityType"].toString()?.equals("Male Only Sessions")) {
                    genter = "oa:MaleOnly"
                } else {
                    genter = "oa:FemaleOnly"
                }
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&genderRestriction=" + genter +
                            "&activityPrefLabel=" + filterMap["sportName"].toString(),
                    Util.getIminToken()
                )
            } else if (filterMap["actvityType"].toString()?.equals("Sessions for Beginners")) {
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&levelType=imin:BeginnerLevel" +
                            "&activityPrefLabel=" + filterMap["sportName"].toString(),
                    Util.getIminToken()
                )
            } else if (filterMap["actvityType"].toString()?.equals("Junior Sessions")) {
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&ageRange[includeRange]=15,20" +
                            "&activityPrefLabel=" + filterMap["sportName"].toString(),
                    Util.getIminToken()
                )
            } else {
                call = api.getPlaceLocationFilterUrl(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&ageRange[includeRange]=15,20" +
                            "&activityPrefLabel=" + filterMap["sportName"].toString(),
                    Util.getIminToken()
                )
            }
        }
        //*************day Time ActivityType****************************************************
        else if (filterMap["sportName"].toString()?.equals("") && selectedItem?.size != 0 && !filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && !filterMap["timeRange"].toString().equals("TIME")
        ) {

//            Log.e("activityType","."+filterMap["actvityType"].toString())
            var genter = ""
            if (filterMap["actvityType"].toString()?.equals("Male Only Sessions") || filterMap["actvityType"].toString()?.equals(
                    "Female Only Sessions"
                )
            ) {

                if (filterMap["actvityType"].toString()?.equals("Male Only Sessions")) {
                    genter = "oa:MaleOnly"
                } else {
                    genter = "oa:FemaleOnly"
                }
                call = api.getPlc_day_actTyp_time_level1(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&genderRestriction=" + genter +
                            "&startTime[gte]=" + list[0].replace(" ", "") +
                            "&startTime[lte]=" + list[1].replace(" ", ""),
                    selectedItem!!,
                    Util.getIminToken()
                )
            } else if (filterMap["actvityType"].toString()?.equals("Sessions for Beginners")) {
                call = api.getPlc_day_actTyp_time_level1(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&levelType=imin:BeginnerLevel&startTime[gte]=" +
                            list[0].replace(" ", "") +
                            "&startTime[lte]=" + list[1].replace(" ", ""),
                    selectedItem!!,
                    Util.getIminToken()
                )
            } else if (filterMap["actvityType"].toString()?.equals("Junior Sessions")) {
                call = api.getPlc_day_actTyp_time_level1(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&ageRange[includeRange]=15,20&startTime[gte]=" +
                            list[0].replace(" ", "") +
                            "&startTime[lte]=" + list[1].replace(" ", ""),
                    selectedItem!!,
                    Util.getIminToken()
                )
            } else {
                call = api.getPlc_day_actTyp_time_level1(
                    Util.getIminUrl() + "geo[radial]=" +
                            filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                            "&page=1&limit=20&ageRange[includeRange]=15,20&startTime[gte]=" +
                            list[0].replace(" ", "") +
                            "&startTime[lte]=" + list[1].replace(" ", ""),
                    selectedItem!!,
                    Util.getIminToken()
                )
            }
        } else if (filterMap["sportName"].toString()?.equals("") && selectedItem?.size == 0 && filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && !filterMap["timeRange"].toString().equals("TIME")
        ) {

            call = api.getPlaceLocationFilterUrl(
                Util.getIminUrl() + "geo[radial]=" +
                        filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) +
                        "&page=1&limit=20&startTime[gte]=" +
                        list[0].replace(" ", "") +
                        "&startTime[lte]=" + list[1].replace(" ", ""),
                Util.getIminToken()
            )
        } else if (filterMap["sportName"].toString()?.equals("") && selectedItem?.size != 0 && filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && !filterMap["timeRange"].toString().equals("TIME")
        ) {

//
            call = api.getPlc_day_time(
                filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()),
                "1",
                "20",
                selectedItem!!,
                list[0].replace(" ", ""),
                list[1].replace(" ", ""),
                Util.getIminToken()
            )
        } else
            call = api.getPlaceLocationFilterUrl(
                Util.getIminUrl() + "geo[radial]=" +
                        filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()) + "&page=1&limit=20",
                Util.getIminToken()
            )
        call.enqueue(object : retrofit2.Callback<FindMapModel> {
            override fun onFailure(call: Call<FindMapModel>?, t: Throwable?) {
                Log.e("response", "fail")
                isProgress.set(View.GONE)
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(
                call: Call<FindMapModel>?,
                response: Response<FindMapModel>?
            ) {

//                val gson = Gson()
//                val jsonElement: JsonElement = gson.toJsonTree(response?.body())
//                Log.e("list_value", jsonElement.toString())
//                val respnsEvents = gson.fromJson(jsonElement, FindMapModel::class.java)

                LocationiList = response?.body()?.iminItem!!
//                Log.e("list_value1", LocationiList.get(0)?.geo?.latitude.toString())
                loadComplect?.value = true
                isProgress.set(View.GONE)
            }

        })
    }
}