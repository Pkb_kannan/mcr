package com.mcr.active.model.challengeModels

import com.google.gson.annotations.SerializedName

data class ChallengeResp (
    @field:SerializedName("data")
    val data: List<ChallengeItem?>? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Boolean? = null
)
