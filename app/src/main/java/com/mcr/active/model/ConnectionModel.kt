package com.mcr.active.model

class ConnectionModel(val type: Int, val isConnected: Boolean)