package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.timeTable

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.databinding.LocationTimeItemBinding
import com.mcr.active.model.EventDetailsModel.IminItemItemDetails
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.LocationWhiteViewModel


class TimeTableAdapter(private val viewModel: LocationWhiteViewModel,
                       private var items: List<IminItemItemDetails>
)
    : RecyclerView.Adapter<TimeTableAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = LocationTimeItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(
            binding
        )
    }

//    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemCount(): Int = items.size




    fun setTimeTable(noti: List<IminItemItemDetails>) {
        this.items = noti
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: LocationTimeItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: LocationWhiteViewModel?, position: Int?
        ) { // viewModel.fetchDogBreedImagesAt(position);
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

