package com.mcr.active.ui.fragment.fitnessApps

import android.content.Context
import android.view.LayoutInflater
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mcr.active.databinding.MyAppsBinding
import com.mcr.active.model.getApps.AppDetailsItem

class MyLinkedAppsAdapter(
    var context: Context?,
    var applist: List<AppDetailsItem>,
    var viewmodel: MyAppsViewModel?
):
    RecyclerView.Adapter<MyLinkedAppsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding  = MyAppsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)

    }

    override fun getItemCount(): Int = applist.size

    override fun onBindViewHolder(holder: MyLinkedAppsAdapter.ViewHolder, position: Int) {
        holder.bind(viewmodel, position)
//        holder.binding.btLink.text= applist.get(position).name
        Glide.with(context!!).load(applist.get(position).logo).into(holder.binding.imageApps)
//        holder.binding.dwlCloud.visibility=VISIBLE

    }



    class ViewHolder(val binding: MyAppsBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: MyAppsViewModel?,
            position1: Int
        ) {
            binding.setVariable(BR.viewModel, data) //BR - generated class; BR.viewModel - 'viewModel' is variable name declared in layout
            binding.setVariable(BR.position, position1) //BR - generated class; BR.viewModel - 'viewModel' is variable name declared in layout
            binding.executePendingBindings()
        }
    }
}