package com.mcr.active.ui.fragment.home.bottomSheetFragment.location

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mcr.active.R
import com.mcr.active.databinding.LocationInfoBottomSheetBinding
import com.mcr.active.ui.fragment.home.HomeViewmodel

class InfoBottomSheetFragment : BottomSheetDialogFragment() {

    var viewmodel: HomeViewmodel? = null
    var binding: LocationInfoBottomSheetBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CoffeeDialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        binding  = DataBindingUtil.inflate(inflater,R.layout.location_info_bottom_sheet,container,false)
//        binding?.viewModel = viewmodel
        initObservables()
        return binding?.getRoot()
    }


    private fun initObservables() {
        binding?.imgClose?.setOnClickListener(View.OnClickListener {
            dismiss()
        })
    }


}