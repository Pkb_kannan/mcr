package com.mcr.active.injection.module

import android.util.Log
import com.mcr.active.MCRApp
import com.mcr.active.network.Api
import com.orhanobut.hawk.Hawk
import com.orhanobut.hawk.HawkBuilder
import com.orhanobut.hawk.LogLevel
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Module which provides all required dependencies about network
 */
@Module
// Safe here as we are dealing with a Dagger 2 module
@Suppress("unused")
object NetworkModule {
    private const val IMIN_BASE_URL = "https://search.imin.co/"
    private const val IMIN_FACILITY_URL = "https://search.imin.co/"
    private const val MCR_URL = "http://192.168.1.100/kinetic_3/index.php/api/mcr_api/member/"
//    private const val MCR_URL = "https://www.kineticinsightpro.com/index.php/api/mcr_api/member/"
    private const val MAP_URL = "https://maps.googleapis.com/maps/api/geocode/json"
    var BASE_URL = "https://www.kineticinsightpro.com/index.php/api/mcr_api/member/"

//    private const val BASE_URL = "https://jsonplaceholder.typicode.com"
    /**
     * Provides the Post service implementation.
     * @param retrofit the Retrofit object used to instantiate the service
     * @return the Post service implementation.
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun providePostApi(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRetrofitInterface(): Retrofit {
        Hawk.init(MCRApp.instance)
            .setEncryptionMethod(HawkBuilder.EncryptionMethod.MEDIUM)
            .setStorage(HawkBuilder.newSqliteStorage(MCRApp.instance))
            .setLogLevel(LogLevel.FULL)
            .build()

        if(Hawk.get("url","normal").equals("normal")) {
            BASE_URL = MCR_URL
            Log.e("url1",Hawk.get("url","imin"))
        }
        else if (Hawk.get("url","normal").equals("map")){
            BASE_URL = MAP_URL
        }
        else{
            BASE_URL = IMIN_BASE_URL
            Log.e("url2",Hawk.get("url","imin"))
        }
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(client)
                .build()
    }
}