package com.mcr.active.model.findActivityListModel


import com.google.gson.annotations.SerializedName


data class FindActivityListModel(

	@field:SerializedName("imin:item")
	val iminItem: List<IminItemItem>? = null
)