package com.mcr.active.model.ethnicityModel


import com.google.gson.annotations.SerializedName


data class DataItem(

	@field:SerializedName("e_name")
	val eName: String? = null,

	@field:SerializedName("e_id")
	val eId: String? = null
)