package com.mcr.active.model.LocationDetailModel


import com.google.gson.annotations.SerializedName


data class PublicAdult(

	@field:SerializedName("priceCurrency")
	val priceCurrency: String? = null,

	@field:SerializedName("price")
	val price: Double? = null,

	@field:SerializedName("imin:membershipRequired")
	val iminMembershipRequired: Boolean? = null,

	@field:SerializedName("type")
	val type: String? = null
)