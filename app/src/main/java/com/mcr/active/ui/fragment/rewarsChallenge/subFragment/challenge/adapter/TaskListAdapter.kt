package com.mcr.active.ui.fragment.rewarsChallenge.subFragment.challenge.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.databinding.TaskItemBinding
import com.mcr.active.model.challengeModels.TaskListItem
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.challenge.ChallengeSubViewModel
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener



class TaskListAdapter (
    var context: Context?,
    var taskLists: List<TaskListItem>,
    var  viewmodel: ChallengeSubViewModel?

) : RecyclerView.Adapter<TaskListAdapter.ViewHolder>() {
    // Inflating Layout and ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding  = TaskItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = taskLists.size

    // Bind data
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewmodel, position)
        holder.binding.seekBar.setOnTouchListener(object : OnTouchListener {
            @SuppressLint("ClickableViewAccessibility")
            override fun onTouch(@SuppressLint("ClickableViewAccessibility") v: View, event: MotionEvent): Boolean {
                return true
            }
        })
    }

    // Creating ViewHolder
    class ViewHolder(val binding: TaskItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: ChallengeSubViewModel?,
            position1: Int
        ) {
            binding.setVariable(BR.viewModel, data) //BR - generated class; BR.viewModel - 'viewModel' is variable name declared in layout
            binding.setVariable(BR.position, position1) //BR - generated class; BR.viewModel - 'viewModel' is variable name declared in layout
            binding.executePendingBindings()
        }
    }

}