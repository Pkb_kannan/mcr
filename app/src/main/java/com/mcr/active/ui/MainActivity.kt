package com.mcr.active.ui

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.mcr.active.MCRApp
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.databinding.ActivityMain1Binding
import com.mcr.active.databinding.SignUpPopupBinding
import com.mcr.active.ui.fragment.fitnessApps.MyApps
import com.orhanobut.hawk.Hawk
import com.uwanttolearn.easysocial.EasySocialAuthActivity
import kotlinx.android.synthetic.main.activity_main1.*
import kotlinx.android.synthetic.main.slid_menu.*

class MainActivity : AppCompatActivity() {

    var page : String? = null
    var flage = 0
    var viewmodel: MainViewModel? = null
    var binding: ActivityMain1Binding? = null
    var binding1: SignUpPopupBinding? = null
    lateinit var transaction:FragmentTransaction


    companion object {
        var navController : NavController? = null
            private set


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main1)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main1)
        viewmodel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        binding?.viewModel = viewmodel
         page = intent.getStringExtra("page")

        setupNavigation()
        initObservables()

    }
    private fun initObservables() {
        Util.hawk(this)
        viewmodel?.TopHide?.observe(this, Observer {
            if (it!!) binding?.lenAppBar?.visibility=View.VISIBLE
            else{
                binding?.lenAppBar?.visibility=View.GONE
            }
        })

        binding?.imgMenu?.setOnClickListener(View.OnClickListener {

            drawerOPen()
        })
        binding?.sidMemuInclud?.imgSidMeu?.setOnClickListener(View.OnClickListener {
            drawerOPen()
        })

        binding?.sidMemuInclud?.lenNav?.setOnClickListener(View.OnClickListener {
            drawerOPen()
        })
        binding?.sidMemuInclud?.btSingin?.setOnClickListener {
            navController?.navigate(R.id.nav_login)
            drawerOPen()
        }
        binding?.sidMemuInclud?.navView2?.setOnClickListener {
            drawerOPen()
        }


        binding?.sidMemuInclud?.navView?.setNavigationItemSelectedListener { menuItem ->
            menuItem.isChecked = true
            drawerOPen()
            when(menuItem.itemId) {

                R.id.nav_home -> {
                    if (Hawk.get("LoginStatus", false)) {
                        show()
                        navController?.navigate(R.id.nav_home)
                    } else {
                        sidemenuHide(true)
                        binding?.lenAppBar?.visibility = View.GONE
                        navController?.navigate(R.id.nav_splash)
                    }
                }
                R.id.nav_location -> {
                    if (Hawk.get("LoginStatus", false)) {
                        show()
//                        navController?.navigate(R.id.nav_home)
                    }
                    binding?.lenAppBar?.visibility = View.VISIBLE
                    navController?.navigate(R.id.nav_location)
                }
                R.id.nav_member_ship ->
                    navController?.navigate(R.id.nav_member_ship)
                R.id.nav_reward ->
                    navController?.navigate(R.id.nav_reward)
                R.id.nav_fitness ->
                    navController?.navigate(R.id.nav_fitness)
                R.id.nav_newa ->
                    navController?.navigate(R.id.news_a_events)
                R.id.nav_log_out -> {
                    binding?.lenAppBar?.visibility = View.GONE
                    binding?.sidMemuInclud?.navView2?.visibility = View.VISIBLE
                    Hawk.put("userData",null)
                    Hawk.clear()
                    Hawk.put("Kudos",null)
                    Hawk.put("page","splash")
                    navController?.navigate(R.id.nav_splash)

            }

            }


            true
        }
        binding?.drawerLayout?.setOnClickListener {
            drawerOPen()
        }
        appBarHide()
        sidemenuHide(true)

    }
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode==2) {

//            val fragment = fragmentManager.findFragmentByTag(MyApps::class.java!!.getName())
//            fragment?.onActivityResult(requestCode, resultCode, data)
                val token = data!!.getStringExtra("data")
                Hawk.put("fitbiToken", token)
                MyApps.fitibit?.value = true
            }

        }
    }


    fun naviControler(): NavController? {
        return navController
    }
    fun appBarHide(){
        if(navController!!.currentDestination?.label?.equals("Splash")!!){
            binding?.lenAppBar?.visibility = View.GONE
            binding?.sidMemuInclud?.navView2?.visibility = View.VISIBLE
        }
        else if(navController!!.currentDestination?.label?.equals("Home")!!){
            binding?.lenAppBar?.visibility = View.VISIBLE
            binding?.sidMemuInclud?.navView2?.visibility = View.GONE
        }
    }
    fun getapplication(): MainActivity {
        return this
    }
    fun show()
    {
        binding?.lenAppBar?.visibility = View.VISIBLE
        binding?.sidMemuInclud?.navView2?.visibility = View.GONE
    }
    fun sidemenuHide(status : Boolean){
        if (status) {
            binding?.imgMenu?.visibility = View.VISIBLE
            binding?.redLine?.visibility = View.VISIBLE
        }
        else{
            binding?.imgMenu?.visibility = View.GONE
            binding?.redLine?.visibility = View.GONE
        }
    }

    fun drawerOPen(){
        if (drawer_layout.isDrawerOpen(GravityCompat.END)) {
            drawer_layout.closeDrawer(GravityCompat.END)
        }else{
            drawer_layout.openDrawer(GravityCompat.END)
        }
    }

//    override fun onSupportNavigateUp(): Boolean {
//        return NavigationUI.navigateUp(
//            Navigation.findNavController(this, R.id.nav_host_fragment), drawer_layout
//        )
//    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
//          supportFragmentManager.popBackStack()
        }

        val navigationController = nav_host_fragment.findNavController()
        if (navigationController.currentDestination?.id == R.id.register){
            sidemenuHide(true)
        }
        if (navigationController.currentDestination?.id == R.id.nav_home) {
            finishAffinity()
        }
        else if (navigationController.currentDestination?.id != R.id.nav_home) {
//            val count=supportFragmentManager.backStackEntryCount
            super.onBackPressed()
        }

        else if (navigationController.currentDestination?.id == R.id.nav_splash){

            finishAffinity()
        }
        else {
//            supportFragmentManager.popBackStack()
            super.onBackPressed()
        }

    }

    private fun setupNavigation() {

             navController = Navigation.findNavController(this, R.id.nav_host_fragment)

            nav_view.setNavigationItemSelectedListener { menuItem ->
                menuItem.isChecked = true
                drawer_layout.closeDrawer(GravityCompat.END)
                true
            }


            // Tie nav graph to items in nav drawer
            NavigationUI.setupWithNavController(nav_view, navController!!)

//        }
    }

    fun getFragment(fragment: Fragment) {
        transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.nav_host_fragment, fragment).commit()
    }

    fun signUp() {

        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        viewmodel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        binding1 = DataBindingUtil.inflate(
            LayoutInflater.from(dialog.context),
            R.layout.sign_up_popup,
            null,
            false
        )
        binding1?.root?.let { dialog.setContentView(it) }
//        dialog .setContentView(R.layout.home_dialog)
        binding1?.viewModel = viewmodel

        binding1?.signUp?.setOnClickListener {
            dialog.cancel()
            navController?.navigate(R.id.register)
        }

        dialog.show()
    }


}




