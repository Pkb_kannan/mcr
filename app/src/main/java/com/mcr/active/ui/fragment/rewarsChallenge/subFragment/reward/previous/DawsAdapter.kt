package com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.previous

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mcr.active.databinding.PreviousDrawsBinding
import com.mcr.active.model.PreviousDawsModel.PreviousDawsItem

class DawsAdapter(private val ctx: Context, private val viewModel: PreviousDrawsViewModel,
                  private var items: List<PreviousDawsItem>?
) : RecyclerView.Adapter<DawsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = PreviousDrawsBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        holder.bind(viewModel, position)
        Glide.with(ctx).load(items?.get(position)?.rewardsImage).into(holder.binding.imgImage)
        holder.binding.textSeek.setOnClickListener {}
    }

    override fun getItemCount(): Int = items?.size!!

    fun setNotification(noti: List<PreviousDawsItem>) {
        this.items = noti
        notifyDataSetChanged()
    }

     class ViewHolder( val binding: PreviousDrawsBinding) :
            RecyclerView.ViewHolder(binding.root) {
        fun bind(viewModel: PreviousDrawsViewModel?, position: Int?
        ) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

