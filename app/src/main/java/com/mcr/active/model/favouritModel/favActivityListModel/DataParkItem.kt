package com.mcr.active.model.favouritModel.favActivityListModel


import com.google.gson.annotations.SerializedName


data class DataParkItem(

	@field:SerializedName("member_id")
	val memberId: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("activity_url")
	val activityUrl: String? = null,

	@field:SerializedName("activity_type")
	val activityType: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)