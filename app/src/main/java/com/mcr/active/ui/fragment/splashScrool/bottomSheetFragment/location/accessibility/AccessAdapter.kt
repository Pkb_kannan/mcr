package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.accessibility

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.databinding.AccessibilityItemBinding
import com.mcr.active.databinding.DayItemTimeTableBinding
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.LocationWhiteViewModel
import kotlin.collections.ArrayList


class AccessAdapter(private val viewModel: LocationWhiteViewModel,
                    private var items: ArrayList<String>)
    : RecyclerView.Adapter<AccessAdapter.ViewHolder>() {

    var selectedItem : ArrayList<String>?= ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = AccessibilityItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(
            binding
        )
    }

//    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position,items,selectedItem)
    }

    override fun getItemCount(): Int = items.size




    fun setAccessAdapter(noti: ArrayList<String>) {
        this.items = noti
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: AccessibilityItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("ResourceAsColor")
        fun bind(
            viewModel: LocationWhiteViewModel?,
            position: Int?,
            items: ArrayList<String>,
            selectedItem: ArrayList<String>?
        ) { //            viewModel.fetchDogBreedImagesAt(position);

//            binding.texDay.setOnClickListener(View.OnClickListener {
//                if (selectedItem?.contains(items[position!!])!!){
//                    binding.texDay.setBackgroundResource(R.color.colorTimeBox)
////                    binding.texDay.setTextColor(R.color.colorBlack)
//                    selectedItem.remove(items[position!!])
//                }
//                else {
//                    binding.texDay.setBackgroundResource(R.color.colorRed)
////                    binding.texDay.setTextColor(R.color.colorWhtie)
//                    selectedItem.add(items[position!!])
//                }
//            })
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

