package com.mcr.active.ui.activity.strava

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.mcr.active.R
import com.mcr.active.databinding.ActivityStravaBinding
import com.mcr.active.ui.fragment.fitnessApps.MyAppsViewModel
import com.sweetzpot.stravazpot.authenticaton.api.ApprovalPrompt
import com.sweetzpot.stravazpot.authenticaton.api.AuthenticationAPI
import com.sweetzpot.stravazpot.authenticaton.api.StravaLogin
import com.sweetzpot.stravazpot.authenticaton.model.AppCredentials
import com.sweetzpot.stravazpot.authenticaton.ui.StravaLoginActivity
import com.sweetzpot.stravazpot.authenticaton.ui.StravaLoginButton
import com.sweetzpot.stravazpot.common.api.AuthenticationConfig


class StravaActivity : AppCompatActivity() {

    var binding: ActivityStravaBinding? = null
    var viewmodel: MyAppsViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_strava)
        viewmodel = ViewModelProviders.of(this).get(MyAppsViewModel::class.java)
        binding?.viewModel = viewmodel
//        setContentView(R.layout.activity_strava)

        if (android.os.Build.VERSION.SDK_INT > 9) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }
        login()
        val loginButton = findViewById<View>(R.id.login_button) as StravaLoginButton
        loginButton.setOnClickListener {
            login()
        }
    }

    private fun login() {
        val intent = StravaLogin.withContext(this)
            .withClientID(40180)
            .withRedirectURI("https://www.kineticinsightpro.com/index.php/api/stravadata/connect/22545474")
            .withApprovalPrompt(ApprovalPrompt.AUTO)
//            .withAccessScope(AccessScope.WRITE)
            .makeIntent()
        startActivityForResult(intent, RQ_LOGIN)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RQ_LOGIN && resultCode == RESULT_OK && data != null) {
            Log.d("Strava code", data.getStringExtra(StravaLoginActivity.RESULT_CODE))
            val code = data.getStringExtra(StravaLoginActivity.RESULT_CODE)
            val config = AuthenticationConfig.create()
                .debug()
                .build()
            val api = AuthenticationAPI(config)
            val result = api.getTokenForApp(AppCredentials.with(CLIENT_ID, CLIENT_SECRET))
                .withCode(code)
                .execute()


            val resultIntent = Intent()
            resultIntent.putExtra("token", result.token.toString().replace("Bearer ",""))
            resultIntent.putExtra("expiresAt", result.getexpiresAt().toString().replace("Bearer ",""))
            resultIntent.putExtra("expiresIn", result.getexpiresIn().toString().replace("Bearer ",""))
            resultIntent.putExtra("tokenType", result.gettokenType().toString().replace("Bearer ",""))
            resultIntent.putExtra("refreshToken", result.getrefreshToken().toString().replace("Bearer ",""))
            resultIntent.putExtra("code", code.toString())
            setResult(RESULT_OK, resultIntent)
            finish()

        }
    }
    companion object {
        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
        val RQ_LOGIN = 1001
        private val CLIENT_ID = 40180
        private val CLIENT_SECRET = "2190196088f7f71949bcb54b96662c9e094b52c5"
    }

}
