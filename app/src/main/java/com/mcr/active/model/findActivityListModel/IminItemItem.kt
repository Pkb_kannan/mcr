package com.mcr.active.model.findActivityListModel


import com.google.gson.annotations.SerializedName


data class IminItemItem(

	@field:SerializedName("identifier")
	val identifier: String? = null,

	@field:SerializedName("activity")
	val activity: List<ActivityItem?>? = null,

	@field:SerializedName("imin:aggregateOffer")
	val iminAggregateOffer: IminAggregateOffer? = null,

	@field:SerializedName("subEvent")
	val subEvent: List<SubEventItem?>? = null,

	@field:SerializedName("isCoached")
	val isCoached: Boolean? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("isAccessibleForFree")
	val isAccessibleForFree: Boolean? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("imin:locationSummary")
	val iminLocationSummary: List<IminLocationSummaryItem?>? = null,

	@field:SerializedName("organizer")
	val organizer: Organizer? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("imin:dataSource")
	val iminDataSource: IminDataSource? = null
)