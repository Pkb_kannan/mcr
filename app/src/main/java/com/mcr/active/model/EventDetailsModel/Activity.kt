package com.mcr.active.model.EventDetailsModel


import com.google.gson.annotations.SerializedName


data class Activity(

	@field:SerializedName("imin:totalItems")
	val iminTotalItems: Int? = null,

	@field:SerializedName("imin:source")
	val iminSource: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("imin:item")
	val iminItem1: List<IminItem1Item?>? = null
)