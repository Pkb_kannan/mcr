package com.mcr.active.model.favouritModel.favLeisureModel


import com.google.gson.annotations.SerializedName


data class Address(

	@field:SerializedName("addressCountry")
	val addressCountry: String? = null,

	@field:SerializedName("streetAddress")
	val streetAddress: String? = null,

	@field:SerializedName("imin:fullAddress")
	val iminFullAddress: String? = null,

	@field:SerializedName("postalCode")
	val postalCode: String? = null,

	@field:SerializedName("addressLocality")
	val addressLocality: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("addressRegion")
	val addressRegion: String? = null
)