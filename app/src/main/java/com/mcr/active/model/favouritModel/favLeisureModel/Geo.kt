package com.mcr.active.model.favouritModel.favLeisureModel


import com.google.gson.annotations.SerializedName


data class Geo(

	@field:SerializedName("latitude")
	val latitude: Double? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("longitude")
	val longitude: Double? = null
)