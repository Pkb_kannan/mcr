package com.mcr.active.model.earnKudosList


import com.google.gson.annotations.SerializedName


data class KudosDataItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("open_activity_id")
	val openActivityId: String? = null,

	@field:SerializedName("sports_id")
	val sportsId: String? = null,

	@field:SerializedName("activity_type")
	val activityType: String? = null,

	@field:SerializedName("earned_kudos")
	val earnedKudos: String? = null
)