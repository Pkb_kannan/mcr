package com.mcr.active.ui.fragment.forgotPassword

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.databinding.ForgotpassBinding
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_main1.*

class ForgotPassword : Fragment() {

    var viewmodel: FPViewModel?=null
    var binding: ForgotpassBinding?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Util?.hawk(this.context!!)
        Hawk.put("url","normal")
        Log.e("url0", Hawk.get("url","imin"))
        viewmodel=ViewModelProviders.of(activity!!).get(FPViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding  = DataBindingUtil.inflate(inflater, R.layout.forgotpass,container,false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.root

    }

    private fun initObservables() {

        viewmodel?.launchMainActivity?.observe(this, Observer {
            if (it!!) {
                Hawk.put("userData",viewmodel?.userData)
                Hawk.put("LoginStatus",true)
                Hawk.put("Kudos",viewmodel?.userData?.totalKudos.toString())
                val navigationController = nav_host_fragment.findNavController()
                if(navigationController.currentDestination?.id == R.id.forgotpass){
                    fragmentManager?.popBackStack()
                }
                Navigation.findNavController(requireView()).navigate(R.id.nav_login)

            }
        })
    }
}