package com.mcr.active.ui.fragment.forgotPassword

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.mcr.active.MCRApp
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.network.Api
import com.mcr.active.base.BaseViewModel
import retrofit2.Call
import retrofit2.Response
import java.util.regex.Pattern
import javax.inject.Inject

class FPViewModel(application: Application) : BaseViewModel(application) {

    @Inject
    lateinit var api: Api
    var userData: LoginModel = LoginModel()
    var btnSelected: ObservableBoolean? = null
    var email: ObservableField<String>? = null
    var launchMainActivity: SingleLiveEvent<Boolean>? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)

    init {
        email = ObservableField("")
        btnSelected = ObservableBoolean(false)
        launchMainActivity = SingleLiveEvent<Boolean>()
    }

    fun ForgotPass() {
        isProgress.set(View.VISIBLE)
        if (isEmailValid(email?.get()!!)) {
            getUser2()
        } else {
            Toast.makeText(MCRApp.instance, "Invalid Email", Toast.LENGTH_SHORT).show()
        }
    }

    private fun getUser2() {
        isProgress.set(View.VISIBLE)
        val call = api.forgotpass(Util.getBaseUrl()+"forgotPassword",email?.get()!!.toString())
        call?.enqueue(object : retrofit2.Callback<LoginModel> {
            override fun onFailure(call: Call<LoginModel>?, t: Throwable?) {
                Log.e("response", t?.message)
                Toast.makeText(getApplication(),"Try again",Toast.LENGTH_SHORT).show()
                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<LoginModel>?,
                response: Response<LoginModel>?
            ) {
                if (response?.body()?.status!!) {
                    userData = response?.body()!!
                    Toast.makeText(getApplication(), response?.body()?.message, Toast.LENGTH_SHORT)
                        .show()
                    isProgress.set(View.GONE)
                    launchMainActivity?.value = true
                } else {
                    Toast.makeText(getApplication(), response?.body()?.message, Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })
    }

    fun isEmailValid(email: String): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        return matcher.matches()


    }

}