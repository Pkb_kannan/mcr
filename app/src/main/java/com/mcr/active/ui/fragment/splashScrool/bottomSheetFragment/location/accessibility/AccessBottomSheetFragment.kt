package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.accessibility

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import com.mcr.active.MCRApp
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.Util.Util.Companion.getDistance
import com.mcr.active.databinding.AccesibilityBinding
import com.mcr.active.databinding.EventBookingBinding
import com.mcr.active.databinding.LocationTimeTableBottomSheetBinding
import com.mcr.active.databinding.TimeTableDetailsBinding
import com.mcr.active.model.EventDetailsModel.IminItemItemDetails
import com.mcr.active.model.EventDetailsModel.SubEvent1Item
import com.mcr.active.ui.MainActivity
import com.mcr.active.ui.fragment.home.bottomSheetFragment.location.viewModel.TimeBottomViewModel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.LocationWhiteViewModel
import com.orhanobut.hawk.Hawk
import java.util.*

class AccessBottomSheetFragment : BottomSheetDialogFragment(){


    var viewmodel: LocationWhiteViewModel? = null
    var binding: AccesibilityBinding? = null
    private var fragmentView: View? = null
    var contex :Context? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CoffeeDialog)
        viewmodel = ViewModelProviders.of(activity!!).get(LocationWhiteViewModel::class.java)
        Util.hawk(this.context!!)
        Hawk.put("url","imin")
        contex = this.context!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater,R.layout.accesibility,container,false)
        binding?.viewModel = viewmodel
//        binding?.TimeVieModel = viewmodelTime
        initObservables()
        return binding?.getRoot()
    }

    private fun initObservables() {
        val list = ArrayList<String>(Arrays.asList(*resources.getStringArray(R.array.Accessibility)))
        viewmodel?.list = ArrayList<String>(Arrays.asList(*resources.getStringArray(com.mcr.active.R.array.Accessibility)))
        binding?.recAccess?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL ,false)
        binding?.recAccess?.adapter =
            AccessAdapter(viewmodel!!, list)
        viewmodel!!.setAccessAdapter(list)
        binding?.imgClose?.setOnClickListener {
            dismiss()
        }
    }


}