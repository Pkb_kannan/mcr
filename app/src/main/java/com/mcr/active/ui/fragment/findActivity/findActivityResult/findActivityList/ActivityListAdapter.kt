package com.mcr.active.ui.fragment.findActivity.findActivityResult.findActivityList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.databinding.FinactivityListItemBinding
import com.mcr.active.model.findActivityListModel.IminItemItem


class ActivityListAdapter(private val viewModel: FindListViewModel,
                          private var items: List<IminItemItem>)
    : RecyclerView.Adapter<ActivityListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = FinactivityListItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

//    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemCount(): Int = items.size




    fun setLocation(noti: List<IminItemItem>) {
        this.items = noti
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: FinactivityListItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: FindListViewModel?, position: Int?
        ) { //            viewModel.fetchDogBreedImagesAt(position);
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

