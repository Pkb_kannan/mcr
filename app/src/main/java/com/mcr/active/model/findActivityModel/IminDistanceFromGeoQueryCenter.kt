package com.mcr.active.model.findActivityModel


import com.google.gson.annotations.SerializedName


data class IminDistanceFromGeoQueryCenter(

	@field:SerializedName("unitCode")
	val unitCode: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("value")
	val value: Double? = null
)