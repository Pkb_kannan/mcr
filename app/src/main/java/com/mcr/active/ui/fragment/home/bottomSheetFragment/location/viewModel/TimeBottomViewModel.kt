package com.mcr.active.ui.fragment.home.bottomSheetFragment.location.viewModel

import android.app.Application
import android.graphics.Color
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.mcr.active.Util.SingleLiveEvent
import java.util.ArrayList


class TimeBottomViewModel (application: Application) : AndroidViewModel(application){

    var days  : ArrayList<String>? =null
    var list :ArrayList<String>  ?= ArrayList()
    var textspin: ObservableField<String>? = null
    var selectedItem : ArrayList<String>?= ArrayList()
    var refreshDayAdapter: SingleLiveEvent<Boolean>? = null

    init{
        textspin = ObservableField("")
        refreshDayAdapter = SingleLiveEvent<Boolean>()
    }

    fun  getResultColor(Index :Int): Int {
        var color = Color.WHITE
        if (selectedItem?.contains(list?.get(Index))!!){
            color = Color.RED
        }
        else {
            color = Color.WHITE
        }
        return color
    }

    fun weekSelect(Index :Int){
        if (selectedItem?.contains(list?.get(Index))!!){
            selectedItem!!.remove(list?.get(Index)!!)

        }
        else {
            selectedItem!!.add(list?.get(Index)!!)
        }
        refreshDayAdapter?.value = true
    }

}