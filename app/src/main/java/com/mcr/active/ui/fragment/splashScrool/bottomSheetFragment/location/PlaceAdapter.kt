package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.databinding.LocationBotomSheetWhiteItemBinding
import com.mcr.active.model.facilityModel.FacilityIminItemItem


class PlaceAdapter(
    private val viewModel: LocationWhiteViewModel,
    private var items: List<FacilityIminItemItem>?
)
    : RecyclerView.Adapter<PlaceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = LocationBotomSheetWhiteItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemCount(): Int = items!!.size
    fun setLocation(
        noti: List<FacilityIminItemItem>?
    ) {
        this.items = noti
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: LocationBotomSheetWhiteItemBinding):
            RecyclerView.ViewHolder(binding.root) {
        fun bind(viewModel: LocationWhiteViewModel?, position: Int?
        ) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

