package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.timeTable

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.Util.Util.Companion.getDistance
import com.mcr.active.databinding.EventBookingBinding
import com.mcr.active.databinding.LocationTimeTableBottomSheetBinding
import com.mcr.active.databinding.TimeTableDetailsBinding
import com.mcr.active.databinding.WebViewBinding
import com.mcr.active.model.EventDetailsModel.IminItemItemDetails
import com.mcr.active.model.EventDetailsModel.SubEvent1Item
import com.mcr.active.ui.fragment.home.bottomSheetFragment.location.viewModel.TimeBottomViewModel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.LocationWhiteViewModel
import com.orhanobut.hawk.Hawk
import java.util.*

class WebView : BottomSheetDialogFragment() {


    var viewmodel:LocationWhiteViewModel ? = null
    var binding: WebViewBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CoffeeDialog)
        viewmodel = ViewModelProviders.of(activity!!).get(LocationWhiteViewModel::class.java)
        Util.hawk(this.context!!)
        Hawk.put("url","imin")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater,R.layout.web_view,container,false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.getRoot()
    }


    private fun initObservables() {

        binding?.web?.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        viewmodel?.webLoad?.observeForever {
            if (it){
                binding?.web?.loadUrl(viewmodel?.WebUrl.toString())
                viewmodel?.webLoad!!.value = false
            }
        }



    }




}