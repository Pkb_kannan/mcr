package com.mcr.active.model.favouritModel.favLeisureModel


import com.google.gson.annotations.SerializedName


data class EventItem(

	@field:SerializedName("offers")
	val offers: List<OffersItem?>? = null,

	@field:SerializedName("duration")
	val duration: String? = null,

	@field:SerializedName("identifier")
	val identifier: String? = null,

	@field:SerializedName("beta:sportsActivityLocation")
	val betaSportsActivityLocation: List<BetaSportsActivityLocationItem?>? = null,

	@field:SerializedName("endDate")
	val endDate: String? = null,

	@field:SerializedName("maximumUses")
	val maximumUses: Int? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("startDate")
	val startDate: String? = null,

	@field:SerializedName("remainingUses")
	val remainingUses: Int? = null,

	@field:SerializedName("checkoutUrl")
	val checkoutUrl: String? = null
)