package com.mcr.active.ui.fragment.newsAndEvents

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.databinding.NewsEventsBinding
import com.mcr.active.databinding.NewsEventsDetailsBinding
import com.mcr.active.model.viewPagerModel.ViewPagerModel
import com.mcr.active.ui.fragment.splashScrool.ViewPagerAdapter
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.search.SearchSpinAdapter
import com.orhanobut.hawk.Hawk
import java.util.*
import kotlin.collections.ArrayList


class NewsAndEventsDetails : Fragment() {

    var currentPage = 0
    var timer: Timer? = null
    val DELAY_MS: Long = 500
    val PERIOD_MS: Long = 3000
    var contex : Context? = null
    var viewmodel: NewsViewModel? = null
    var binding: NewsEventsDetailsBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewmodel = ViewModelProviders.of(activity!!).get(NewsViewModel::class.java)
        contex = this.context!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.news_events_details, container, false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.root

    }


    private fun initObservables() {
//        viewmodel?.getListDetails(NewsAndEvents.eventId)
        binding?.txtDiscription?.text = "Testing"
        dinding()
//        viewmodel?.listComplectDetails?.observeForever {
//            if (it) {
//                viewmodel?.listComplectDetails?.value = false
////                dinding()
//
//            }
//        }


    }
    fun dinding(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                binding?.txtDiscription?.text = Html.fromHtml(viewmodel?.listDetails?.description, Html.FROM_HTML_MODE_COMPACT).toString()
        else
            binding?.txtDiscription?.text = Html.fromHtml(viewmodel?.listDetails?.description).toString()

        binding?.txtTile?.text = viewmodel?.listDetails?.title
        if (viewmodel?.listDetails?.more?.size!! >0 ){
            binding?.txtSeconTitle?.text = viewmodel?.listDetails?.more?.get(0)?.quote
            binding?.txtSecontDes?.text = viewmodel?.listDetails?.more?.get(0)?.paragraph
            if (viewmodel?.listDetails?.more?.size!! >1){
                binding?.txtThirdTitle?.text = viewmodel?.listDetails?.more?.get(1)?.quote
                binding?.txtThirdDes?.text = viewmodel?.listDetails?.more?.get(1)?.paragraph
            }
        }
//        if (viewmodel?.listDetails?.postType.equals("E")){
//            binding?.txtType?.text = "EVENTS"
//        }
//        else{
//            binding?.txtType?.text = "NEWS STORY"
//        }

        setupViews()
    }

    private fun populateList(): ArrayList<ViewPagerModel> {

        val list = ArrayList<ViewPagerModel>()

        if (viewmodel?.listDetails?.media?.size==0 ) {
            val imageModel = ViewPagerModel()
            imageModel.setConten(viewmodel?.listDetails?.mediaUrl!!)
            list.add(imageModel)
        }
        else {
            for (i in viewmodel?.listDetails?.media?.indices!!) {
                val imageModel = ViewPagerModel()

                imageModel.setConten(viewmodel?.listDetails?.media!![i]?.mediaUrl!!)
                list.add(imageModel)
            }
        }

        return list
    }

    private fun setupViews() {
        var imageModelArrayList : ArrayList<ViewPagerModel>? = null
        imageModelArrayList = populateList()
       val pagerAdapter = NewsViewPagerAdapter(contex!!,imageModelArrayList)
        binding?.pagerBanner?.adapter = pagerAdapter
        binding?.indicator?.setViewPager( binding?.pagerBanner)

//        val density = resources.displayMetrics.density

        //Set circle indicator radius
        binding?.indicator?.setRadius((5 * 2.2).toFloat())

        /*After setting the adapter use the timer */
        val handler = Handler()
        val Update = Runnable {
            if (currentPage === imageModelArrayList.size ) {
                currentPage = 0
            }
            binding?.pagerBanner?.setCurrentItem(currentPage++, true)
        }

        timer = Timer() // This will create a new Thread
        timer!!.schedule(object : TimerTask() { // task to be scheduled
            override fun run() {
                handler.post(Update)
            }
        }, DELAY_MS, PERIOD_MS)


    }
}

