package com.mcr.active.model.challengeModels

import com.google.gson.annotations.SerializedName


data class TaskListItem(

	@field:SerializedName("task_name")
	val taskName: String? = null,

	@field:SerializedName("task_description")
	val taskDescription: String? = null,

	@field:SerializedName("task_value")
	val taskValue: String? = null,
	@field:SerializedName("task_id")
	val taskId: String? = null,
	@field:SerializedName("challenge_id")
	val challegeId: String? = null,
	@field:SerializedName("acheived_value")
	val acheivedValue: String? = null
)
