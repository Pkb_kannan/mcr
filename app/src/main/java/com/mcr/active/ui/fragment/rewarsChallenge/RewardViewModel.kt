package com.mcr.active.ui.fragment.rewarsChallenge

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.model.challengeList
import com.mcr.active.model.favouritModel.removeModel.RemoveFavModel
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.model.rewardModels.DataItem
import com.mcr.active.model.rewardModels.RespModel
import com.mcr.active.network.Api
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.adapter.RewardAdapter
import com.orhanobut.hawk.Hawk
import retrofit2.Call
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.ArrayList
import javax.inject.Inject

class RewardViewModel (application: Application) : AndroidViewModel(application){

    @Inject
    lateinit var api: Api
    var openDetails: SingleLiveEvent<Boolean>? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    var kidos: MutableLiveData<String> = MutableLiveData<String>()

    init {
        openDetails = SingleLiveEvent<Boolean>()
        kidos?.value = Hawk.get("Kudos","0")

    }
    fun getProgress(): Int? {
        return  kidos?.value?.toInt()
    }


}
