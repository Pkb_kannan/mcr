package com.mcr.active.model.newsAndEvends.list


import com.google.gson.annotations.SerializedName


data class FeaturedItem(

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("event_duration")
	val eventDuration: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("media_url")
	val mediaUrl: String? = null,

	@field:SerializedName("client_id")
	val clientId: String? = null,

	@field:SerializedName("venue_facility_id")
	val venueFacilityId: String? = null,

	@field:SerializedName("event_cost")
	val eventCost: String? = null,

	@field:SerializedName("event_date")
	val eventDate: String? = null,

	@field:SerializedName("activity_id")
	val activityId: String? = null,

	@field:SerializedName("post_type")
	val postType: String? = null,

	@field:SerializedName("location")
	val location: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("featured_post_order")
	val featuredPostOrder: String? = null,

	@field:SerializedName("supplier_id")
	val supplierId: String? = null,

	@field:SerializedName("event_time")
	val eventTime: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("venue_id")
	val venueId: String? = null,

	@field:SerializedName("is_featured")
	val isFeatured: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)