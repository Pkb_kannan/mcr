package com.mcr.active.ui.fragment.fitnessApps

import android.app.Application
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.base.BaseViewModel
import com.mcr.active.model.CommonResponseModel
import com.mcr.active.model.getApps.AppDetailsItem
import com.mcr.active.model.getApps.GetAllAppsModel
import com.mcr.active.model.getApps.fitBitModel.FitBitModel
import com.mcr.active.model.getApps.googleFit.GoogleFit
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.network.Api
import com.orhanobut.hawk.Hawk
import retrofit2.Call
import retrofit2.Response
import java.security.Timestamp
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


class MyAppsViewModel(application: Application) : BaseViewModel(application) {

    @Inject
    lateinit var api: Api
    var fitibit: SingleLiveEvent<Boolean>? = null
    var googlfit: SingleLiveEvent<Boolean>? = null
    var userData: GetAllAppsModel = GetAllAppsModel()
    var stravalaung: SingleLiveEvent<Boolean>? = null
    var googlfitRlogin: SingleLiveEvent<Boolean>? = null
    var refresAdadpter: SingleLiveEvent<Boolean>? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    var refresAdadpterSignIn: SingleLiveEvent<Boolean>? = null
    var applistnormal : ArrayList<AppDetailsItem> = ArrayList()
    private val appslists: MutableLiveData<List<AppDetailsItem>> = MutableLiveData()

    init {
        fitibit = SingleLiveEvent<Boolean>()
        googlfit = SingleLiveEvent<Boolean>()
        stravalaung = SingleLiveEvent<Boolean>()
        refresAdadpter = SingleLiveEvent<Boolean>()
        googlfitRlogin = SingleLiveEvent<Boolean>()
        refresAdadpterSignIn = SingleLiveEvent<Boolean>()
    }
    var linked_appId: String = "1"
    var appLinkType: String = "link" // or unlink
    var appTocken: String = "dsafgsdfg"  // app tocken from linked app

    fun getAllEmployee(): ObservableArrayList<String> {
        var apps= ObservableArrayList<String>()
        apps.add("LINK1")
        apps.add("LINK2")
        apps.add("LINK3")
        apps.add("LINK4")
        return apps
    }

    fun getLinkedApps(): MutableLiveData<List<AppDetailsItem>> {
        isProgress.set(View.VISIBLE)
        val member_id = Hawk.get("userData", LoginModel())
        val call = api.getAllapps(Util.getBaseUrl() + "getallapp", member_id.memberId.toString())
        call?.enqueue(object : retrofit2.Callback<GetAllAppsModel> {
            override fun onFailure(call: Call<GetAllAppsModel>?, t: Throwable?) {
                Log.e("response", t?.message)
                Toast.makeText(getApplication(), "Try again", Toast.LENGTH_SHORT).show()
                isProgress.set(View.GONE)
            }


            override fun onResponse(
                call: Call<GetAllAppsModel>?,
                response: Response<GetAllAppsModel>?
            ) {
                if (response?.body()?.status!!) {
                    userData = response?.body()!!
                    isProgress.set(View.GONE)
                    applistnormal = userData.data as ArrayList<AppDetailsItem>
                    appslists.value = userData.data

                } else {
                    Toast.makeText(getApplication(), response?.body()?.message, Toast.LENGTH_SHORT)
                        .show()
                }
                isProgress.set(View.GONE)
            }
        })
        return appslists
    }

    fun getLinkUnlinkApps(position: Int, token: ArrayList<String>?) {
        var call : Call<CommonResponseModel>? = null
        isProgress.set(View.VISIBLE)
        appTocken = token!![0]
        linked_appId= appslists.value!![position]?.id!!
        val member_id = Hawk.get("userData", LoginModel())
        if (position == 0){
            call = api.getLinkUnlinkStrava(
                Util.getBaseUrl() + "link_unlinkapp",
                member_id.memberId.toString(), appLinkType,linked_appId,appTocken,token!![3],token!![3],token!![4],token!![2])//,token!![5]
           // )
        }
        else if (position == 1){
            call = api.getLinkUnlinkFitBit(
                Util.getBaseUrl() + "link_unlinkapp",
                member_id.memberId.toString(), appLinkType,linked_appId,appTocken,token!![2],token!![3],token!![1]
            )
        }

        else{
            call = api.getLinkUnlinkapps(
                Util.getBaseUrl() + "link_unlinkapp",
                member_id.memberId.toString(), appLinkType,linked_appId,appTocken,token!![3],token!![1],token!![2])
        }



        call?.enqueue(object : retrofit2.Callback<CommonResponseModel> {
            override fun onFailure(call: Call<CommonResponseModel>?, t: Throwable?) {
                Log.e("response", t?.message)
                Toast.makeText(getApplication(), "Try again", Toast.LENGTH_SHORT).show()
                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<CommonResponseModel>?,
                response: Response<CommonResponseModel>?
            ) {
                if (response?.code()==200) {

                    Toast.makeText(getApplication(), response?.body()?.message, Toast.LENGTH_SHORT)
                        .show()
                    var strava = AppDetailsItem(id = appslists.value!![position].id,
                        name = appslists.value!![position].name,
                        logo = appslists.value!![position].logo,
                        connected = true)
                    applistnormal.set(position,strava)
                    appslists.value = applistnormal

                    if(position==2)
                    refresAdadpterSignIn?.value = true
                    else
                        refresAdadpter?.value = true
                    isProgress.set(View.GONE)

                } else {
                    Toast.makeText(getApplication(), response?.body()?.message, Toast.LENGTH_SHORT)
                        .show()
                }
                isProgress.set(View.GONE)
            }
        })
    }
    fun getUnlinkApps(position : Int) {
        isProgress.set(View.VISIBLE)
        linked_appId= appslists.value!![position]?.id!!
        val member_id = Hawk.get("userData", LoginModel())
        val call = api.getUnlinkapps(
            Util.getBaseUrl() + "link_unlinkapp",
            member_id.memberId.toString(), "unlink",linked_appId
            )
        call?.enqueue(object : retrofit2.Callback<CommonResponseModel> {
            override fun onFailure(call: Call<CommonResponseModel>?, t: Throwable?) {
                Log.e("response", t?.message)
                Toast.makeText(getApplication(), "Try again", Toast.LENGTH_SHORT).show()
                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<CommonResponseModel>?,
                response: Response<CommonResponseModel>?
            ) {
                if (response?.body()?.status!!) {

                    Toast.makeText(getApplication(), response?.body()?.message, Toast.LENGTH_SHORT)
                        .show()
                    var strava = AppDetailsItem(id = appslists.value!![position].id,
                        name = appslists.value!![position].name,
                        logo = appslists.value!![position].logo,
                        connected = false)
                    applistnormal.set(position,strava)
                    appslists.value = applistnormal

                    refresAdadpter?.value = true
                    isProgress.set(View.GONE)

                } else {
                    Toast.makeText(getApplication(), response?.body()?.message, Toast.LENGTH_SHORT)
                        .show()
                }
                isProgress.set(View.GONE)
            }
        })
    }

    fun getGoogleFitDetails(code:String,token:String) {
        isProgress.set(View.VISIBLE)
        val member_id = Hawk.get("userData", LoginModel())
        val call = api.googltFitToken(
            "https://oauth2.googleapis.com/token",
            "924779780263-74b3p5vodd3vqin1132ksu5332qo6gh7.apps.googleusercontent.com",
            "3GjYA044qVfg1DKBH57oyR0q",
            code,
            "https://mcractive-6d96e.firebaseapp.com/__/auth/handler",
            "authorization_code"
            )
        call?.enqueue(object : retrofit2.Callback<GoogleFit> {
            override fun onFailure(call: Call<GoogleFit>?, t: Throwable?) {
                Log.e("response", t?.message)
                Toast.makeText(getApplication(), "Try again", Toast.LENGTH_SHORT).show()
                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<GoogleFit>?,
                response: Response<GoogleFit>?
            ) {
                if (response?.isSuccessful!!) {

                    var listdetails: ArrayList<String> = arrayListOf()
                    if (!response?.body()?.refreshToken!!?.equals(null)) {
                        listdetails?.add(response?.body()?.accessToken!!)
                        listdetails?.add(response?.body()?.refreshToken!!)
                        listdetails?.add(response?.body()?.expiresIn.toString())
                        listdetails?.add(response?.body()?.tokenType!!)
                        isProgress.set(View.GONE)
                        getLinkUnlinkApps(2, listdetails)
                    }
                    else{
                        googlfitRlogin?.value = true
                    }

                } else {

                }
                isProgress.set(View.GONE)
            }
        })
    }
    fun appnavigations(pos: Int){
        var appid = appslists.value?.get(pos)?.id
        when(appid){
            "3"->{
//                val i = Intent(getApplication(), StravaActivity::class.java)
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                ContextCompat.startActivity(getApplication(), i, null)
                //                Toast.makeText(getApplication(),"strava",Toast.LENGTH_SHORT).show()
                stravalaung?.value = true
            }
            "4"->fitibit?.value = true
            "5"->googlfit?.value = true
        }

    }

    fun setVisiblity(index: Int?): Int {
        if (appslists.value?.get(index!!)?.connected!!){
            return View.VISIBLE
        }
        else {
            return View.GONE
        }
    }
    fun stravStatus1(){

        stravalaung?.value = true
    }
    fun stravStatus(){
        Toast.makeText(getApplication(),"success",Toast.LENGTH_SHORT).show()
       var strava = AppDetailsItem(id = appslists.value!![0].id,
            name = appslists.value!![0].name,
            logo = appslists.value!![0].logo,
            connected = true)
        applistnormal.set(0,strava)
        appslists.value = applistnormal

        stravalaung?.value = true
    }
    fun getAccessToken(): String? { //mark!!!
        /** This must be obtained from redirect_url tha contained the authorization code  */
        val authCode: String = Hawk.get("fitbiToken", "null")
        val clientId = "22BJPX"
        //        String  grantType = "token";
        val grantType = "authorization_code"
        val redirectUrl = "https://mcractive.com"
        return String.format(
            "https://api.fitbit.com/oauth2/token?" + "grant_type" + "=" + grantType + "&"
                    + "client_id" + "=%s&" + "redirect_uri" + "=%s&" + "code" + "=" + authCode,
            clientId, redirectUrl, authCode
        )
    }
    fun fitbitToken(){
        isProgress.set(View.VISIBLE)
        val call = api.fibitToken(getAccessToken()!!,"application/x-www-form-urlencoded"," Basic MjJCSlBYOjIyYzNiODhlMTViNGU1YTcyYTVkM2ZlOWE1MTg1ZmI5")
        call?.enqueue(object : retrofit2.Callback<FitBitModel> {
            override fun onFailure(call: Call<FitBitModel>?, t: Throwable?) {
                Log.e("response", t?.message)
                Toast.makeText(getApplication(), "Try again", Toast.LENGTH_SHORT).show()
                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<FitBitModel>?,
                response: Response<FitBitModel>?
            ) {
                if (response?.isSuccessful!!) {
                    val tsLong = System.currentTimeMillis()
                    var listdetails: ArrayList<String> = arrayListOf()
                    listdetails?.add(response.body()?.accessToken!!)
//                    listdetails?.add(response.body()?.)
                    listdetails?.add(response.body()?.expiresIn?.plus(tsLong).toString())
                    listdetails?.add(response.body()?.tokenType!!)
                    listdetails?.add(response.body()?.refreshToken!!)

                    val ts = response.body()?.expiresIn?.plus(tsLong*1000)
//                    val expiration = tsLong + Int64(response.expiresIn * 1000)

                    val cal = Calendar.getInstance(Locale.ENGLISH)
                    cal.setTimeInMillis(ts!!)
                    val date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString()
//                    val sdf =  SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
//                    val date1 = sdf.parse(date);
//                   val calendar = Calendar.getInstance();
//
//                    calendar.setTime(date1);
//                    calendar.add(Calendar.MILLISECOND, response.body()?.expiresIn!!)
                    Log.e("time_stamp",date)
//                    listdetails?.add(code)
                    getLinkUnlinkApps(1, listdetails)

//                    isProgress.set(View.GONE)

                } else {
                    Toast.makeText(getApplication(), response?.message(), Toast.LENGTH_SHORT)
                        .show()
                }
                isProgress.set(View.GONE)
            }
        })

    }


}
