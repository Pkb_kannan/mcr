package com.mcr.active.model.favouritModel.favouritrIminItemModel


import com.google.gson.annotations.SerializedName


data class IminLocationSummaryItem(

	@field:SerializedName("geo")
	val geo: Geo? = null,

	@field:SerializedName("identifier")
	val identifier: String? = null,

	@field:SerializedName("address")
	val address: Address? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("telephone")
	val telephone: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)