package com.mcr.active.ui.fragment.home.bottomSheetFragment.location

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mcr.active.R
import com.mcr.active.databinding.LocationBotomSheetBinding
import com.mcr.active.ui.fragment.home.Home
import com.mcr.active.ui.fragment.home.HomeViewmodel

class LocationBottomSheetFragment : BottomSheetDialogFragment() {

    var binding: LocationBotomSheetBinding? = null
    var viewmodel: HomeViewmodel? = null
    private var fragmentView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CoffeeDialog)
        viewmodel = ViewModelProviders.of(activity!!).get(HomeViewmodel::class.java)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater,R.layout.location_botom_sheet,container,false)

        binding?.viewModel = viewmodel
        initObservables()
        return binding?.getRoot()
    }
    private fun initObservables() {
        binding?.btLocation?.setOnClickListener(View.OnClickListener {
            dismiss()
        })
        binding?.btSearch?.setOnClickListener(View.OnClickListener {
            dismiss()
            fragmentManager?.let { it1 -> viewmodel?.showSearchDialogFragment(it1) }
        })
        binding?.btStar?.setOnClickListener(View.OnClickListener {
            dismiss()
            fragmentManager?.let { it1 -> viewmodel?.showFavouritDialogFragment(it1) }
        })
        binding?.btInfo?.setOnClickListener(View.OnClickListener {
            dismiss()
            fragmentManager?.let { it1 -> viewmodel?.showLocatioInfoDialogFragment(it1) }
        })
        binding?.btTime?.setOnClickListener(View.OnClickListener {
            dismiss()
            fragmentManager?.let { it1 -> viewmodel?.showLocatioTimeDialogFragment(it1) }
        })

    }

}