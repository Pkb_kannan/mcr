package com.mcr.active.ui.fragment.rewarsChallenge

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.mcr.active.R
import com.mcr.active.databinding.RewardChallengeBinding
import com.mcr.active.ui.fragment.home.HomeViewmodel
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.previous.PreviousDraws
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.challenge.Challenge
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.Reward
import com.orhanobut.hawk.Hawk


//import com.mcr.active.ui.fragment.rewarsChallenge.RewardChallengsDirections.actionFragmentRewardToFragmentChallenge


class RewardChallengs : Fragment() {

    companion object {
        var viewmodel: RewardViewModel? = null
        var binding: RewardChallengeBinding? = null
        fun setkudo(){
            binding?.txtEarned?.text = Hawk.get("Kudos","0")
        }
        init {
            viewmodel?.kidos?.value = Hawk.get("Kudos","0")
        }
    }


//    var bottomSheetBehavior = BottomSheetBehavior.from(location_bottom)
private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>
//    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewmodel = ViewModelProviders.of(activity!!).get(RewardViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater,R.layout.reward_challenge,container,false)
        binding?.viewModel = viewmodel
        initObservables()

        return binding?.getRoot()
    }
    @SuppressLint("ResourceAsColor", "ResourceType")
    private fun initObservables() {
        binding?.txtEarned?.text = Hawk.get("Kudos","0")
        binding?.btChallenge?.setOnClickListener(View.OnClickListener {
            hide()
            binding?.btChallenge?.setBackgroundResource(R.color.colorRed)
            binding?.btAchivment?.setBackgroundResource(R.drawable.hash_line_box)
            binding?.btReward?.setBackgroundResource(R.drawable.hash_line_box)
            val transaction = getFragmentManager()?.beginTransaction()
            transaction?.replace(R.id.reward_challeng_fragment, Challenge())!!.addToBackStack("challenge_fragment")
            transaction?.commit()
//            Navigation.findNavController(it).navigate(actionNavRewardToChallenge())
//            val finalHost = NavHostFragment.create(R.navigation.reward_chellage)
//            getFragmentManager()?.beginTransaction()
//                ?.replace(R.id.fragment_challenge, finalHost)
//                ?.setPrimaryNavigationFragment(finalHost) // this is the equivalent to app:defaultNavHost="true"
//                ?.commit()

        })

//        binding?.btAchivment?.setOnClickListener(View.OnClickListener {
//            hide()
//            binding?.btAchivment?.setBackgroundResource(R.color.colorRed)
//            binding?.btChallenge?.setBackgroundResource(R.drawable.hash_line_box)
//            binding?.btReward?.setBackgroundResource(R.drawable.hash_line_box)
//        })

        binding?.btReward?.setOnClickListener(View.OnClickListener {

            show()
            binding?.btReward?.setBackgroundResource(R.color.colorRed)
            binding?.btChallenge?.setBackgroundResource(R.drawable.hash_line_box)
            binding?.btAchivment?.setBackgroundResource(R.drawable.hash_line_box)
            val transaction = getFragmentManager()?.beginTransaction()
            transaction?.replace(R.id.reward_challeng_fragment, Reward())!!.addToBackStack("reward")
            val count= fragmentManager!!.backStackEntryCount
            transaction?.commit()

        })

//        binding?.txtChallenges?.setOnClickListener(View.OnClickListener {
//            binding?.txtChallenges?.setBackgroundResource(R.color.ColorHash_BG)
//            binding?.price?.setBackgroundResource(R.drawable.hash_line_box)
//            binding?.previous?.setBackgroundResource(R.drawable.hash_line_box)
//
//        })

        binding?.previous?.setOnClickListener(View.OnClickListener {
            binding?.previous?.setBackgroundResource(R.color.ColorHash_BG)
            binding?.txtChallenges?.setBackgroundResource(R.drawable.hash_line_box)
            binding?.price?.setBackgroundResource(R.drawable.hash_line_box)
            val transaction = getFragmentManager()?.beginTransaction()
            transaction?.replace(R.id.reward_challeng_fragment,
                PreviousDraws()
            )
            transaction?.commit()
        })

        binding?.price?.setOnClickListener(View.OnClickListener {
            binding?.price?.setBackgroundResource(R.color.ColorHash_BG)
            binding?.txtChallenges?.setBackgroundResource(R.drawable.hash_line_box)
            binding?.previous?.setBackgroundResource(R.drawable.hash_line_box)
            val transaction = getFragmentManager()?.beginTransaction()
            transaction?.replace(R.id.reward_challeng_fragment, Reward())
            transaction?.commit()
        })


    }

    private fun hide() {
        binding?.previous?.visibility=View.GONE
        binding?.price?.visibility=View.GONE
        binding?.txtChallenges?.visibility=View.GONE
        binding?.lenTop?.visibility=View.GONE
    }
    private fun show() {
        binding?.previous?.visibility=View.VISIBLE
        binding?.price?.visibility=View.VISIBLE
        binding?.txtChallenges?.visibility=View.VISIBLE
        binding?.lenTop?.visibility=View.VISIBLE
    }


}


