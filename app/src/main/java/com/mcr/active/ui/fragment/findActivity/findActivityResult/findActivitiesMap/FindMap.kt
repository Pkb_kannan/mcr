package com.mcr.active.ui.fragment.findActivity.findActivityResult.findActivitiesMap

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterItem
import com.google.maps.android.clustering.ClusterManager
import com.mcr.active.MCRApp
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.databinding.FindActivitiesMapBinding
import com.mcr.active.databinding.MapMarkerClickWindowBinding
import com.mcr.active.model.FindActivityMapModel.IminItemItem
import com.mcr.active.model.FindActivityMapModel.MapClusterModel
import com.mcr.active.ui.fragment.findActivity.findActivityResult.FindResultMain
import com.orhanobut.hawk.Hawk
import java.math.BigDecimal
import java.math.RoundingMode


class FindMap : Fragment(), OnMapReadyCallback {

    lateinit var mMap: GoogleMap
    var viewmodel: FindMapViewModel? = null
    var binding: FindActivitiesMapBinding? = null
    var binding1: MapMarkerClickWindowBinding? = null
    var markersArray: List<IminItemItem>? = arrayListOf()
    private var mClusterManager: ClusterManager<MapClusterModel>? = null

    companion object {

        fun newInstance(): FindMap {
            return FindMap()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewmodel = ViewModelProviders.of(activity!!).get(FindMapViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.find_activities_map, container, false)

        binding?.viewModel = viewmodel
//
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        initObservables()

        /*Google Map*/
        return binding?.getRoot()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        bt_mapview_close.setOnClickListener {
////            Navigation.findNavController(it).navigate(actionNavFindactivityMapFragmentToNavLocation())
////            fragmentManager!!.popBackStack()
//        }
    }

    private fun initObservables() {

        context?.let { Util?.hawk(it) }
        viewmodel?.filterList = Hawk.get("findactvities", viewmodel?.filterList)!!
        viewmodel?.filterMap = viewmodel?.filterList?.get(0)!!
        mClusterManager?.clearItems()
        viewmodel?.loadComplect?.value = false
        viewmodel?.search()
        viewmodel?.loadComplect?.observeForever(Observer {
            if (it) {
                markersArray = viewmodel?.positions()
                FindResultMain.couuntTotal(markersArray?.size!!)
                if (markersArray?.size!! > 0) {

                    onMapReady(mMap)
//                    map_loadrg()
                }
            }
        })


    }

    fun map_loadrg() {

        if (::mMap.isInitialized) {

            mClusterManager = ClusterManager<MapClusterModel>(MCRApp.instance, mMap)
            mMap.setOnCameraIdleListener(mClusterManager)
            for (i in markersArray?.indices!!) {
                val mapmodel = MapClusterModel()
                mapmodel.MapClusterModel(
                    markersArray?.get(0)?.geo?.latitude!!,
                    markersArray?.get(0)?.geo?.longitude!!
//
                )
                mClusterManager!!.addItem(mapmodel)
            }

//            mMap.setOnMarkerClickListener { onMarkerClick(it) }
            if (markersArray?.size != 0) {
                mMap.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            markersArray?.get(0)?.geo?.latitude!!,
                            markersArray?.get(0)?.geo?.longitude!!
                        ), 8.0f
                    )
                )
            }

        }
        viewmodel?.loadComplect?.value = false
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        if (markersArray?.size != 0) {
            mClusterManager = ClusterManager<MapClusterModel>(MCRApp.instance, mMap)
            mMap.setOnCameraIdleListener(mClusterManager)
            for (i in markersArray?.indices!!) {
                val mapmodel = MapClusterModel()
                mapmodel.MapClusterModel(
                    markersArray?.get(i)?.geo?.latitude!!,
                    markersArray?.get(i)?.geo?.longitude!!
//
                )
                mClusterManager!!.addItem(mapmodel)
            }
//            mMap.setOnMarkerClickListener { onMarkerClick(it)}
            mMap.setOnMarkerClickListener(mClusterManager)
            mClusterManager!!.setOnClusterClickListener { onClusterClick(it) }
            mClusterManager!!.setOnClusterItemClickListener { onMarkerClick(it) }

            if (markersArray?.size != 0) {
                mMap.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            markersArray?.get(0)?.geo?.latitude!!,
                            markersArray?.get(0)?.geo?.longitude!!
                        ), 8.0f
                    )
                )
            }
        }

//        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
//        map_loadrg()
//        viewmodel?.loadComplect?.value = true
    }


    fun onMarkerClick(p0: MapClusterModel): Boolean {
        p0?.position
        for (i in markersArray?.indices!!) {

            if (markersArray!![i].geo?.latitude == p0?.position?.latitude && markersArray!![i].geo?.longitude == p0?.position?.longitude) {
                Details(markersArray!![i])
            }

        }

        return false
    }

    fun onClusterClick(p0: Cluster<MapClusterModel>): Boolean {
        p0?.position
        mMap.moveCamera(
            CameraUpdateFactory.zoomBy(
                2.0f
            )
        )
        return false
    }

    fun Details(details: IminItemItem) {

        val dialog = Dialog(this.context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        viewmodel = ViewModelProviders.of(activity!!).get(FindMapViewModel::class.java)
        binding1 = DataBindingUtil.inflate(
            LayoutInflater.from(dialog.context),
            R.layout.map_marker_click_window,
            null,
            false
        )
        binding1?.root?.let { dialog.setContentView(it) }
//        dialog .setContentView(R.layout.home_dialog)
        binding1?.viewModel = viewmodel

        binding1?.txtTile?.text = details.name
        binding1?.txtAddress?.text = details.address?.iminFullAddress
        binding1?.txtDistanc?.text =
            getPlaceDistance(details.geo?.iminDistanceFromGeoQueryCenter?.value)

        binding1?.txtCash?.setOnClickListener(View.OnClickListener {
            dialog.cancel()
        })
        dialog.show()
    }

    fun getPlaceDistance(index: Double?): String? {

        return (BigDecimal(0.6214 * index!!).setScale(
            2,
            RoundingMode.HALF_EVEN
        )).toString() + " Miles Away"

    }
}