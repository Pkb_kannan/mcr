package com.mcr.active.model.LocationDetailModel


import com.google.gson.annotations.SerializedName


data class Organizer(

	@field:SerializedName("legalName")
	val legalName: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

//	@field:SerializedName("logo")
//	val logo: Logo? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("beta:formattedDescription")
	val betaFormattedDescription: String? = null,

	@field:SerializedName("beta:video")
	val betaVideo: List<BetaVideoItem?>? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("sameAs")
	val sameAs: List<String?>? = null
)