package com.mcr.active.ui.fragment.signup

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.model.RegisterModel.RegisterModel
import com.mcr.active.model.ethnicityModel.EthniCityModel
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.network.Api
import com.orhanobut.hawk.Hawk
import com.mcr.active.base.BaseViewModel
import retrofit2.Call
import retrofit2.Response
import java.util.regex.Pattern
import javax.inject.Inject

class RegViewModel(application: Application) : BaseViewModel(application) {
    @Inject
    lateinit var api: Api

    var isFlag: ObservableBoolean? = null
    var dob: ObservableField<String>? = null
    val eidList = ObservableArrayList<String>()
    var genter: ObservableField<String>? = null
    val arrayList = ObservableArrayList<String>()
    var lastname: ObservableField<String>? = null
    var postCode: ObservableField<String>? = null
    var password: ObservableField<String>? = null
    var doberror: ObservableField<String>? = null
    var ethnicty: ObservableField<String>? = null
    var firstname: ObservableField<String>? = null
    var ethnictyId: ObservableField<String>? = null
    var disability: ObservableField<String>? = null
    var gentererror: ObservableField<String>? = null
    var houseNumber: ObservableField<String>? = null
    var errorMessage: SingleLiveEvent<String>? = null
    var addressLine1: ObservableField<String>? = null
    var addressLine2: ObservableField<String>? = null
    var emailAddress: ObservableField<String>? = null
    var lastnameerror: ObservableField<String>? = null
    var passworderror: ObservableField<String>? = null
    var ethnictyerror: ObservableField<String>? = null
    var contactNumber: ObservableField<String>? = null
    var postcodeerror: ObservableField<String>? = null
    var firstnameerror: ObservableField<String>? = null
    var disabilityerror: ObservableField<String>? = null
    var housenumbererror: ObservableField<String>? = null
    var emailaddresserror: ObservableField<String>? = null
    var addressline2error: ObservableField<String>? = null
    var addressline1error: ObservableField<String>? = null
    var contactnumbererror: ObservableField<String>? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    var launchSinInActivity: SingleLiveEvent<Boolean>? = null
    var launchMoredetailActivity: SingleLiveEvent<Boolean>? = null


    init {
        dob = ObservableField("")
        genter = ObservableField("")
        lastname = ObservableField("")
        ethnicty = ObservableField("")
        firstname = ObservableField("")
        ethnictyId = ObservableField("")
        disability = ObservableField("")
        launchMoredetailActivity = SingleLiveEvent<Boolean>()

        password = ObservableField("")
        postCode = ObservableField("")
        houseNumber = ObservableField("")
        addressLine1 = ObservableField("")
        addressLine2 = ObservableField("")
        emailAddress = ObservableField("")
        contactNumber = ObservableField("")
        errorMessage = SingleLiveEvent<String>()
        housenumbererror = ObservableField("")
        launchSinInActivity = SingleLiveEvent<Boolean>()
        launchMoredetailActivity = SingleLiveEvent<Boolean>()

        doberror = ObservableField("")
        gentererror = ObservableField("")
        ethnictyerror = ObservableField("")
        lastnameerror = ObservableField("")
        passworderror = ObservableField("")
        postcodeerror = ObservableField("")
        firstnameerror = ObservableField("")
        disabilityerror = ObservableField("")
        emailaddresserror = ObservableField("")
        addressline1error = ObservableField("")
        addressline2error = ObservableField("")
        contactnumbererror = ObservableField("")


        clearvalidation()

    }

    fun clearvalidation() {
        doberror!!.set(null)
        gentererror!!.set(null)
        lastnameerror!!.set(null)
        postcodeerror!!.set(null)
        ethnictyerror!!.set(null)
        passworderror!!.set(null)
        firstnameerror!!.set(null)
        disabilityerror!!.set(null)
        housenumbererror!!.set(null)
        addressline1error!!.set(null)
        emailaddresserror!!.set(null)
        addressline2error!!.set(null)
        contactnumbererror!!.set(null)

    }

    fun validateFields() {
        clearvalidation()
        if (houseNumber?.get().equals("")) {
            Log.e("checking", houseNumber?.get().toString())
            housenumbererror?.set("this field required")
        } else if (addressLine1?.get().equals("")) {
            Log.e("checking", addressLine1?.get().toString())
            addressline1error?.set("this field required")
        } else if (addressLine2?.get().equals("")) {
            Log.e("checking", addressLine2?.get().toString())
            addressline2error?.set("this field required")
        } else if (!isValidPostcode(postCode?.get()!!.toUpperCase())) {
            Log.e("checking", postCode?.get().toString())
            postcodeerror?.set("Please enter valid postcode")
        } else if (contactNumber?.get().equals("")) {
            Log.e("checking", contactNumber?.get().toString())
            contactnumbererror?.set("this field required")
        } else if (!Util.isEmailValid(emailAddress?.get().toString())) {
            Log.e("checkingmail", emailAddress?.get().toString())
            emailaddresserror?.set("Please enter valid email")
        } else if (password?.get().equals("")) {
            Log.e("checking", password?.get().toString())
            passworderror?.set("this field required")
        } else {
            Log.d("checking", "Success")
            regUser()
        }

    }

    fun genderSelectItem(
        parent: AdapterView<*>?,
        view: View?,
        pos: Int,
        id: Long
    ) {
        genter?.set(parent?.getSelectedItem().toString())
    }

    fun disabilitySelectItem(
        parent: AdapterView<*>?,
        view: View?,
        pos: Int,
        id: Long
    ) {
        disability?.set(parent?.getSelectedItem().toString())
    }

    fun login() {
        if (houseNumber?.get().equals("")) {
            Log.e("checking", houseNumber?.get().toString())
            housenumbererror?.set("this field required")
        }
        errorMessage?.value = ""
//        launchSinInActivity?.value = true
    }

    fun AddMoreDetails() {
        clearvalidation()
        if (firstname?.get().equals("")) {
            Log.e("checking", firstname?.get().toString())
            firstnameerror?.set("this field required")
        } else if (lastname?.get().equals("")) {
            Log.e("checking", lastname?.get().toString())
            lastnameerror?.set("this field required")
        } else if (genter?.get().equals("") || genter?.get().equals("Gender")) {
            Log.e("checking", genter?.get().toString())
            gentererror?.set("this field required")
        } else if (dob?.get().equals("")) {
            Log.e("checking", dob?.get().toString())
            doberror?.set("this field required")
        } else if (ethnicty?.get().equals("") || ethnicty?.get().equals("Ethinicity")) {
            Log.e("checking", ethnicty?.get().toString())
            ethnictyerror?.set("this field required")
        } else if (disability?.get().equals("")) {
            Log.e("checking", disability?.get().toString())
            disabilityerror?.set("this field required")
        } else {
            launchMoredetailActivity?.value = true
        }
    }

    fun getEthnicity(): ArrayList<String> {
        isProgress.set(View.VISIBLE)
        val call = api.getEthnicity(Util.getBaseUrl() + "getEthnicity_manchester")
        call?.enqueue(object : retrofit2.Callback<EthniCityModel> {
            override fun onFailure(call: Call<EthniCityModel>?, t: Throwable?) {
//                Log.e("response", t?.message)
                Log.e("response", "fail")
                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<EthniCityModel>?,
                response: Response<EthniCityModel>?
            ) {
                if (response!!.code() == 200) {
                    val gson = Gson()
                    val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                    Log.e("list_value", jsonElement.toString())
                    Log.d("response", response!!.body()?.data.toString())
                    arrayList.clear()
                    arrayList!!.add("Ethinicity")
                    for (i in 0..response!!.body()?.data!!.size - 1) {
                        Log.d("array", response!!.body()?.data!![i]!!.eName.toString())
                        arrayList!!.add(response!!.body()?.data!![i]!!.eName.toString())
                        eidList!!.add(response!!.body()?.data!![i]!!.eId.toString())
                    }
                    isProgress.set(View.GONE)
                } else {
                    Log.d("response", "Ethnicity API Error")
                }
            }
        })
        return arrayList
    }

    fun regUser() {
        isProgress.set(View.VISIBLE)
        Log.d("firstname", firstname?.get()!!.toString())
        Log.d("lastname", lastname?.get()!!.toString())
        Log.d("genter", genter?.get()!!.toString())
        Log.d("disability", disability?.get()!!.toString())
        Log.d("addressLine1", addressLine1?.get()!!.toString())
        Log.d("addressLine2", addressLine2?.get()!!.toString())
        Log.d("postCode", postCode?.get()!!.toString())
        Log.d("contactNumber", contactNumber?.get()!!.toString())
        Log.d("ethnicity", ethnicty?.get()!!.toString())
        Log.d("emailAddress", emailAddress?.get()!!.toString())
        Log.d("password", password?.get()!!.toString())

        val call = api.regUser(
            Util.getBaseUrl() + "member_register",
            firstname?.get()!!.toString(),
            lastname?.get()!!.toString(),
            genter?.get()!!.toString(),
            disability?.get()!!.toString(),
            houseNumber?.get()!!.toString(),
            addressLine1?.get()!!.toString(),
            addressLine2?.get()!!.toString(),
            postCode?.get()!!.toString().toUpperCase(),
            contactNumber?.get()!!.toString(),
            "ethnicty?.get()!!.toString()",
            emailAddress?.get()!!.toString(),
            dob?.get()!!.toString(),
            password?.get()!!.toString()
        )

        call?.enqueue(object : retrofit2.Callback<RegisterModel> {
            override fun onFailure(call: Call<RegisterModel>?, t: Throwable?) {
                Log.d("response", t?.message)
                Log.d("response", "fail")
                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<RegisterModel>?,
                response: Response<RegisterModel>?
            ) {
                val gson = Gson()
                val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                Log.d("list_value", jsonElement.toString())
                Log.d("response", response.toString())
                if (response?.isSuccessful!!) {

//                    var userData: LoginModel = LoginModel(status=true, message="success", memberId=response?.body()?.memberId.toString(),memberEmail = emailAddress?.get()!!.toString() ,
//                        addressOne = addressLine1?.get()!!.toString(),
//                        memberFirstname = firstname?.get()!!.toString(),
//                        memberLastname = lastname?.get()!!.toString(),
//                        addressTwo = addressLine2?.get()!!.toString(),
//                        totalKudos = "0",
//                        token = response?.body()?.token.toString(),
//                        steps = "0",
//                        booking = "0",
//                        activities = "0")
                    firstname = ObservableField("")
                    lastname = ObservableField("")
                    genter = ObservableField("")
                    dob = ObservableField("")
                    ethnicty = ObservableField("")
                    ethnictyId = ObservableField("")
                    disability = ObservableField("")

                    houseNumber = ObservableField("")
                    addressLine1 = ObservableField("")
                    addressLine2 = ObservableField("")
                    postCode = ObservableField("")
                    contactNumber = ObservableField("")
                    emailAddress = ObservableField("")
                    password = ObservableField("")
//                    Hawk.put("userData",userData)
//                    Hawk.put("LoginStatus",true)
//                    Hawk.put("Kudos","0")
                    Toast.makeText(getApplication(), response?.body()?.message, Toast.LENGTH_SHORT)
                        .show()
                    launchSinInActivity?.value = true
                    isProgress.set(View.GONE)
                } else if (response.code() == 400) {
                    Toast.makeText(
                        getApplication(), "Account is already exist with this Email ID.",
                        Toast.LENGTH_LONG
                    ).show()
                    isProgress.set(View.GONE)
                }
            }
        })
    }

    /**
     * Method to check post code is valid or not.
     *
     * @param postcodeString post code
     * @return true if valid else false
     */
    fun isValidPostcode(postcodeString: String): Boolean {
        val pattern =
            Pattern.compile("(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})")
        val matcher = pattern.matcher(postcodeString)
        return matcher.matches()
    }
}