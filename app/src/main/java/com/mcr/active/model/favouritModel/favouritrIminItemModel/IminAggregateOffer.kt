package com.mcr.active.model.favouritModel.favouritrIminItemModel


import com.google.gson.annotations.SerializedName


data class IminAggregateOffer(

	@field:SerializedName("publicAdult")
	val publicAdult: PublicAdult? = null,

	@field:SerializedName("type")
	val type: String? = null
)