package com.mcr.active.ui.fragment.findActivity.findActivityResult

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import com.mcr.active.R
import kotlinx.android.synthetic.main.spinner_search.view.*


internal class FindActAdapter (
    ctx: Context,
    spinList: List<String>
) : ArrayAdapter<String>(ctx, 0, spinList) {


    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {


        val view = recycledView ?: LayoutInflater.from(context).inflate(
            R.layout.spinner_search,
            parent,
            false
        )

        if (position % 2 == 0) {
            view.spinText.setAllCaps(false)
            view.linLayout.setBackgroundColor(Color.WHITE)
            view.spinText.setTextColor(Color.BLACK)

        }
        else {
            view.spinText.setAllCaps(false)
            view.linLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.colorFindSpinner))
            view.spinText.setTextColor(Color.BLACK)
        }
        view.spinText.text=getItem(position)
        return view
    }
}