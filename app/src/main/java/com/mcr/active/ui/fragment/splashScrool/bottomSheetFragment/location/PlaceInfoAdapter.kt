package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.databinding.LocationBottomSheetInfoItemBinding
import com.mcr.active.model.favouritModel.favLeisureModel.AmenityFeatureItem


class PlaceInfoAdapter(
    private val viewModel: LocationWhiteViewModel,
    private var items: List<AmenityFeatureItem?>?
)
    : RecyclerView.Adapter<PlaceInfoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = LocationBottomSheetInfoItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

//    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemCount(): Int = items?.size!!




    fun setLocation(
        noti: List<AmenityFeatureItem?>?
    ) {
        this.items = noti!!
//        this.fragmentManager1 = fragmentManager
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: LocationBottomSheetInfoItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: LocationWhiteViewModel?, position: Int?
        ) { //            viewModel.fetchDogBreedImagesAt(position);
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

