package com.mcr.active.googleFitManager

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Scope
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.fitness.request.DataReadRequest
import com.google.android.gms.fitness.result.DataReadResult
import com.mcr.active.MCRApp
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * NFX Development
 * Created by nick on 7/31/17.
 */
class GoogleFitManager() {

    private lateinit var fitApiClient: GoogleApiClient
    val connectionResultPublishSubject: PublishSubject<Connection> = PublishSubject.create()

    fun buildClient():GoogleApiClient {
        fitApiClient = MCRApp.instance?.let {
            GoogleApiClient.Builder(it)
                .addApi(Fitness.HISTORY_API)
                .addScope(Scope(Scopes.FITNESS_BODY_READ))
                .useDefaultAccount()
                .addConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks {
                    override fun onConnected(initialBundle: Bundle?) {
                        connectionResultPublishSubject.onNext(ConnectionUpdate.onConnected())
                    }

                    override fun onConnectionSuspended(cause: Int) {
                        connectionResultPublishSubject.onNext(ConnectionUpdate.onSuspended(cause))
                    }
                })
                .addOnConnectionFailedListener { result ->
                    connectionResultPublishSubject.onNext(ConnectionUpdate.onFailed(result)) }
                .build()
        }!!
        fitApiClient.connect()
        return fitApiClient
    }

    fun getStepCount(): Long {
        var total: Long = 0
        val result =
            Fitness.HistoryApi.readDailyTotal(
                fitApiClient,
                DataType.TYPE_STEP_COUNT_DELTA
            )
        val totalResult =
            result.await(30, TimeUnit.SECONDS)
        if (totalResult.status.isSuccess) {
            val totalSet = totalResult.total
            total =
                if (totalSet!!.isEmpty) 0 else totalSet.dataPoints[0].getValue(Field.FIELD_STEPS).asInt().toLong()
        } else {
            Log.w(ContentValues.TAG, "There was a problem getting the step count.")
        }
        Log.i(ContentValues.TAG, "Total steps: $total")

        return total
    }

    init {
        buildClient()
    }

    fun getstepCount(): Observable<DataReadResult>? {
//        val startTime = getStartOfDay(day_item_finactivity).timeInMillis
//        val endTime = getEndOfDay(day_item_finactivity).timeInMillis
        val cal = Calendar.getInstance()
        cal.setTime( Date())
        val endTime = cal.getTimeInMillis()
        cal.add(Calendar.YEAR, -1)
        val startTime = cal.getTimeInMillis()



        return Observable.create {
            val readRequest = DataReadRequest.Builder()
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .bucketByTime(1, TimeUnit.DAYS)
                .build()

            val result = Fitness.HistoryApi.readData(fitApiClient, readRequest)

            result.setResultCallback({ resultData ->
                if (resultData.status.isSuccess) {
                    it.onNext(resultData)
                    it.onComplete()
                } else {
                    it.onError(FitApiException(resultData.status))
                }
            }, 30, TimeUnit.SECONDS)
        }
    }

    private fun getStartOfDay(day: Calendar): Calendar {
        day.set(Calendar.HOUR_OF_DAY, 0)
        day.set(Calendar.MINUTE, 0)
        day.set(Calendar.SECOND, 0)
        day.set(Calendar.MILLISECOND, 0)
        return day
    }

    private fun getEndOfDay(day: Calendar): Calendar {
        day.set(Calendar.HOUR_OF_DAY, 23)
        day.set(Calendar.MINUTE, 59)
        day.set(Calendar.SECOND, 59)
        day.set(Calendar.MILLISECOND, 999)
        return day
    }

    fun connect() {
        if( !fitApiClient.isConnecting && !fitApiClient.isConnected) {
            fitApiClient.connect()
        }
    }

    fun disconnect() {
        if (fitApiClient.isConnected) {
            fitApiClient.disconnect()
        }
    }
}


