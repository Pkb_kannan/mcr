package com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.previous


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.mcr.active.R
import com.mcr.active.databinding.PreviousDawsRcBinding
import com.mcr.active.model.PreviousDawsModel.PreviousDawsItem
import com.orhanobut.hawk.Hawk


class PreviousDraws : Fragment() {

    var viewmodel: PreviousDrawsViewModel? = null
    var binding: PreviousDawsRcBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Hawk.put("url","normal")
        viewmodel = ViewModelProviders.of(activity!!).get(PreviousDrawsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater,R.layout.previous_daws_rc,container,false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.getRoot()
    }

    private fun initObservables() {
//        binding?.txtKudos?.text =  Hawk.get("Kudos","0")
        viewmodel?.fetchRewards()?.observe(
            this,
            Observer<List<PreviousDawsItem>?> { challenfList ->
                if (challenfList!!.size == 0) {
                } else {
                    binding?.recyPrice?.layoutManager = GridLayoutManager(context,1)
//                    binding?.recyPrice?.adapter = RewardAdapter(context,viewmodel!!,challenfList)
                    binding?.recyPrice?.adapter =
                        context?.let {
                            DawsAdapter(
                                it,
                                viewmodel!!,
                                challenfList
                            )
                        }
                    viewmodel!!.setNotificationInAdapter(challenfList)
                }
            })
    }

    }




