package com.mcr.active.ui.fragment.login

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.navigation.Navigation
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mcr.active.MCRApp
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.base.BaseViewModel
import com.mcr.active.model.favouritModel.removeModel.RemoveFavModel
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.network.Api
import retrofit2.Call
import retrofit2.Response
import java.io.IOException
import java.util.regex.Pattern
import javax.inject.Inject


class LoginViewModel(application: Application) : BaseViewModel(application) {

    @Inject
    lateinit var api: Api
    var error: String? = null
    var userData: LoginModel = LoginModel()
    var btnSelected: ObservableBoolean? = null
    var email: ObservableField<String>? = null
    var password: ObservableField<String>? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    var launchMainActivity: SingleLiveEvent<Boolean>? = null

    init {

        email = ObservableField("")
        password = ObservableField("")
        btnSelected = ObservableBoolean(false)
        launchMainActivity = SingleLiveEvent<Boolean>()
    }


    fun Login() {
        isProgress.set(View.VISIBLE)
        if (email?.get()!!.isNotEmpty()) {
            if (isEmailValid(email?.get()!!)) {
                if (password?.get()!!.toString().length < 6 && !isPasswordValid(password?.get()!!)) {
                    isProgress.set(View.GONE)
                    Toast.makeText(MCRApp.instance, "Invalid Password", Toast.LENGTH_SHORT).show()
                } else {
//                launchMainActivity?.value = true
                    getUser()
                }
            } else {
                isProgress.set(View.GONE)
                Toast.makeText(MCRApp.instance, "Invalid Email", Toast.LENGTH_SHORT).show()
            }
        } else {
            isProgress.set(View.GONE)
            Toast.makeText(MCRApp.instance, "Please Enter an Email_id", Toast.LENGTH_SHORT).show()
        }

    }


    fun onPasswordChanged(s: CharSequence, start: Int, befor: Int, count: Int) {
        btnSelected?.set(Util.isEmailValid(email?.get()!!) && s.toString().length >= 8)

    }

    fun setErrorMessage(view: EditText, errorMessage: String?) {
        view.setError(errorMessage)
    }


    fun isEmailValid(email: String): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    fun isPasswordValid(password: String): Boolean {
        val expression = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#\$%^&+=!])(?=\\\\S+\$).{4,}\$";
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(password)
        return matcher.matches()
    }


    fun getUser() {
        isProgress.set(View.VISIBLE)
        val call = api.getUser(
            Util.getBaseUrl() + "member_login",
            email?.get()!!.toString(),
            password?.get()!!.toString()
        )

        call?.enqueue(object : retrofit2.Callback<LoginModel> {
            override fun onFailure(call: Call<LoginModel>?, t: Throwable?) {
                Log.e("response", t?.message)

                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<LoginModel>?,
                response: Response<LoginModel>?
            ) {
                val gson = Gson()
                if (response?.code() == 200) {
                    userData = response?.body()!!
                    val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                    Log.e("list_value", jsonElement.toString())
                    isProgress.set(View.GONE)
                    email = ObservableField("")
                    password = ObservableField("")
                    launchMainActivity?.value = true
                } else {
                    try {
                        val loginError: RemoveFavModel = gson.fromJson(
                            response!!.errorBody()!!.string(),
                            RemoveFavModel::class.java
                        )
                        Toast.makeText(getApplication(), loginError.message, Toast.LENGTH_SHORT)
                            .show()
                        isProgress.set(View.GONE)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }

            }
        })
    }

}