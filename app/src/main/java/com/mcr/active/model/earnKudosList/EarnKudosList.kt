package com.mcr.active.model.earnKudosList


import com.google.gson.annotations.SerializedName


data class EarnKudosList(

	@field:SerializedName("data")
	val data: List<KudosDataItem>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)