package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.info

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.customView.RecyclerItemClickListenr
import com.mcr.active.databinding.LocationInfoBottomSheetBinding
import com.mcr.active.model.favouritModel.favLeisureModel.AmenityFeatureItem
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.LocationWhiteViewModel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.PlaceInfoAdapter
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.timeTable.DayAdapter
import com.orhanobut.hawk.Hawk
import java.util.*

@SuppressLint("ByteOrderMark")
class InfoBottomSheetSplashFragment : BottomSheetDialogFragment() {

    var viewmodel: LocationWhiteViewModel? = null
    var binding: LocationInfoBottomSheetBinding? = null
    private var fragmentView: View? = null
    var list: List<String>? = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CoffeeDialog)
        viewmodel = ViewModelProviders.of(activity!!).get(LocationWhiteViewModel::class.java)
        Util.hawk(this.context!!)
        Hawk.put("url","imin")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.location_info_bottom_sheet, container, false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.getRoot()
    }


    private fun initObservables() {

        val list = ArrayList<String>(Arrays.asList(*resources.getStringArray(R.array.dayList)))
        viewmodel?.list = ArrayList<String>(Arrays.asList(*resources.getStringArray(R.array.weekName)))

        viewmodel?.selectedItem?.clear()
        binding?.recyDay?.layoutManager = GridLayoutManager(context,7)
//        binding?.recyDay?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL ,false)
        binding?.recyDay?.adapter =
            DayAdapter(viewmodel!!, list)
        viewmodel!!.setDayInAdapter(list)

        viewmodel?.FacilityList()

        this!!.context?.let {
            RecyclerItemClickListenr(it, binding!!.recyDay, object : RecyclerItemClickListenr.OnItemClickListener {

                override fun onItemClick(view: View, position: Int) {
                    //do your work here..

                    viewmodel?.dayClick(position,viewmodel?.list!!.get(position))
                    binding?.recyDay?.adapter?.notifyDataSetChanged()
                }

                override fun onItemLongClick(view: View?, position: Int) {
                    TODO("do nothing")
                }
            })
        }?.let { binding?.recyDay?.addOnItemTouchListener(it) }

        viewmodel?.workingDays?.observeForever(Observer {
            if (it!!){

                binding?.recyDay?.adapter?.notifyDataSetChanged()
                viewmodel?.workingDays?.value = false
            }
        })
        binding?.imgClose?.setOnClickListener(View.OnClickListener {
            dismiss()
        })


        viewmodel?.FacilityFeature?.observe(
            this,
            Observer<List<AmenityFeatureItem?>?> { locationList ->
                if (locationList==null || locationList!!.size == 0) {
                }
                else{
                    binding?.recyFacility?.layoutManager = GridLayoutManager(context,2)
                    binding?.recyFacility?.adapter =
                        PlaceInfoAdapter(
                            viewmodel!!,
                            locationList
                        )
                    viewmodel!!.setFacilitAdapter(locationList)
                }
            })
    }





}