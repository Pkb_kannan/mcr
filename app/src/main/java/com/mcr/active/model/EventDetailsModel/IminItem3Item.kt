package com.mcr.active.model.EventDetailsModel


import com.google.gson.annotations.SerializedName


data class IminItem3Item(

	@field:SerializedName("inScheme")
	val inScheme: String? = null,

	@field:SerializedName("imin:resultCount")
	val iminResultCount: Int? = null,

	@field:SerializedName("prefLabel")
	val prefLabel: String? = null,

	@field:SerializedName("definition")
	val definition: String? = null,

	@field:SerializedName("beta:publisher")
	val betaPublisher: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)