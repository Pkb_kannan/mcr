package com.mcr.active.Util

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.mcr.active.R
import com.mcr.active.model.favouritModel.favLeisureModel.EventItem
import com.mcr.active.model.favouritModel.favouritrIminItemModel.SubEventItem1
import com.orhanobut.hawk.Hawk
import com.orhanobut.hawk.HawkBuilder
import com.orhanobut.hawk.LogLevel
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


class Util {
    companion object {
        private val RECORD_REQUEST_CODE = 101
        var permissionsRequired = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        fun isEmailValid(email: String): Boolean {
            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(email)
            return matcher.matches()
        }
        fun hawk(context : Context){
            Hawk.init(context)
                .setEncryptionMethod(HawkBuilder.EncryptionMethod.MEDIUM)
                .setStorage(HawkBuilder.newSqliteStorage(context))
                .setLogLevel(LogLevel.FULL)
                .build()
        }

        fun getTime(): String {
            val timeInMillis = System.currentTimeMillis()
            val cal1: Calendar = Calendar.getInstance()
            cal1.setTimeInMillis(timeInMillis)
            val dateFormat = SimpleDateFormat("hh:mm")
            val dateforrow = dateFormat.format(cal1.getTime())
            return dateforrow
        }

        fun getDateTime(): String {
            val timeInMillis = System.currentTimeMillis()
            val cal1: Calendar = Calendar.getInstance()
            cal1.setTimeInMillis(timeInMillis)
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            val dateforrow = dateFormat.format(cal1.getTime())
            return dateforrow
        }

        fun getDayname(date:List<String>): String {
            val inFormat = SimpleDateFormat("yyy-MM-dd")
            val parts = date
            val date = inFormat.parse(parts?.get(0)?.toString())
            val outFormat = SimpleDateFormat("EEEE")
            val goal = outFormat.format(date)
            val inputFormat: DateFormat = SimpleDateFormat("HH:mm:ss")
            val outputFormat: DateFormat = SimpleDateFormat("KK:mm a")
            return goal + " "+outputFormat.format(inputFormat.parse(parts?.get(1)?.replace("Z","")))
        }

        fun getDayname1(date: List<SubEventItem1?>?): String {
            var date_time: String? = ""
                val inFormat = SimpleDateFormat("yyy-MM-dd")
                val parts = date?.get(0)?.startDate
                val date = inFormat.parse(parts!!.split("T")[0])
                val outFormat = SimpleDateFormat("EEE MMM-dd-yy")
                val goal = outFormat.format(date)
                val outputFormat: DateFormat =
                    SimpleDateFormat("KK:mm a")
                date_time += goal+","

            return date_time!!
        }
        fun getDaynameLeisure(date: List<EventItem?>?): String {

            var date_time: String? = ""
                val inFormat = SimpleDateFormat("yyy-MM-dd")
                val parts = date?.get(0)?.startDate
                val date = inFormat.parse(parts!!.split("T")[0])
                val outFormat = SimpleDateFormat("EEE MMM-dd-yy")
                val goal = outFormat.format(date)
                val outputFormat: DateFormat =
                    SimpleDateFormat("KK:mm a")
                date_time += goal+","
            return date_time!!

        }
        fun getEndTime(date:List<String>): String {

            val inFormat = SimpleDateFormat("yyy-MM-dd")
            val parts = date
            val date = inFormat.parse(parts?.get(0)?.toString())
            val inputFormat: DateFormat = SimpleDateFormat("HH:mm:ss")
            val outputFormat: DateFormat =
                SimpleDateFormat("KK:mm a")
            return " "+outputFormat.format(inputFormat.parse(parts?.get(1)?.replace("Z","")))

        }
        fun getcheck_out_time(start : String,end : String): String {
            val inFormat = SimpleDateFormat("yyy-MM-dd")
            val date = start.split("T")
            val date1 = end.split("T")
            val inputFormat: DateFormat = SimpleDateFormat("HH:mm:ss")
            val outputFormat: DateFormat =
                SimpleDateFormat("KK:mm a")
            if (date[0].equals(date1[0]))
                return date[0]+" "+date[1].replace(":00Z","")+" - "+date1[1].replace(":00Z","").toString()
            else
                return date[0]+" "+date[1].replace(":00Z","")+","+date1[0]+" "+date1[1].replace(":00Z","").toString()
        }

        fun getDistance(distance : Double): String {
            return (BigDecimal(0.6214 * distance).setScale(2, RoundingMode.HALF_EVEN)).toString()
        }

        fun getIminToken(): String {
            return "dwlR7D50eTzWYGQnmzUZ4hD7Gu2KtxIj"
        }
        fun getBaseUrl(): String {
            return "https://www.kineticinsightpro.com/index.php/api/mcr_api/member/"
//            return "http://192.168.1.100/kinetic_3/index.php/api/mcr_api/member/"
        }
        fun getIminUrl(): String {
            return "https://search.imin.co/events-api/v2/places?"
        }
        fun getIminEventUrl(): String {
            return "https://search.imin.co/events-api/v2/event-series?"
        }
        fun getCurrentWeeknae(): String? {
            val calendar = Calendar.getInstance()
            val date = calendar.time
            return "schema:"+SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.time)
        }


        fun milToKm(miles : String): String? {

            val distance = miles.toDouble() * 1.609344
            return BigDecimal(distance).setScale(2, RoundingMode.HALF_EVEN).toString()
        }

        fun setupPermissions(context : Context): Boolean {
            val permission = ContextCompat.checkSelfPermission(context,
                android.Manifest.permission.CALL_PHONE)

            if (permission != PackageManager.PERMISSION_GRANTED) {
                return false
            }
            else{
                return true
            }
        }

        fun makeRequest(context : Activity) {
            ActivityCompat.requestPermissions(context,
                arrayOf(android.Manifest.permission.CALL_PHONE),
                RECORD_REQUEST_CODE)
        }

        fun setupPermissionsGallery(context : Context): Boolean {
            val permission = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)

            return permission == PackageManager.PERMISSION_GRANTED
        }

        fun makeRequestGallery(context : Activity) {
            ActivityCompat.requestPermissions(context,
                permissionsRequired,
                102)
        }

        fun setupPermissionsLocation(context : Context): Boolean {
            val permission = ContextCompat.checkSelfPermission(context,
                android.Manifest.permission.ACCESS_FINE_LOCATION)

            if (permission != PackageManager.PERMISSION_GRANTED) {
                Log.i(ContentValues.TAG, "Permission to record denied")
//                makeRequest(context)
                return false
            }
            else{
                return true
            }
        }

        fun makeRequestLocation(context : Activity) {
            ActivityCompat.requestPermissions(context,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                100)
        }
        fun todyOpen(timeto : String , timefrom : String):String{

            var time  = timeto!!.split("T")[1].replace(":00Z","")
              time  += " - "+timefrom!!.split("T")[1].replace(":00Z","")

            return time
        }

        fun getImageUri(
            inContext: Context,
            inImage: Bitmap
        ): Uri? {
            val bytes = ByteArrayOutputStream()
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
            val path = MediaStore.Images.Media.insertImage(
                inContext.contentResolver,
                inImage,
                "Title",
                null
            )
            return Uri.parse(path)
        }

        fun getRealPathFromURI(uri: Uri?,inContext: Context): String? {
            val cursor: Cursor =
                inContext.getContentResolver().query(uri!!, null, null, null, null)!!
            cursor.moveToFirst()
            val idx =
                cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            return cursor.getString(idx)
        }

        fun getRequest(
            paramName: String?,
            file: File
        ): MultipartBody.Part? {
            return MultipartBody.Part.createFormData(
                paramName!!,
                file.name,
                imageData(file)!!
            )
        }

        fun imageData(file: File?): RequestBody? {
            return getImageBody("multipart/form-data", file)
        }

        fun getImageBody(type: String?, file: File?): RequestBody? {
            return RequestBody.create(type!!.toMediaTypeOrNull(), file!!)
        }

        fun plain(content: String?): RequestBody? {
            return getRequestBody("text/plain", content)
        }

        fun getRequestBody(
            type: String?,
            content: String?
        ): RequestBody? {
            return RequestBody.create(type!!.toMediaTypeOrNull(), content!!)
        }



        /**
         * This method will checks whether the device is connected to internet or not
         *
         * @param context the context from where is method called
         * @return true if connected else false
         *
         */
        fun isNetworkConnected(context: Context): Boolean {

            val connMgr =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
            val mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)

//            return wifi != null && wifi.isConnectedOrConnecting || mobile != null && mobile.isConnectedOrConnecting

            val isConnected =
                wifi != null && wifi.isConnectedOrConnecting || mobile != null && mobile.isConnectedOrConnecting
            if (isConnected) {
            return true
                Log.d("Network Available ", "YES")
            } else  {
                Log.d("No Network Available ", "NO")
                return false
            }
        }
    }

}