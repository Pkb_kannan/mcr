package com.mcr.active.model.favouritModel.favActivityListModel


import com.google.gson.annotations.SerializedName


data class FavActivityListModel(

	@field:SerializedName("data_activity")
	val dataActivity: List<DataActivityItem>? = null,

	@field:SerializedName("data_leisure")
	val dataLeisure: List<DataLeisureItem>? = null,

	@field:SerializedName("data_park")
	val dataPark: List<DataParkItem>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)