package com.mcr.active.ui.fragment.findActivity.findActivityResult.findActivityList

import android.app.Dialog
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.mcr.active.R
import com.mcr.active.customView.RecyclerItemClickListenr
import com.mcr.active.databinding.FindActivityDetailsBinding
import com.mcr.active.databinding.FindActivityListBinding
import com.mcr.active.model.LocationDetailModel.LocationDetailsModel
import com.mcr.active.model.findActivityListModel.IminItemItem
import com.mcr.active.ui.fragment.home.HomeViewmodel


class FindActivityList : Fragment() {

    var viewmodel1: HomeViewmodel? = null
    var locationId: String = null.toString()
    var viewmodel: FindListViewModel? = null
    var binding: FindActivityListBinding? = null
    var binding1: FindActivityDetailsBinding? = null
    private var LocationiList: MutableLiveData<List<IminItemItem>> =
        MutableLiveData<List<IminItemItem>>()
    private var LocationDetailsLive: MutableLiveData<LocationDetailsModel> =
        MutableLiveData<LocationDetailsModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewmodel = ViewModelProviders.of(activity!!).get(FindListViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.find_activity_list, container, false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.getRoot()
    }


    private fun initObservables() {

        viewmodel?.listLocation()?.observe(
            this,
            Observer<List<IminItemItem>?> { IminItemItem ->
                if (IminItemItem!!.size == 0) {
                } else {
                    binding?.recyPlace?.layoutManager = GridLayoutManager(context, 1)
                    binding?.recyPlace?.adapter = ActivityListAdapter(
                        viewmodel!!, IminItemItem
                    )
                    LocationiList.value = IminItemItem
                    viewmodel!!.setLocationInAdapter(IminItemItem)
                }
            })

        viewmodel?.loadComplect1?.observe(this, Observer {
            if (it!!) {
                LocationDetailsLive = viewmodel?.LocationDetails()!!
                Details(LocationDetailsLive)
            }

        })

        this!!.context?.let {
            RecyclerItemClickListenr(
                it,
                binding!!.recyPlace,
                object : RecyclerItemClickListenr.OnItemClickListener {

                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..

                        viewmodel?.searchDetails(
                            LocationiList.value?.get(position)?.identifier.toString(),
                            LocationiList.value?.get(position)!!.iminLocationSummary?.get(0)?.id.toString()

                        )
                        locationId =
                            LocationiList.value?.get(position)!!.iminLocationSummary?.get(0)
                                ?.id.toString()

                    }

                    override fun onItemLongClick(view: View?, position: Int) {
                        TODO("do nothing")
                    }
                })
        }?.let { binding?.recyPlace?.addOnItemTouchListener(it) }

    }

    fun Details(details: MutableLiveData<LocationDetailsModel>) {

        val dialog = Dialog(this.context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        viewmodel = ViewModelProviders.of(activity!!).get(FindListViewModel::class.java)
        binding1 = DataBindingUtil.inflate(
            LayoutInflater.from(dialog.context),
            R.layout.find_activity_details,
            null,
            false
        )
        binding1?.root?.let { dialog.setContentView(it) }
//        dialog .setContentView(R.layout.home_dialog)
        binding1?.viewModel = viewmodel

        binding1?.name?.text = details.value?.name
        binding1?.txtDiscription?.text = details.value?.description
        binding1?.txtOfferPrice?.text =
            "Non - member - £" + details.value?.iminAggregateOffer?.publicAdult?.price.toString()
        binding1?.txtPrice?.text =
            "Adult : " + details.value?.iminAggregateOffer?.publicAdult?.price.toString()
        binding1?.txtOrgName?.text = details.value?.organizer?.name
        if (details.value?.organizer?.email != null) {
            binding1?.imgMail?.visibility = VISIBLE
            binding1?.txtEmail?.text = details.value?.organizer?.email
        } else {
            binding1?.imgMail?.visibility = GONE
        }

        binding1?.txtUrl?.text = details.value?.organizer?.url
        binding1?.txtUrl?.paintFlags =
            binding1?.txtUrl?.getPaintFlags()?.or(Paint.UNDERLINE_TEXT_FLAG)!!

        for (n in details.value?.iminLocationSummary?.indices!!) {
            if (details.value?.iminLocationSummary?.get(n)?.name.equals(locationId)) {
                binding1?.txtLocationName?.setText(details.value?.iminLocationSummary!![n]?.name)
                binding1?.txtLocation?.setText(details.value?.iminLocationSummary!![n]?.address?.iminFullAddress)
            }
        }
        binding1?.imgClose?.setOnClickListener(View.OnClickListener {
            dialog.cancel()
        })

        dialog.show()
    }


}