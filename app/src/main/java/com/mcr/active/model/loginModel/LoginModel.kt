package com.mcr.active.model.loginModel


import com.google.gson.annotations.SerializedName


data class LoginModel(
	@field:SerializedName("status")
	val status: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("member_id")
	val memberId: String? = null,

	@field:SerializedName("member_email")
	val memberEmail: String? = null,

	@field:SerializedName("address_one")
	val addressOne: String? = null,

	@field:SerializedName("member_firstname")
	val memberFirstname: String? = null,

	@field:SerializedName("member_lastname")
	val memberLastname: String? = null,

	@field:SerializedName("address_two")
	val addressTwo: Any? = null,

	@field:SerializedName("total_kudos")
	val totalKudos: Any? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("steps")
	val steps: String? = null,

	@field:SerializedName("booking")
	val booking: String? = null,

	@field:SerializedName("activities")
	val activities: String? = null,
	@field:SerializedName("member_image")
	val profiPic: String? = null


)