package com.mcr.active.ui.fragment.findActivity

import `in`.madapps.placesautocomplete.PlaceAPI
import `in`.madapps.placesautocomplete.adapter.PlacesAutoCompleteAdapter
import `in`.madapps.placesautocomplete.listener.OnPlacesDetailsListener
import `in`.madapps.placesautocomplete.model.PlaceDetails
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.databinding.adapters.SeekBarBindingAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import com.jaygoo.widget.SeekBar
import com.mcr.active.R
import com.mcr.active.databinding.FindActivitiesBinding
import com.mcr.active.ui.fragment.findActivity.FindDirections.actionNavLocationToNavFindactivityResult
import com.mcr.active.ui.fragment.home.HomeDirections
import com.orhanobut.hawk.Hawk
import java.util.*
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.mcr.active.Util.Util
import com.mcr.active.ui.fragment.findActivity.findActivityResult.FindActAdapter


class Find: Fragment() ,OnRangeChangedListener {

    var list : List<String>? = null
    var placesApi : PlaceAPI? = null
    var viewmodel: FindViewmodel? = null
    var binding: FindActivitiesBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewmodel = ViewModelProviders.of(activity!!).get(FindViewmodel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater, R.layout.find_activities,container,false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.getRoot()
    }



    private fun initObservables() {

        initActivityTypeSpinner()
        initDistanceSpinner()

        viewmodel?.strDistance = ObservableField("")
        viewmodel?.strDistanceposition = ObservableField("")
        viewmodel?.strActivityType = ObservableField("")
        viewmodel?.timeFromTo = ObservableField("TIME")
//        viewmodel?.tvText= ObservableField("")
        viewmodel?.sportName= ObservableField("")

        placesApi = PlaceAPI.Builder()
            .apiKey("AIzaSyBLxxS1iGqJMu3y2KwOnbIwvR-8xBeYuig")
            .build(this.context!!)
        binding?.tvM1?.setAdapter(PlacesAutoCompleteAdapter(this.context!!, placesApi!!))
        binding?.tvM1?.onItemClickListener =
            AdapterView.OnItemClickListener { parent, _, position, _ ->
                val place = parent.getItemAtPosition(position) as `in`.madapps.placesautocomplete.model.Place
                if (!place.id.toString().equals("-1")) {
                    binding?.tvM1?.setText(place.description)
                    getPlaceDetails(place.id)
                }
            }

//        val autocompleteFragment = fragmentManager?.findFragmentById(R.id.tv_m) as? PlaceAutocompleteFragment
//        autocompleteFragment?.setOnPlaceSelectedListener(this)

       val list = ArrayList<String>(Arrays.asList(*resources.getStringArray(com.mcr.active.R.array.dayList)))
       viewmodel?.list = ArrayList<String>(Arrays.asList(*resources.getStringArray(com.mcr.active.R.array.weekName)))
        viewmodel?.launchFindResult?.observe(this, androidx.lifecycle.Observer {
            if (it!!)
            {   Hawk.put("url","imin")
                Navigation.findNavController(this.view!!).navigate(R.id.nav_findactivity_result)
//                Navigation.findNavController(requireView()).navigate(actionNavLocationToNavFindactivityResult())
//                navController.navigate(R.id.)
            }
        })
        binding?.recyDay?.layoutManager = GridLayoutManager(context,7)
//        binding?.recyDay?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL ,false)
        binding?.recyDay?.adapter = DayAdapter(viewmodel!!, list)
        viewmodel!!.setDayInAdapter(list)

        viewmodel?.refreshDayAdapter?.observe(this, androidx.lifecycle.Observer {
            if (it!!) {
                binding?.recyDay?.adapter?.notifyDataSetChanged()
                viewmodel?.refreshDayAdapter?.value = false
            }
        })



        binding?.seekbar?.setOnRangeChangedListener(this)

//        binding?.seekbarStatus?.setOnClickListener(View.OnClickListener {
//            viewmodel?.visibility?.set(false)
//        })


    }
//    override fun onPlaceSelected(p0: Place?) {
//
//        Toast.makeText(context,""+p0!!.name+p0!!.latLng,Toast.LENGTH_LONG).show();
//    }
//
//    override fun onError(status: Status) {
//        Toast.makeText(context,""+status.toString(),Toast.LENGTH_LONG).show();
//    }

    override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
    }

    override fun onRangeChanged(
        view: RangeSeekBar?,
        leftValue: Float,
        rightValue: Float,
        isFromUser: Boolean
    ) {
//        Log.e("Progress", ""+leftValue.toInt())
        if(isFromUser) {
            if (leftValue.toInt() == 9 && rightValue.toInt() == 9)
                viewmodel?.timeFromTo?.set("09:00 - 09:00")

//        else
//        viewmodel?.timeFromTo?.set(""+leftValue.toInt()+":00 - "+rightValue.toInt()+":00")
            else if (leftValue.toInt() == 9)
                viewmodel?.timeFromTo?.set("09:00 - " + rightValue.toInt() + ":00")
            else if (leftValue.toInt() == 0 && rightValue.toInt() == 0)
                viewmodel?.timeFromTo?.set("TIME")
            else
                viewmodel?.timeFromTo?.set("" + leftValue.toInt() + ":00 - " + rightValue.toInt() + ":00")
        }

    }

    override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
    }

    private fun getPlaceDetails(placeId: String) {
        placesApi?.fetchPlaceDetails(placeId, object :
            OnPlacesDetailsListener {
            override fun onError(errorMessage: String) {
                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
            }

            override fun onPlaceDetailsFetched(placeDetails: PlaceDetails) {
                viewmodel?.tvText?.set(placeDetails.lat.toString()+","+placeDetails.lng.toString())

//                setupUI(placeDetails)
            }
        })
    }

    private fun initActivityTypeSpinner() {


        val spinnerTypeList = ArrayList<String>(Arrays.asList(*resources.getStringArray(com.mcr.active.R.array.Activity_type)))

//        val dropDownHeight = binding?.spinnerActType!!.getLayoutParams().height + 100
//        binding?.spinnerActType!!.setDropDownVerticalOffset(dropDownHeight)
        binding?.spinnerActType!!.adapter= FindActAdapter(this.requireActivity(),spinnerTypeList)

        binding?.spinnerActType?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onNothingSelected(adapterView: AdapterView<*>?) {

                }
                override fun onItemSelected(
                    adapterView: AdapterView<*>?,
                    p1: View?,
                    pos: Int,
                    p3: Long
                ) {
                    binding!!.txtType.text= adapterView?.selectedItem.toString()
                    viewmodel?.strActivityType?.set(adapterView?.selectedItem.toString())
                }

            }

    }

    private fun initDistanceSpinner() {
        val distanceList = ArrayList<String>(Arrays.asList(*resources.getStringArray(com.mcr.active.R.array.Distance)))
        binding?.distanceSpinner!!.adapter= FindActAdapter(this.requireActivity(),distanceList)

        binding?.distanceSpinner?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onNothingSelected(adapterView: AdapterView<*>?) {

                }
                override fun onItemSelected(
                    adapterView: AdapterView<*>?,
                    p1: View?,
                    pos: Int,
                    p3: Long
                ) {
                    binding!!.txtDistance.text= adapterView?.selectedItem.toString()

                    viewmodel?.strDistance?.set(pos.toString())
                }

            }
    }

}