package com.mcr.active.ui.fragment.newsAndEvents

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.customView.RecyclerItemClickListenr
import com.mcr.active.databinding.NewsEventsBinding
import com.mcr.active.ui.MainActivity
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.search.SearchSpinAdapter
import com.orhanobut.hawk.Hawk


class NewsAndEvents : Fragment() {

    var viewmodel: NewsViewModel? = null
    var binding: NewsEventsBinding? = null

    companion object {
        var eventId = "0"
            private set


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Util?.hawk(this.context!!)
        Hawk.put("url", "imin")
//        Log.e("url0", Hawk.get("url","imin"))
        viewmodel = ViewModelProviders.of(activity!!).get(NewsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.news_events, container, false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.root

    }


    private fun initObservables() {




        viewmodel?.listComplect?.observeForever {
            if (it){
//                viewmodel?.listComplect!!.value = false

                viewmodel?.headText?.set(viewmodel?.list?.get(0)).toString()
                val spinnerAdapter = context?.let { SearchSpinAdapter(it, viewmodel?.list!!) }
                // Set layout to use when the list of choices appear
                spinnerAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                binding?.spinner?.setAdapter(spinnerAdapter)
            }
        }




        binding?.spinner?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onNothingSelected(adapterView: AdapterView<*>?) {
                    Toast.makeText(context, "Nothing Selected", Toast.LENGTH_SHORT).show()
                }

                override fun onItemSelected(
                    adapterView: AdapterView<*>?,
                    p1: View?,
                    pos: Int,
                    p3: Long
                ) {
                   binding?.txtSpinner?.text = viewmodel?.list!!.get(pos).toString()
                    viewmodel?.headText?.set(viewmodel?.list!!.get(pos)).toString()
                    binding?.recyNews?.removeAllViews()
                    viewmodel?.type?.set(pos)
                    if (pos==0) {

                        binding?.recyNews?.layoutManager = GridLayoutManager(context, 1)
                        binding?.recyNews?.adapter = NewsEventsAdapter(context!!,
                            viewmodel!!,
                            viewmodel?.featuredList,null,null
                        )
                        viewmodel!!.setFacilitAdapter(viewmodel?.featuredList,null,null)
                    }
                    else if (pos==1){
                        binding?.recyNews?.layoutManager = GridLayoutManager(context, 1)
                        binding?.recyNews?.adapter = NewsEventsAdapter(
                            context!!,
                            viewmodel!!,
                            null,viewmodel?.newsList,null
                        )
                        viewmodel!!.setFacilitAdapter(null,viewmodel?.newsList,null)
                    }
                    else{
                        binding?.recyNews?.layoutManager = GridLayoutManager(context, 1)
                        binding?.recyNews?.adapter = NewsEventsAdapter(context!!,
                            viewmodel!!, null,null,viewmodel?.eventList
                        )
                        viewmodel!!.setFacilitAdapter(null,null,viewmodel?.eventList)
                    }
                    (binding?.recyNews?.adapter as NewsEventsAdapter).notifyDataSetChanged()


                }
            }
//        viewmodel?.listComplectDetails?.observeForever {
//            if (it) {
//               binding?.testing?.setText(viewmodel?.listDetails?.title)
//            }
//        }
        viewmodel?.listComplectDetails?.observeForever {
            if (it) {
                viewmodel?.listComplectDetails?.value = false
//                dinding()
                MainActivity.navController?.navigate(R.id.news_a_events_details)

            }
        }

        this!!.context?.let {
            RecyclerItemClickListenr(it, binding!!.recyNews, object : RecyclerItemClickListenr.OnItemClickListener {

                override fun onItemClick(view: View, position: Int) {
                    //do your work here..
                    if (viewmodel?.type?.get()==0){
                        eventId = viewmodel?.featuredList!![position]?.id.toString()
                    }
                    else if (viewmodel?.type?.get()==1)
                        eventId = viewmodel?.newsList!![position]?.id.toString()
                    else
                        eventId = viewmodel?.eventList!![position]?.id.toString()
                    viewmodel?.getListDetails(eventId)

                }

                override fun onItemLongClick(view: View?, position: Int) {
                    TODO("do nothing")
                }
            })
        }?.let { binding?.recyNews?.addOnItemTouchListener(it) }


    }
}

