package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.favourit

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.databinding.FavouritActivityBottomItemBinding
import com.mcr.active.model.favouritModel.favouritrIminItemModel.FavActivityModel


class FavouritActivityAdapter(
    private val viewModel: FavouritViewModel,
    private var items: List<FavActivityModel?>
) : RecyclerView.Adapter<FavouritActivityAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = FavouritActivityBottomItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemCount(): Int {
        if (items.size == 0) {
            return 0
        } else {
            return items.size
        }
    }

    fun setFav(
        noti: List<FavActivityModel?>
    ) {
        this.items = noti
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: FavouritActivityBottomItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            viewModel: FavouritViewModel?, position: Int?
        ) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

