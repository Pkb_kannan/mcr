package com.mcr.active.model.PreviousDawsModel

import com.google.gson.annotations.SerializedName


data class Response(

    @field:SerializedName("data")
	val data: List<PreviousDawsItem?>? = null,

    @field:SerializedName("total_earned_kudos")
	val totalEarnedKudos: String? = null,

    @field:SerializedName("message")
	val message: String? = null,

    @field:SerializedName("status")
	val status: Boolean? = null
)