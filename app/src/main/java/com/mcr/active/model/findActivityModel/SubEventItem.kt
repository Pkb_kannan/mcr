package com.mcr.active.model.findActivityModel


import com.google.gson.annotations.SerializedName


data class SubEventItem(

	@field:SerializedName("subEvent")
	val subEvent1: List<SubEventItem1?>? = null,

	@field:SerializedName("location")
	val location: Location? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("category")
	val category: List<String?>? = null,

	@field:SerializedName("imin:checkoutUrlTemplate")
	val iminCheckoutUrlTemplate: String? = null,

	@field:SerializedName("duration")
	val duration: String? = null,

	@field:SerializedName("endDate")
	val endDate: String? = null,

	@field:SerializedName("maximumAttendeeCapacity")
	val maximumAttendeeCapacity: Int? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("remainingAttendeeCapacity")
	val remainingAttendeeCapacity: Int? = null,

	@field:SerializedName("startDate")
	val startDate: String? = null
)