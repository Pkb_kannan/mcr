package com.mcr.active.model.getApps

import com.google.gson.annotations.SerializedName

data class GetAllAppsModel(
    @field:SerializedName("app_data")
    val data: List<AppDetailsItem>? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Boolean? = null
)