package com.mcr.active.model.FindActivityMapModel


import com.google.gson.annotations.SerializedName


data class Geo(

    @field:SerializedName("latitude")
    val latitude: Double? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("longitude")
    val longitude: Double? = null,

    @field:SerializedName("imin:distanceFromGeoQueryCenter")
    val iminDistanceFromGeoQueryCenter: IminDistanceFromGeoQueryCenter? = null
)