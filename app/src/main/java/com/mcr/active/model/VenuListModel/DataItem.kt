package com.mcr.active.model.VenuListModel


import com.google.gson.annotations.SerializedName


data class DataItem(

	@field:SerializedName("venue_name")
	val venueName: String? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("mile")
	val mile: Double? = null,

	@field:SerializedName("facilities")
	val facilities: String? = null,

	@field:SerializedName("venue_id")
	val venueId: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null
)