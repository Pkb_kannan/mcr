package com.mcr.active.model.EventDetailsModel


import com.google.gson.annotations.SerializedName


data class IminItem1Item(

	@field:SerializedName("imin:resultCount")
	val iminResultCount: Int? = null,

	@field:SerializedName("prefLabel")
	val prefLabel: String? = null,

	@field:SerializedName("definition")
	val definition: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("broader")
	val broader: List<String?>? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("topConceptOf")
	val topConceptOf: String? = null,

	@field:SerializedName("altLabel")
	val altLabel: List<String?>? = null,

	@field:SerializedName("related")
	val related: List<String?>? = null
)