package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.timeTable

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import com.mcr.active.MCRApp
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.Util.Util.Companion.getDistance
import com.mcr.active.databinding.EventBookingBinding
import com.mcr.active.databinding.LocationTimeTableBottomSheetBinding
import com.mcr.active.databinding.TimeTableDetailsBinding
import com.mcr.active.model.EventDetailsModel.IminItemItemDetails
import com.mcr.active.model.EventDetailsModel.SubEvent1Item
import com.mcr.active.ui.MainActivity
import com.mcr.active.ui.fragment.home.bottomSheetFragment.location.viewModel.TimeBottomViewModel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.LocationWhiteViewModel
import com.orhanobut.hawk.Hawk
import java.util.*

class TimeBottomSheetFragment : BottomSheetDialogFragment(),OnRangeChangedListener {


    var viewmodel: LocationWhiteViewModel? = null
    var viewmodelTime: TimeBottomViewModel? = null
    var binding: LocationTimeTableBottomSheetBinding? = null
    var binding1: TimeTableDetailsBinding? = null
    var bindingBooking: EventBookingBinding? = null
    private var fragmentView: View? = null
    var contex :Context? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CoffeeDialog)
        viewmodel = ViewModelProviders.of(activity!!).get(LocationWhiteViewModel::class.java)
        viewmodelTime = ViewModelProviders.of(activity!!).get(TimeBottomViewModel::class.java)
        Util.hawk(this.context!!)
        Hawk.put("url","imin")
        contex = this.context!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater,R.layout.location_time_table_bottom_sheet,container,false)
        binding?.viewModel = viewmodel
//        binding?.TimeVieModel = viewmodelTime
        initObservables()
        return binding?.getRoot()
    }


    private fun initObservables() {
        viewmodel?.selectedItem?.clear()
        binding?.seekbar?.setOnRangeChangedListener(this)
        val list = ArrayList<String>(Arrays.asList(*resources.getStringArray(R.array.dayList)))
        viewmodel?.list = ArrayList<String>(Arrays.asList(*resources.getStringArray(com.mcr.active.R.array.weekName)))
        binding?.recyDay?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL ,false)
        binding?.recyDay?.adapter =
            DayAdapter(viewmodel!!, list)
        viewmodel!!.setDayInAdapter(list)
        binding?.imgClose?.setOnClickListener(View.OnClickListener {
            viewmodel?.timeFromTo?.set("TIME")
            dismiss()
        })
        binding?.recyDay?.layoutManager = GridLayoutManager(context,7)
//        binding?.recyDay?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL ,false)
        binding?.recyDay?.adapter = DayAdapter(viewmodel!!, list)
        viewmodel!!.setDayInAdapter(list)
        viewmodel?.refreshDayAdapter?.observe(this, Observer {
            if (it!!) {
             binding?.recyDay?.adapter?.notifyDataSetChanged()
             viewmodel?.refreshDayAdapter?.value = false
            }
        })

        viewmodel?.timeTableLoad?.observeForever(Observer {
            if (it!!){
                binding?.rectList?.layoutManager = GridLayoutManager(context,1)
                binding?.rectList?.adapter =
                    TimeTableAdapter(
                        viewmodel!!,
                        viewmodel!!.gettimeTable()
                    )
                viewmodel!!.setTimeTableAdapter()

            }
        })

        viewmodel?.TimeTableDetailsOpen?.observeForever(Observer {
            if (it!!){
                viewmodel?.TimeTableDetailsOpen!!.value = false
                Details(viewmodel?.openDetailsDialog())
            }
        })
        viewmodel?.TimeTableBookingOpen?.observeForever(Observer {
            if (it!!){
                viewmodel?.TimeTableBookingOpen!!.value = false
                Booking(viewmodel?.openBookingDialog())
            }
        })

    }

    override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
    }

    override fun onRangeChanged(
        view: RangeSeekBar?,
        leftValue: Float,
        rightValue: Float,
        isFromUser: Boolean
    ) {
        if (leftValue.toInt()==9 && rightValue.toInt()==9)
            viewmodel?.timeFromTo?.set("09:00 - 09:00")

        else if (leftValue.toInt()==9 )
            viewmodel?.timeFromTo?.set("09:00 - "+rightValue.toInt()+":00")

        else
            viewmodel?.timeFromTo?.set(""+leftValue.toInt()+":00 - "+rightValue.toInt()+":00")
    }

    override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
    }

    fun Details(details: IminItemItemDetails?) {

        val dialog = Dialog(contex!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding1 = DataBindingUtil.inflate(
            LayoutInflater.from(dialog.context),
            R.layout.time_table_details,
            null,
            false
        )
        binding1?.root?.let { dialog.setContentView(it) }
        binding1?.viewModel = viewmodel

        viewmodel?.TimeTableDetailsOpen?.value = false

        binding1?.name?.text = details?.name
        binding1?.txtPrice?.text = details?.iminAggregateOffer?.publicAdult?.price.toString()
        binding1?.txtLocation?.text = details?.iminLocationSummary?.get(0)?.address?.iminFullAddress
        val distance = getDistance(details?.iminLocationSummary?.get(0)?.geo?.iminDistanceFromGeoQueryCenter?.value?.toDouble()!!).toString()

        binding1?.txtLocation?.text = distance + "Miles Away"
        binding1?.txtDiscription?.text = details.description
        binding1?.imgClose?.setOnClickListener(View.OnClickListener {

            dialog.cancel()
        })
        dialog.show()
    }

    fun Booking(listBooking: List<SubEvent1Item?>?) {

        val dialog = Dialog(contex!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        viewmodel = ViewModelProviders.of(activity!!).get(LocationWhiteViewModel::class.java)
        bindingBooking = DataBindingUtil.inflate(
            LayoutInflater.from(dialog.context),
            R.layout.event_booking,
            null,
            false
        )
        bindingBooking?.root?.let { dialog.setContentView(it) }
        bindingBooking?.viewModel = viewmodel

        viewmodel?.TimeTableDetailsOpen?.value = false


        bindingBooking?.imgClose?.setOnClickListener(View.OnClickListener {

            dialog.cancel()
        })

        bindingBooking?.recyBook?.layoutManager = GridLayoutManager(dialog.context,1)
        bindingBooking?.recyBook?.adapter =
            BookingAdapter(viewmodel!!, listBooking)
        (bindingBooking?.recyBook?.adapter as BookingAdapter)?.setBookInAdapter(listBooking)
        bindingBooking?.recyBook?.adapter?.notifyDataSetChanged()

        dialog?.show()
    }


}