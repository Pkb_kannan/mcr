package com.mcr.active.model.findActivityListModel


import com.google.gson.annotations.SerializedName


data class SubEventItem1(

	val duration: String? = null,

    @field:SerializedName("endDate")
	val endDate: String? = null,

    @field:SerializedName("maximumAttendeeCapacity")
	val maximumAttendeeCapacity: Int? = null,

    @field:SerializedName("id")
	val id: String? = null,

    @field:SerializedName("remainingAttendeeCapacity")
	val remainingAttendeeCapacity: Int? = null,

    @field:SerializedName("startDate")
	val startDate: String? = null
)