package com.mcr.active.model.EventDetailsModel


import com.google.gson.annotations.SerializedName


data class EventDetailsModel(

	@field:SerializedName("imin:totalItems")
	val iminTotalItems: Int? = null,

	@field:SerializedName("view")
	val view: View? = null,

	@field:SerializedName("imin:item")
	val iminItem: List<IminItemItemDetails>? = null,

	@field:SerializedName("imin:facet")
	val iminFacet: IminFacet? = null
)