package com.mcr.active.model.FindActivityMapModel


import com.google.gson.annotations.SerializedName


data class IminItemItem(

	@field:SerializedName("geo")
	val geo: Geo? = null,

	@field:SerializedName("imin:event")
	val iminEvent: IminEvent? = null,

	@field:SerializedName("address")
	val address: Address? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("telephone")
	val telephone: String? = null
)