package com.mcr.active.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.mcr.active.MCRApp
import com.mcr.active.Util.Util
import com.mcr.active.component.DaggerViewModelInjector
import com.mcr.active.component.ViewModelInjector
import com.mcr.active.ui.MainViewModel
import com.mcr.active.ui.activity.splash.SplashViewModel
import com.mcr.active.ui.fragment.findActivity.findActivityResult.findActivitiesMap.FindMapViewModel
import com.mcr.active.ui.fragment.findActivity.FindViewmodel
import com.mcr.active.ui.fragment.findActivity.findActivityResult.findActivityList.FindListViewModel
import com.mcr.active.ui.fragment.forgotPassword.FPViewModel
import com.mcr.active.ui.fragment.home.HomeViewmodel
import com.mcr.active.ui.fragment.login.LoginViewModel
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.previous.PreviousDrawsViewModel
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.challenge.ChallengeSubViewModel
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.RewardSubViewModel
import com.mcr.active.ui.fragment.signup.RegViewModel
import com.mcr.active.ui.fragment.splashScrool.SplashScroolViewModel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.favourit.FavouritViewModel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.LocationWhiteViewModel
import com.mcr.active.injection.module.NetworkModule
import com.mcr.active.ui.fragment.fitnessApps.MyAppsViewModel
import com.mcr.active.ui.fragment.newsAndEvents.NewsViewModel


abstract class BaseViewModel(application: Application) : AndroidViewModel(application){
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is LoginViewModel -> injector.inject(this)
            is HomeViewmodel -> injector.inject(this)
            is FindViewmodel -> injector.inject(this)
            is FindMapViewModel -> injector.inject(this)
            is SplashScroolViewModel -> injector.inject(this)
            is MainViewModel -> injector.inject(this)
            is SplashViewModel -> injector.inject(this)
            is LocationWhiteViewModel -> injector.inject(this)
            is FindListViewModel -> injector.inject(this)
            is ChallengeSubViewModel -> injector.inject(this)
            is RewardSubViewModel -> injector.inject(this)
            is FavouritViewModel -> injector.inject(this)
            is RegViewModel -> injector.inject(this)
            is FPViewModel -> injector.inject(this)
            is PreviousDrawsViewModel -> injector.inject(this)
            is NewsViewModel -> injector.inject(this)
            is MyAppsViewModel -> injector.inject(this)
        }
    }

}