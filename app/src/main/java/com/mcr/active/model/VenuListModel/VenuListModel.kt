package com.mcr.active.model.VenuListModel


import com.google.gson.annotations.SerializedName


data class VenuListModel(

	@field:SerializedName("data")
	val data: List<DataItem>? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)