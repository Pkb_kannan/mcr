package com.mcr.active.ui.fragment.findActivity

import android.app.Application
import android.graphics.Color
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.navigation.Navigation
import com.mcr.active.R
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.model.favouritModel.favLeisureModel.FavLeisureListModel
import com.mcr.active.network.Api
import com.orhanobut.hawk.Hawk
import com.mcr.active.base.BaseViewModel
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap


class FindViewmodel(application: Application) : BaseViewModel(application) {

    @Inject
    lateinit var api: Api

    val color = Color.WHITE
    var bt_search= ObservableBoolean()
    var visibility= ObservableBoolean()
    var days  : ArrayList<String>? =null
    private var adapter: DayAdapter? = null
    var tvText:ObservableField<String>? = null
    var list :ArrayList<String>  ?= ArrayList()
    var sportName:ObservableField<String>? = null
    var timeFromTo : ObservableField<String>? = null
    var strDistance: ObservableField<String>? = null
    var selectedItem : ArrayList<String>?= ArrayList()
    var strActivityType: ObservableField<String>? = null
    var launchFindResult: SingleLiveEvent<Boolean>? = null
    var refreshDayAdapter: SingleLiveEvent<Boolean>? = null
    var strDistanceposition: ObservableField<String>? = null
    var launchFindMapFragment: SingleLiveEvent<Boolean>? = null


    init{

        bt_search.set(false)
        visibility.set(false)
        tvText=ObservableField("")
        sportName=ObservableField("")
        strDistance = ObservableField("")
        timeFromTo = ObservableField("TIME")
        launchFindResult=SingleLiveEvent<Boolean>()
        strActivityType = ObservableField("")
        refreshDayAdapter = SingleLiveEvent<Boolean>()
        strDistanceposition = ObservableField("")
        launchFindMapFragment = SingleLiveEvent<Boolean>()
    }

    fun timeRange(){
        if(!visibility.get())visibility.set(true)
        else visibility.set(false)
    }

    fun locationResults(){ launchFindMapFragment?.value = true}



    fun setDayInAdapter(noti: ArrayList<String>) {
        adapter?.setDayInAdapter(noti)
        adapter?.notifyDataSetChanged()
        this.days = noti
    }
    fun daybind(position : Int): String? {
        return days?.get(position)
    }

    fun distanceSelectItem(
        parent: AdapterView<*>?,
        view: View?,
        pos: Int,
        id: Long
    ) { //pos                                 get selected item position

        strDistance?.set(parent?.getSelectedItem().toString())
        strDistanceposition?.set(pos.toString())
    }

    fun activitySelectItem(
        parent: AdapterView<*>?,
        view: View?,
        pos: Int,
        id: Long
    ) { //pos                                 get selected item position

        strActivityType?.set(parent?.getSelectedItem().toString())
    }

    fun getValues(){



        var hashMap : HashMap<String, Any>
                = HashMap<String, Any> ()


        var filterList = ArrayList<HashMap<String, Any>>()


        hashMap.put("timeRange" , timeFromTo?.get().toString())
        hashMap.put("sportName" , sportName?.get().toString())
        hashMap.put("zip" , tvText?.get().toString())
        hashMap.put("distance" , strDistance?.get().toString())
        hashMap.put("actvityType" , strActivityType?.get().toString())
        hashMap.put("days" , this!!.selectedItem!!)
        filterList.add(hashMap)
        Log.e("select_value_time",hashMap["days"].toString())
        for(key in hashMap.keys){
//            println("Element at key $key : ${hashMap[key]}")
            Log.e("select_value",hashMap[key].toString())
        }

        Hawk.put("findactvities", filterList)
        if (strDistance?.get().toString().equals("0") || strDistance?.get().toString().equals(""))
            Toast.makeText(getApplication(),"Select Distance",Toast.LENGTH_SHORT).show()
        else if (tvText?.get().toString().equals(""))
            Toast.makeText(getApplication(),"Select Place",Toast.LENGTH_SHORT).show()
        else {
            selectedItem?.clear()
            launchFindResult?.value = true
        }


    }

    fun  getResultColor(Index :Int): Int {
    var color = Color.WHITE
    if (selectedItem?.contains(list?.get(Index))!!){
        color = Color.RED
    }
    else {
        color = Color.WHITE
    }
    return color
}
fun weekSelect(Index :Int){
    if (selectedItem?.contains(list?.get(Index))!!){
        selectedItem!!.remove(list?.get(Index)!!)
    }
    else {
        selectedItem!!.add(list?.get(Index)!!)
    }
    refreshDayAdapter?.value = true
}
}