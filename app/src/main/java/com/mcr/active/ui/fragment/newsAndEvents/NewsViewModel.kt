package com.mcr.active.ui.fragment.newsAndEvents

import android.app.Application
import android.os.Build
import android.text.Html
import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.base.BaseViewModel
import com.mcr.active.model.newsAndEvends.details.News
import com.mcr.active.model.newsAndEvends.details.NewsEventsDetails
import com.mcr.active.model.newsAndEvends.list.EventsItem
import com.mcr.active.model.newsAndEvends.list.FeaturedItem
import com.mcr.active.model.newsAndEvends.list.NewsEvents
import com.mcr.active.model.newsAndEvends.list.NewsItem
import com.mcr.active.network.Api
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject


class NewsViewModel(application: Application) : BaseViewModel(application) {

    @Inject
    lateinit var api: Api
    var listDetails: News = News()
    var type: ObservableField<Int>? = null
    var list: ArrayList<String> = arrayListOf()
    var newsList: List<NewsItem> = arrayListOf()
    var headText: ObservableField<String>? = null
    var eventList: List<EventsItem> = arrayListOf()
    var listComplect: SingleLiveEvent<Boolean>? = null
    var featuredList: List<FeaturedItem> = arrayListOf()
    private var NewsEventsAdadpter: NewsEventsAdapter? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    var listComplectDetails: SingleLiveEvent<Boolean>? = null

    init {

        list.clear()
        list.add("FEATURED")
        list.add("All NEWS")
        list.add("ALL EVENTS")
        type = ObservableField(0)
        headText = ObservableField("")
        listComplect = SingleLiveEvent<Boolean>()
        listComplectDetails = SingleLiveEvent<Boolean>()
        getList()
    }

    fun setFacilitAdapter(
        feac: List<FeaturedItem>?,
        newsList: List<NewsItem>?,
        eventList: List<EventsItem>?
    ) {
        NewsEventsAdadpter?.setNews(feac, newsList!!, eventList!!)
        NewsEventsAdadpter?.notifyDataSetChanged()
    }

    fun getList() {
        listComplect?.value = false
        isProgress.set(View.VISIBLE)
        val call = api.getNewsEventList(
            Util.getBaseUrl() + "news_events"
        )

        call?.enqueue(object : retrofit2.Callback<NewsEvents> {
            override fun onFailure(call: Call<NewsEvents>?, t: Throwable?) {
                Log.d("response", t?.message)
                Log.d("response", "fail")
                isProgress.set(View.GONE)
                listComplect?.value = false
            }

            override fun onResponse(
                call: Call<NewsEvents>?,
                response: Response<NewsEvents>?
            ) {
                val gson = Gson()
                val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                Log.d("list_value", jsonElement.toString())
                if (response?.isSuccessful!!) {
                    if (response.code() == 200) {
                        newsList = response.body()?.news as List<NewsItem>
                        eventList = response.body()?.events as List<EventsItem>
                        featuredList = response.body()?.featured as List<FeaturedItem>
                    }

                } else if (response.code() == 400) {

                }
                isProgress.set(View.GONE)
                listComplect?.value = true
            }
        })

    }

    fun getTitle(position: Int): String {
        if (type?.get() == 0) {
            if (featuredList != null || featuredList.size != 0) {
                return featuredList[position]?.title.toString()
            } else
                return ""
        } else if (type?.get() == 1) {
            if (newsList != null || newsList.size != 0) {
                return newsList[position]?.title.toString()
            } else
                return ""
        } else {
            if (eventList != null || eventList.size != 0) {
                return eventList[position]?.title.toString()
            } else
                return ""
        }

    }

    fun getdiscription(position: Int): String {
        if (type?.get() == 0) {
            if (featuredList != null || featuredList.size != 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    return Html.fromHtml(
                        featuredList[position]?.description,
                        Html.FROM_HTML_MODE_COMPACT
                    ).toString()
                else
                    return Html.fromHtml(featuredList[position]?.description).toString()
//               return featuredList[position]?.description.toString()
            } else
                return ""
        } else if (type?.get() == 1) {
            if (newsList != null || newsList.size != 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    return Html.fromHtml(
                        newsList[position]?.description,
                        Html.FROM_HTML_MODE_COMPACT
                    ).toString()
                else
                    return Html.fromHtml(newsList[position]?.description).toString()
//                return newsList[position]?.description.toString()
            } else
                return ""
        } else {
            if (eventList != null || eventList.size != 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    return Html.fromHtml(
                        eventList[position]?.description,
                        Html.FROM_HTML_MODE_COMPACT
                    ).toString()
                else
                    return Html.fromHtml(eventList[position]?.description).toString()
//                return eventList[position]?.description.toString()
            } else
                return ""
        }

    }

    fun head(position: Int): String {
        if (type?.get() == 0) {
            if (featuredList != null || featuredList.size != 0) {
                if (featuredList[position]?.postType.toString().equals("E"))
                    return "EVENTS"
                else
                    return "NEWS STORY"
            } else return ""
        } else if (type?.get() == 1) {

            return "NEWS STORY"
        } else {
            return "EVENTS"
        }
    }

    /*****************************details**************************************/

    fun getListDetails(id: String) {
        listComplectDetails?.value = false
        isProgress.set(View.VISIBLE)
        val call = api.getNewsEventDetails(
            Util.getBaseUrl() + "news", id
        )

        call?.enqueue(object : retrofit2.Callback<NewsEventsDetails> {
            override fun onFailure(call: Call<NewsEventsDetails>?, t: Throwable?) {
                Log.d("response", t?.message)
                isProgress.set(View.GONE)
                listComplectDetails?.value = false
            }

            override fun onResponse(
                call: Call<NewsEventsDetails>?,
                response: Response<NewsEventsDetails>?
            ) {
                val gson = Gson()
                val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                Log.d("list_value", jsonElement.toString())
                if (response?.isSuccessful!!) {
                    if (response.code() == 200) {
                        listDetails = response.body()?.news!!
                    }

                } else if (response.code() == 400) {

                }
                isProgress.set(View.GONE)
                listComplectDetails?.value = true
            }
        })

    }


}