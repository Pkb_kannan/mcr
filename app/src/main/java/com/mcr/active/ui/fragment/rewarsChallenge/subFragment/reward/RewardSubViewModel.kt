package com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward


import android.app.Application
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mcr.active.R
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.model.*
import com.mcr.active.model.favouritModel.removeModel.RemoveFavModel
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.model.rewardModels.DataItem
import com.mcr.active.model.rewardModels.RespModel
import com.mcr.active.network.Api
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.adapter.RewardAdapter
import com.orhanobut.hawk.Hawk
import com.mcr.active.base.BaseViewModel
import com.mcr.active.ui.fragment.home.Home
import com.mcr.active.ui.fragment.rewarsChallenge.RewardChallengs
import retrofit2.Call
import retrofit2.Response
import java.util.ArrayList
import javax.inject.Inject
import java.text.SimpleDateFormat


class RewardSubViewModel (application: Application) : BaseViewModel(application){

    @Inject
    lateinit var api: Api
    private var adapter: RewardAdapter? = null
    private var rewardsItem: DataItem = DataItem()
    var openDetails: SingleLiveEvent<Boolean>? = null
    var refreshAdapter: SingleLiveEvent<Boolean>? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    private var rewardList :ArrayList<DataItem> = ArrayList()
    var kidos: MutableLiveData<String> = MutableLiveData<String>()
    private val rewardsLive: MutableLiveData<List<DataItem>> = MutableLiveData()


    init {
        openDetails = SingleLiveEvent<Boolean>()
        refreshAdapter = SingleLiveEvent<Boolean>()
        kidos?.value = Hawk.get("Kudos","0")
        fetchRewards()
    }



    fun setNotificationInAdapter(noti: List<DataItem>?) {
        if (noti != null) {
            adapter?.setNotification(noti)
        }
        adapter?.notifyDataSetChanged()
    }
    fun goneUnless(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }
//    fun setVisiblity(index: Int?,type: Int?): Int {
//        if (getNotification(index)?.getseekVisible()!!){
//            if (type==1)
//            return View.VISIBLE
//            else return View.GONE
//        }
//        else {
//            if (type==1)
//                return View.GONE
//            else return View.VISIBLE
//        }
//    }
    fun getNotification(index: Int?): DataItem? {
        return if (rewardsLive.getValue() != null && index != null && rewardsLive.getValue()!!.size > index
        ) {
            rewardsLive.value?.get(index)
        } else null
    }

    fun getMaxKudos(index: Int?): Int? {
        return if (rewardsLive.getValue() != null && index != null && rewardsLive.getValue()!!.size > index
        ) {
            rewardsLive.value?.get(index)?.rewardsKudos?.toInt()
        } else null
    }
    fun getProgress(): Int? {
        return  kidos?.value?.toInt()
    }

    fun getKudosText(index: Int?): String? {
        return if (rewardsLive.getValue() != null && index != null && rewardsLive.getValue()!!.size > index
        ) {
            rewardsLive.value?.get(index)?.rewardsKudos + " KUDOS ALLOCATED TO THIS PRICE DRAW"
        } else null
    }



    fun getCloseDateText(index: Int?): String? {
        return if (rewardsLive.getValue() != null && index != null && rewardsLive.getValue()!!.size > index
        ) {
            val inputFormat = SimpleDateFormat("yyyy-MM-dd")
            val outputFormat = SimpleDateFormat("dd/MM/yyyy")
            val inputDateStr = rewardsLive.value?.get(index)?.rewardsCloseDate
            val date = inputFormat.parse(inputDateStr)
            val outputDate = outputFormat.format(date)
            val claseTime=rewardsLive.value?.get(index)?.rewardsCloseTime
            "Entry closes at $claseTime on $outputDate"

        } else null
    }


    fun fetchRewards() : MutableLiveData<List<DataItem>>{
        isProgress.set(View.VISIBLE)
        val member_id = Hawk.get("userData", LoginModel())
        val call = api.getRewards(Util.getBaseUrl()+"getrewards",member_id.memberId.toString())
        call.enqueue(object :retrofit2.Callback<RespModel> {
            override fun onFailure(call: Call<RespModel>, t: Throwable) {
                Log.e("response", t?.message)
                isProgress.set(View.GONE)
                Toast.makeText(getApplication(), "Connection failed !!!", Toast.LENGTH_LONG).show()
            }
            override fun onResponse(call: Call<RespModel>, response: Response<RespModel>) {

                if (response.isSuccessful && response.body() != null) {
                    isProgress.set(View.GONE)
                    rewardList= response.body()!!.data as ArrayList<DataItem>
                    rewardsLive.value=rewardList
                    kidos?.value = response.body()!!.totalKudos
                    Hawk.put("Kudos",kidos?.value.toString())

                } else {
                }

            }

        })
        return rewardsLive
    }

    fun setVisiblity(index: Int?): Int {
        if ( rewardsLive.value?.get(index!!)!= null && rewardsLive.value?.get(index!!)?.rewardsKudos?.toInt()!! >= Hawk.get("Kudos","0").toInt()){
            return View.VISIBLE
        }
        else {
            return View.GONE
        }
    }
    fun setVisiblityEnterDraw(index: Int?): Int {
        if ( rewardsLive.value?.get(index!!)!= null && rewardsLive.value?.get(index!!)?.rewardsKudos?.toInt()!! < Hawk.get("Kudos","0").toInt() && rewardsLive.value?.get(index!!)?.status.equals("0")){
            return View.VISIBLE
        }
        else {
            return View.GONE
        }
    }
    fun setVisiblityAlreadyEnter(index: Int?): Int {
        if ( rewardsLive.value?.get(index!!)!= null && rewardsLive.value?.get(index!!)?.status.equals("1") && rewardsLive.value?.get(index!!)?.rewardsKudos?.toInt()!! < Hawk.get("Kudos","0").toInt()){
            return View.VISIBLE
        }
        else {
            return View.GONE
        }
    }
    fun enterDraw(index : Int){
        openDetails?.value = true
        rewardsItem = rewardsLive.value!![index]
    }
    fun spentKudos(index : Int){
        isProgress.set(View.VISIBLE)
        val member_id = Hawk.get("userData", LoginModel())
        val call = api.spentKudos(Util.getBaseUrl()+"spendkudos",member_id.memberId.toString(),rewardsLive.value!![index].rewardsId.toString(),rewardsLive.value!![index].rewardsKudos.toString())
        call.enqueue(object :retrofit2.Callback<RemoveFavModel> {
            override fun onFailure(call: Call<RemoveFavModel>, t: Throwable) {
                Log.e("response", t?.message)
                isProgress.set(View.GONE)
                Toast.makeText(getApplication(), "Connection failed !!!", Toast.LENGTH_LONG).show()
            }
            override fun onResponse(call: Call<RemoveFavModel>, response: Response<RemoveFavModel>) {

                if (response.isSuccessful && response.body() != null) {
                    val totl = kidos?.value?.toInt()?.minus(rewardsLive.value!![index].rewardsKudos?.toInt()!!).toString()
                    kidos?.value =totl
                    Hawk.put("Kudos",kidos?.value.toString())
                    Home.setkudo()
                    RewardChallengs.setkudo()
                    isProgress.set(View.GONE)
                    refreshAdapter?.value = true
                     var rewards: DataItem = rewardsLive.value!![index]

                    rewards = DataItem(rewardsCreatedDate = rewardsLive.value!![index].rewardsCreatedDate,
                        rewardsModifiedDate = rewardsLive.value!![index].rewardsModifiedDate,
                        rewardsCloseDate = rewardsLive.value!![index].rewardsCloseDate,
                        rewardsCloseTime = rewardsLive.value!![index].rewardsCloseTime,
                        rewardsImage = rewardsLive.value!![index].rewardsImage,
                        rewardsKudos = rewardsLive.value!![index].rewardsKudos,
                        rewardsStatus = rewardsLive.value!![index].rewardsStatus,
                        rewardsId = rewardsLive.value!![index].rewardsId,
                        clientId = rewardsLive.value!![index].clientId,
                        rewardsName = rewardsLive.value!![index].rewardsName,
                        rewardsDescription = rewardsLive.value!![index].rewardsDescription,
                        status = "1")
                    rewardList.set(index,rewards)
                    rewardsLive.value = rewardList
                    refreshAdapter?.value = true
//                    rewards = DataItem(rewards_created_date = "1")
                    val gson = Gson()
                    val jsonElement: JsonElement = gson.toJsonTree(rewards)
                    Log.e("edit_value", jsonElement.toString())
//                    rewardsLive.value!![index] = rewards


                } else {
                }
                Toast.makeText(getApplication(), response.body()!!.message,Toast.LENGTH_SHORT).show()

            }

        })
    }


}