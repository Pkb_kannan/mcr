package com.mcr.active.model.LocationDetailModel


import com.google.gson.annotations.SerializedName


data class IminAggregateOffer(

	@field:SerializedName("publicAdult")
	val publicAdult: PublicAdult? = null,

	@field:SerializedName("type")
	val type: String? = null
)