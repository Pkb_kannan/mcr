package com.mcr.active.model.RegisterModel

import com.google.gson.annotations.SerializedName

data class RegisterModel(
	@field:SerializedName("member_id")
	val memberId: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)