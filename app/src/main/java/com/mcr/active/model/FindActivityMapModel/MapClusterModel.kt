package com.mcr.active.model.FindActivityMapModel

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class MapClusterModel : ClusterItem {

    private var mPosition: LatLng? = null
    private var mTitle: String? = null
    private var mSnippet: String? = null

    fun MapClusterModel(lat: Double, lng: Double) {
        mPosition = LatLng(lat, lng)
        mTitle = null
        mSnippet = null
    }

    fun MapClusterModel(lat: Double, lng: Double, title: String, snippet: String) {
        mPosition = LatLng(lat, lng)
        mTitle = title
        mSnippet = snippet
    }

    override fun getSnippet(): String? {
        return  mSnippet
    }

    override fun getTitle(): String? {
     return  mTitle
    }

    override fun getPosition(): LatLng? {
        return  mPosition
    }
}