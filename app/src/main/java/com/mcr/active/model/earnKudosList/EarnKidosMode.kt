package com.mcr.active.model.earnKudosList


import com.google.gson.annotations.SerializedName


data class EarnKidosMode(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null,

	@field:SerializedName("total_earned_kudos")
	val totalKudos: String? = null
)