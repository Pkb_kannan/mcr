package com.mcr.active.ui.fragment.home

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mcr.active.R
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.Util.Util.Companion.getRequest
import com.mcr.active.Util.Util.Companion.plain
import com.mcr.active.base.BaseViewModel
import com.mcr.active.model.earnKudosList.EarnKidosMode
import com.mcr.active.model.earnKudosList.EarnKudosList
import com.mcr.active.model.earnKudosList.KudosDataItem
import com.mcr.active.model.favouritModel.removeModel.RemoveFavModel
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.model.noliList
import com.mcr.active.model.picUload.PicUpload
import com.mcr.active.network.Api
import com.mcr.active.ui.MainViewModel
import com.mcr.active.ui.fragment.home.adadpter.EarnKudosAdapter
import com.mcr.active.ui.fragment.home.adadpter.NotificationAdapter
import com.mcr.active.ui.fragment.home.bottomSheetFragment.favourit.FavouritBottomSheetFragmentScrool
import com.mcr.active.ui.fragment.home.bottomSheetFragment.location.InfoBottomSheetFragment
import com.mcr.active.ui.fragment.home.bottomSheetFragment.location.LocationBottomSheetFragmentSplash
import com.mcr.active.ui.fragment.home.bottomSheetFragment.search.SearchBottomSheetFragmentScrool
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.timeTable.TimeBottomSheetFragment
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.search.DayAdapter2
import com.orhanobut.hawk.Hawk
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.util.*
import javax.inject.Inject

class HomeViewmodel(application: Application) : BaseViewModel(application) {

    @Inject
    lateinit var api: Api
    var row_index: Int = -1
    var days: ArrayList<String>? = null
    var userData: LoginModel = LoginModel()
    private var adapter2: DayAdapter2? = null
    var EarnKudos: SingleLiveEvent<Boolean>? = null
    private var adapter: NotificationAdapter? = null
    var homeDialog: SingleLiveEvent<Boolean>? = null
    private var kudosAdapter: EarnKudosAdapter? = null
    var select_position: SingleLiveEvent<Int>? = null
    var addapterRefresh: SingleLiveEvent<Boolean>? = null
    var EarnKudosLoging: SingleLiveEvent<Boolean>? = null
    var homedataObserver: SingleLiveEvent<Boolean>? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    var LocationDialogClose: SingleLiveEvent<Boolean>? = null
    private var NotiList: List<noliList> = ArrayList<noliList>()
    var name: MutableLiveData<String> = MutableLiveData<String>()
    var step: MutableLiveData<String> = MutableLiveData<String>()
    var kidos: MutableLiveData<String> = MutableLiveData<String>()
    var isProgressProfile: ObservableInt = ObservableInt(View.GONE)
    var activity: MutableLiveData<String> = MutableLiveData<String>()
    var bookings: MutableLiveData<String> = MutableLiveData<String>()
    private val NotiLive: MutableLiveData<List<noliList>> = MutableLiveData<List<noliList>>()
    val earnKudosList: MutableLiveData<List<KudosDataItem>> = MutableLiveData<List<KudosDataItem>>()


    init {
        select_position?.value = -1
        homeDialog = SingleLiveEvent<Boolean>()
        EarnKudos = SingleLiveEvent<Boolean>()
        EarnKudosLoging = SingleLiveEvent<Boolean>()
        homedataObserver = SingleLiveEvent<Boolean>()
        LocationDialogClose = SingleLiveEvent<Boolean>()
        MainViewModel(getApplication())?.ToolBar(true)

    }

    fun fetchList(): MutableLiveData<List<noliList>> {
        val dob1 = noliList()
        val dob = noliList()
        dob1.setTitlee("NEW SESSION ADDED")
        dob1.setgetConten("FAMILY ROUNDERS, NUTHURST PARK, MOSTON THURSDAY AT 16:45")
        NotiList = listOf(dob1)
        dob.setTitlee("SESSION CANCELLED")
        dob.setgetConten("FAMILY SWIM, EAST MANCHESTER LEISURE CENTER HAS BEEN CANCELLED ON THURSDAY 16TH JUL")
        NotiList = listOf(dob1, dob)

        NotiLive.value = NotiList

        return NotiLive
    }

    fun locationClose() {
        LocationDialogClose?.value = true
    }

    fun homeDialod() {
        homeDialog?.value = true
    }

    fun setNotificationInAdapter(noti: List<noliList>) {
        adapter?.setNotification(noti)
        adapter?.notifyDataSetChanged()
    }

    fun getNotification(index: Int?): noliList? {
        return if (NotiLive.getValue() != null && index != null && NotiLive.getValue()!!.size > index
        ) {
            NotiLive.getValue()!!.get(index)
        } else null
    }

    fun showLocatioTimeDialogFragment(fragmager: FragmentManager) {
        val bottomSheetFragment =
            TimeBottomSheetFragment()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    fun showLocatioInfoDialogFragment(fragmager: FragmentManager) {
        val bottomSheetFragment =
            InfoBottomSheetFragment()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    fun showLocationDialogFragment(fragmager: FragmentManager) {
        val bottomSheetFragment =
            LocationBottomSheetFragmentSplash()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    fun showSearchDialogFragment(fragmager: FragmentManager) {
        val bottomSheetFragment =
            SearchBottomSheetFragmentScrool()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    fun showFavouritDialogFragment(fragmager: FragmentManager) {
        val bottomSheetFragment =
            FavouritBottomSheetFragmentScrool()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }


    fun onSelectItem(
        parent: AdapterView<*>?,
        view: View?,
        pos: Int,
        id: Long
    ) {

    }

    fun earnKudos() {
        EarnKudosLoging?.value = false
        isProgress.set(View.VISIBLE)
        val call = api.earnkudosList(Util.getBaseUrl() + "earnkudos")

        call?.enqueue(object : retrofit2.Callback<EarnKudosList> {
            override fun onFailure(call: Call<EarnKudosList>?, t: Throwable?) {
                Log.e("response", t?.message)

                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<EarnKudosList>?,
                response: Response<EarnKudosList>?
            ) {
                val gson = Gson()
                val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                Log.e("list_kudos", jsonElement.toString())
                if (response?.body()?.status!!) {
                    isProgress.set(View.GONE)
                    earnKudosList.value = response?.body()?.data
                    EarnKudosLoging?.value = true

                } else {
                    Toast.makeText(getApplication(), response?.body()?.message, Toast.LENGTH_SHORT)
                        .show()
                }


            }

        })
    }

    fun name(index: Int): String {
        return if (earnKudosList.value!![index] != null) {
            earnKudosList.value!![index]?.name.toString()
        } else return ""
    }

    fun setKudosInAdapter(kudos: List<KudosDataItem>) {
        kudosAdapter?.setKudos(kudos)
        kudosAdapter?.notifyDataSetChanged()
    }

    fun setVisiblity(index: Int?): Int {
        if (row_index == index) {
            return View.VISIBLE
        } else {
            return View.GONE
        }
    }

    fun earnKudosApi() {
        if (Hawk.get("LoginStatus", false)) {
            var userData: LoginModel = Hawk.get("userData", userData)

            isProgress.set(View.VISIBLE)
//            EarnKudos?.value = false
            val call = api.earnkudos(
                Util.getBaseUrl() + "earnkudos",
                userData.memberId.toString(),
                earnKudosList.value?.get(row_index)?.sportsId!!,
                earnKudosList.value?.get(row_index)?.activityType!!,
                earnKudosList.value?.get(row_index)?.openActivityId!!,
                earnKudosList.value?.get(row_index)?.earnedKudos!!
            )

            call?.enqueue(object : retrofit2.Callback<EarnKidosMode> {
                override fun onFailure(call: Call<EarnKidosMode>?, t: Throwable?) {
                    Log.e("response", t?.message)
                    EarnKudos?.value = true
                    isProgress.set(View.GONE)
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onResponse(
                    call: Call<EarnKidosMode>?,
                    response: Response<EarnKidosMode>?
                ) {
//
                    if (response?.body()?.status!!) {
                        kidos?.value = response?.body()?.totalKudos
                        Hawk.put("Kudos", response?.body()?.totalKudos)
                        EarnKudos?.value = true
                        isProgress.set(View.GONE)


                    } else {
                        Toast.makeText(
                            getApplication(),
                            response?.body()?.message,
                            Toast.LENGTH_SHORT
                        ).show()
                        EarnKudos?.value = true
                    }


                }

            })
        }
    }

    fun setDayInAdapter(noti: ArrayList<String>) {
        adapter2?.setDayInAdapter(noti)
        adapter2?.notifyDataSetChanged()
        this.days = noti

    }

    fun daybind(position: Int): String? {
        return days?.get(position)
    }

    fun picUploads(file: File, id: String) {
        isProgressProfile.set(View.VISIBLE)
        var user_image: MultipartBody.Part? = null
        val ids: RequestBody = plain(id)!!

        if (file != null) { // empty or doesn't exist
            user_image = getRequest("file", file)

        }

        val call = api.updateProfile(
            Util.getBaseUrl() + "profile_image", ids,
            user_image!!
        )

        call?.enqueue(object : retrofit2.Callback<PicUpload> {
            override fun onFailure(call: Call<PicUpload>?, t: Throwable?) {
                Log.e("response", t?.message)
                EarnKudos?.value = true
                isProgressProfile.set(View.GONE)
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(
                call: Call<PicUpload>?,
                response: Response<PicUpload>?
            ) {
//                    val gson = Gson()
//                    val jsonElement: JsonElement = gson.toJsonTree(response?.body())
//                    Log.e("list_kudos", jsonElement.toString())

                if (response?.code() == 200) {
                    Toast.makeText(getApplication(), "Success", Toast.LENGTH_SHORT).show()
                    Hawk.put("profil_pic", response?.body()?.image)
                } else {
                    Toast.makeText(getApplication(), R.string.try_latter, Toast.LENGTH_SHORT).show()
                }


                isProgressProfile.set(View.GONE)

            }

        })

    }

    fun getUser() {
        userData = Hawk.get("userData", userData)
        isProgress.set(View.VISIBLE)
        val call = api.getUser1(Util.getBaseUrl() + "home", userData?.memberId.toString())

        call?.enqueue(object : retrofit2.Callback<LoginModel> {
            override fun onFailure(call: Call<LoginModel>?, t: Throwable?) {
                Log.e("response", t?.message)

                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<LoginModel>?,
                response: Response<LoginModel>?
            ) {
                if (response?.code() == 200) {
                    userData = response?.body()!!
                    Hawk.put("userData", userData)
//                    val jsonElement: JsonElement = gson.toJsonTree(response?.body())
//                    Log.e("list_value", jsonElement.toString())
                    isProgress.set(View.GONE)

                    homedataObserver?.value = true
                }
//                else{
//                    homedataObserver?.value = true
//                }
                isProgress.set(View.GONE)

            }
        })
    }


}