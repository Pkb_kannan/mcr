package com.mcr.active.model.FindActivityMapModel


import com.google.gson.annotations.SerializedName


data class IminEvent(

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)