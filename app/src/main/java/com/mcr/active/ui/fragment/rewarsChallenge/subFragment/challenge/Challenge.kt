package com.mcr.active.ui.fragment.rewarsChallenge.subFragment.challenge

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.R
import com.mcr.active.databinding.ChallengeSubBinding
import com.mcr.active.model.challengeModels.ChallengeItem
import com.mcr.active.model.challengeModels.TaskListItem
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.challenge.adapter.TaskListAdapter
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.search.SearchSpinAdapter
import com.orhanobut.hawk.Hawk


class Challenge : Fragment() {


    var binding: ChallengeSubBinding? = null
    var mtaskList = arrayListOf<TaskListItem>()
    var viewmodel: ChallengeSubViewModel? = null
    var mtaskListsorted = arrayListOf<TaskListItem>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Hawk.put("url","normal")
        viewmodel = ViewModelProviders.of(activity!!).get(ChallengeSubViewModel::class.java)
        initObservables()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater,R.layout.challenge_sub,container,false)

        binding?.viewModel = viewmodel

        mtaskList = viewmodel!!.taskLists
        binding?.btChallengReward?.setOnClickListener {
            binding?.btChallengReward?.setBackgroundResource(R.color.colorRed)
            binding?.btProgress?.setBackgroundResource(R.drawable.hash_line_box)
        }
        binding?.btProgress?.setOnClickListener {
            binding?.btChallengReward?.setBackgroundResource(R.drawable.hash_line_box)
            binding?.btProgress?.setBackgroundResource(R.color.colorRed)
        }
        binding?.spinnerRewards?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(adapterView: AdapterView<*>?) {
                    Toast.makeText(context, "Nothing Selected", Toast.LENGTH_SHORT).show()
                }
                override fun onItemSelected(
                    adapterView: AdapterView<*>?,
                    p1: View?,
                    pos: Int,
                    p3: Long
                ) {
                    viewmodel!!.progressList_item?.set(adapterView!!.getItemAtPosition(pos).toString())
                    val mchallegeId = viewmodel?.detailsList!!.get(pos).challengeId
                    mtaskListsorted.clear()
                    for (i in 0 until mtaskList.size) {
                        if (mchallegeId.equals(mtaskList.get(i).challegeId)) {
                            mtaskListsorted.add( mtaskList.get(i))
                        }
                    }
                    viewmodel!!.mtaskListsorted.value=mtaskListsorted
                    loadrecycler()
                }

            }

        return binding?.getRoot()
    }
    private fun initObservables() {
//        viewmodel?.getChallenges()

        viewmodel?.getChallenges()?.observe(this, Observer<List<ChallengeItem>?>(fun(challenfList: List<ChallengeItem>?) {
            if (challenfList!!.size == 0) {
            } else initChanllengeSpinner(challenfList)
        }))
    }

    private fun loadrecycler() {
        binding?.recyclerTasklist?.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false) as RecyclerView.LayoutManager?
        binding?.recyclerTasklist?.adapter = TaskListAdapter(context, mtaskListsorted,viewmodel)

    }


    fun initChanllengeSpinner(challenfList: List<ChallengeItem>) {


        var chList= ArrayList<String>()

       for ( s in challenfList) {
           chList.add(s.challengeName!!)
       }
        val aa = context?.let { SearchSpinAdapter(it, chList) }
        // Set layout to use when the list of choices appear
        aa?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        binding?.spinnerRewards?.setAdapter(aa)

        binding?.spinnerRewards?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onNothingSelected(adapterView: AdapterView<*>?) {
                    Toast.makeText(context, "Nothing Selected", Toast.LENGTH_SHORT).show()
                }

                override fun onItemSelected(
                    adapterView: AdapterView<*>?,
                    p1: View?,
                    pos: Int,
                    p3: Long
                ) {
                    viewmodel!!.progressList_item?.set(adapterView!!.getItemAtPosition(pos).toString())
                    val mchallegeId = challenfList.get(pos).challengeId
                    mtaskListsorted.clear()
                    binding!!.spinnerChallenge.text= adapterView?.selectedItem.toString()
                    for (i in 0 until mtaskList.size) {
                        if (mchallegeId.equals(mtaskList.get(i).challegeId)) {
                            mtaskListsorted.add( mtaskList.get(i))
                        }
                    }
                    viewmodel!!.mtaskListsorted.value=mtaskListsorted
                    loadrecycler()
                }
            }
    }
    }


