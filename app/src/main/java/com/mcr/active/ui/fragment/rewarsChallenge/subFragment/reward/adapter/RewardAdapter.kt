package com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mcr.active.databinding.RewardItemBinding
import com.mcr.active.model.rewardModels.DataItem
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.RewardSubViewModel


class RewardAdapter(private val ctx: Context, private val viewModel: RewardSubViewModel,
                    private var items: List<DataItem>?
)
    : RecyclerView.Adapter<RewardAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RewardItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

//    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position)
        Glide.with(ctx).load(items?.get(position)?.rewardsImage).into(holder.binding.imgImage)
        holder.binding.textSeek.setOnClickListener {}



    }

    override fun getItemCount(): Int = items?.size!!

    fun setNotification(noti: List<DataItem>) {
        this.items = noti
        notifyDataSetChanged()
    }



     class ViewHolder( val binding: RewardItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: RewardSubViewModel?, position: Int?
        ) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }



}

