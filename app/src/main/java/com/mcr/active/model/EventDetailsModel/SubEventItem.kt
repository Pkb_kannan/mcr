package com.mcr.active.model.EventDetailsModel


import com.google.gson.annotations.SerializedName


data class SubEventItem(

	@field:SerializedName("subEvent")
	val subEvent1: List<SubEvent1Item?>? = null,

	@field:SerializedName("location")
	val location: Location? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("category")
	val category: List<String?>? = null,

	@field:SerializedName("imin:checkoutUrlTemplate")
	val iminCheckoutUrlTemplate: String? = null
)