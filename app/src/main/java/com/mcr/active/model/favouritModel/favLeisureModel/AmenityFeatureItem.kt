package com.mcr.active.model.favouritModel.favLeisureModel

data class AmenityFeatureItem(
	val name: String? = null,
	val type: String? = null,
	val value: Boolean? = null
)
