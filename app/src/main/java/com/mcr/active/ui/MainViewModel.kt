package com.mcr.active.ui

import android.app.Application
import android.view.View
import android.view.View.VISIBLE
import androidx.databinding.ObservableInt
import androidx.lifecycle.AndroidViewModel
import com.mcr.active.MCRApp
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.network.Api
import com.orhanobut.hawk.Hawk
import com.orhanobut.hawk.HawkBuilder
import com.orhanobut.hawk.LogLevel
import com.mcr.active.base.BaseViewModel
import javax.inject.Inject

class MainViewModel(application: Application) : BaseViewModel(application) {

    var slidMenuOpen: SingleLiveEvent<Boolean>? = null
    var TopHide: SingleLiveEvent<Boolean>? = null
    var hide: ObservableInt? = null
    @Inject
    lateinit var api: Api

    init {
        slidMenuOpen = SingleLiveEvent<Boolean>()
        TopHide = SingleLiveEvent<Boolean>()
        hide = ObservableInt()
        slidMenuOpen?.value = true

        Hawk.init(MCRApp.instance)
            .setEncryptionMethod(HawkBuilder.EncryptionMethod.MEDIUM)
            .setStorage(HawkBuilder.newSqliteStorage(MCRApp.instance))
            .setLogLevel(LogLevel.FULL)
            .build()
    }
    fun ToolBar(visible : Boolean){
        TopHide?.value = visible

    }
}