package com.mcr.active.ui.fragment.home.bottomSheetFragment.favourit

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.databinding.FavouritBootomSheetBinding
import com.mcr.active.model.favouritModel.favLeisureModel.FavLeisureListModel
import com.mcr.active.model.favouritModel.favouritrIminItemModel.FavActivityModel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.favourit.FavouritActivityAdapter
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.favourit.FavouritLeisureAdapter
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.favourit.FavouritViewModel
import com.orhanobut.hawk.Hawk

class FavouritBottomSheetFragmentScrool : BottomSheetDialogFragment() {

    var binding: FavouritBootomSheetBinding? = null
    var viewmodel: FavouritViewModel? = null
    private var fragmentView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CoffeeDialog)
        Util.hawk(this.context!!)
        Hawk.put("url","imin")
        viewmodel = ViewModelProviders.of(activity!!).get(FavouritViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding  = DataBindingUtil.inflate(inflater,R.layout.favourit_bootom_sheet,container,false)

        binding?.viewModel = viewmodel
        initObservables()
        return binding?.getRoot()
    }

    @SuppressLint("ResourceAsColor")
    private fun initObservables() {
        locButton()
        viewmodel?.FavouriteIminList?.clear()
        viewmodel?.FavouriteLeisureIminList?.clear()
        viewmodel?.FavouriteIminListLive?.value = null
        viewmodel?.FavouriteLeisureIminListLive?.value = null
        viewmodel?.activityTyp = 0
        if (  Hawk.get("LoginStatus",false)){

            viewmodel?.getFavouritsList()
        }
        else
            viewmodel?.isProgress?.set(View.GONE)
        binding?.btLocation?.setOnClickListener(View.OnClickListener {

            dismiss()
            fragmentManager?.let { it1 -> viewmodel?.showLocationDialogFragment(it1) }

        })
        binding?.btSearch?.setOnClickListener(View.OnClickListener {
            dismiss()
            fragmentManager?.let { it1 -> viewmodel?.showSearchDialogFragment(it1) }
        })
        binding?.btStar?.setOnClickListener(View.OnClickListener {
            dismiss()

        })
        binding?.recyFav?.removeAllViews()
//        if (viewmodel?.activityTyp == 0)
        viewmodel?.FavouriteIminListLive?.observe(
            this,
            Observer<List<FavActivityModel?>?> { favList ->
                if (favList==null||favList!!.size == 0) {
                    binding?.recyFav?.visibility = View.GONE
                } else {
                    binding?.recyFav?.visibility = View.VISIBLE
                    viewmodel?.activityTyp = 0
                    binding?.recyFav?.removeAllViews()
                    binding?.recyFav?.layoutManager = GridLayoutManager(context,1)
                    binding?.recyFav?.adapter = FavouritActivityAdapter(viewmodel!!,favList)
                    viewmodel!!.setFavActivityInAdapter(favList)
                }
            })
//        if (viewmodel?.activityTyp == 1)
        viewmodel?.FavouriteLeisureIminListLive?.observe(
            this,
            Observer<List<FavLeisureListModel?>?> { favList ->
                if (favList==null || favList!!.size == 0) {
                    binding?.recyFav?.removeAllViews()
                    binding?.recyFav?.visibility = View.GONE
                }
                else{
                    binding?.recyFav?.visibility = View.VISIBLE
                    viewmodel?.activityTyp = 1
                    binding?.recyFav?.removeAllViews()
                    binding?.recyFav?.layoutManager = GridLayoutManager(context,1)
                    binding?.recyFav?.adapter = FavouritLeisureAdapter(viewmodel!!,favList)
                    viewmodel!!.setFavLeisureInAdapter(favList)
                }
            })
        binding?.btFaclity?.setOnClickListener(View.OnClickListener {
            binding?.btFaclity?.setBackgroundResource(R.color.colorSplash)
            binding?.btFaclity?.setTextColor(Color.parseColor("#ffffff"))
            binding?.btActivity?.setTextColor(Color.parseColor("#063E5B"))
            binding?.btParks?.setTextColor(Color.parseColor("#063E5B"))
            binding?.btActivity?.setBackgroundResource(R.color.colorWhtie)
            binding?.btParks?.setBackgroundResource(R.color.colorWhtie)
            viewmodel?.activityTyp = 1
            binding?.recyFav?.removeAllViews()
            viewmodel?.FavouriteLeisureIminList?.clear()
            viewmodel?.FavouriteLeisureIminListLive?.value = null
            if (viewmodel?.FavouriteLeisureList?.size!=0)
            viewmodel?.getFavouriteLeisureImin()
        })
        binding?.btActivity?.setOnClickListener(View.OnClickListener {
            binding?.btActivity?.setBackgroundResource(R.color.colorSplash)
            binding?.btActivity?.setTextColor(Color.parseColor("#ffffff"))
            binding?.btFaclity?.setTextColor(Color.parseColor("#063E5B"))
            binding?.btParks?.setTextColor(Color.parseColor("#063E5B"))
            binding?.btFaclity?.setBackgroundResource(R.color.colorWhtie)
            binding?.btParks?.setBackgroundResource(R.color.colorWhtie)
            viewmodel?.activityTyp = 0
            binding?.recyFav?.removeAllViews()
            viewmodel?.FavouriteIminList?.clear()
            viewmodel?.FavouriteIminListLive ?.value = null
            if (viewmodel?.FavouriteActivityList?.size!=0)
            viewmodel?.getFavouriteActivituImin()

        })

    }

    private fun locButton() {
        if (  Hawk.get("location",false)){

            binding?.btLocation?.setBackgroundResource(R.color.colorSplash)
            binding?.btLocation?.setImageResource(R.drawable.ic_location_white_vt)
        }
        else
        {
            binding?.btLocation?.setBackgroundResource(R.color.colorWhtie)
            binding?.btLocation?.setImageResource(R.drawable.ic_location_white_sp)
        }
    }

}