package com.mcr.active.model.favouritModel.favouritrIminItemModel


import com.google.gson.annotations.SerializedName


data class EventScheduleItem(

	@field:SerializedName("repeatFrequency")
	val repeatFrequency: String? = null,

	@field:SerializedName("endDate")
	val endDate: String? = null,

	@field:SerializedName("byDay")
	val byDay: List<String?>? = null,

	@field:SerializedName("startTime")
	val startTime: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("startDate")
	val startDate: String? = null
)