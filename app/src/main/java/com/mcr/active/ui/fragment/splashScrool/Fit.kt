package com.mcr.active.ui.fragment.splashScrool

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.databinding.GoogleFitPermissionDialogBinding
import com.mcr.active.databinding.LoginBinding
import com.mcr.active.databinding.SplashBinding
import com.mcr.active.ui.activity.splash.SplashViewModel
import com.mcr.active.ui.fragment.signup.MoreDetailsFragmentDirections
import com.mcr.active.ui.fragment.splashScrool.SplashScroolDirections
import com.orhanobut.hawk.Hawk


class Fit : Fragment() {

    var binding: SplashBinding? = null
    var binding1: GoogleFitPermissionDialogBinding? = null
    var viewmodel: SplashViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        Util?.hawk(this.context!!)
        Hawk.put("url","imin")
//        Log.e("url0", Hawk.get("url","imin"))
        viewmodel = ViewModelProviders.of(activity!!).get(SplashViewModel::class.java)
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding  = DataBindingUtil.inflate(inflater,R.layout.splash,container,false)
        binding?.viewModel = viewmodel



        initObservables()
        return binding?.root

    }


    private fun initObservables() {
        binding?.textView?.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.nav_home)
        }

    }


        }

