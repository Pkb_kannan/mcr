package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.search

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.R
import com.mcr.active.databinding.DayItemHomeactivityBinding
import com.mcr.active.databinding.DayItemSearchbottomBinding
import kotlin.collections.ArrayList


class DayAdapter3(private val viewModel: SearchViewModel,
                  private var items: ArrayList<String>)
    : RecyclerView.Adapter<DayAdapter3.ViewHolder>() {

    var selectedItem : ArrayList<String>?= ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DayItemSearchbottomBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(
            binding
        )
    }

    //    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position,items,selectedItem)
    }

    override fun getItemCount(): Int = items.size




    fun setDayInAdapter(noti: ArrayList<String>) {
        this.items = noti
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: DayItemSearchbottomBinding) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("ResourceAsColor")
        fun bind(
            viewModel: SearchViewModel?,
            position: Int?,
            items: ArrayList<String>,
            selectedItem: ArrayList<String>?
        ) { //            viewModel.fetchDogBreedImagesAt(position);

//            binding.texDay.setOnClickListener(View.OnClickListener {
//                if (selectedItem?.contains(items[position!!])!!){
//                    binding.texDay.setBackgroundResource(R.color.colorWhtie)
//                    binding.texDay.setTextColor(R.color.colorBlack)
//                    selectedItem.remove(items[position!!])
//                }
//                else {
//                    binding.texDay.setBackgroundResource(R.color.colorRed)
//                    binding.texDay.setTextColor(R.color.colorWhtie)
//                    selectedItem.add(items[position!!])
//                }
//            })
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

