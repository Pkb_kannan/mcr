package com.sweetzpot.stravazpot.authenticaton.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.sweetzpot.stravazpot.athlete.model.Athlete;

public class LoginResult implements Parcelable {
    @SerializedName("access_token") private Token token;
    @SerializedName("athlete") private Athlete athlete;
    @SerializedName("token_type") private Token tokenType;
    @SerializedName("expires_at") private Token expiresAt;
    @SerializedName("expires_in") private Token expiresIn;
    @SerializedName("refresh_token") private Token refreshToken;

    protected LoginResult(Parcel in) {
        token = in.readParcelable(Token.class.getClassLoader());
        tokenType = in.readParcelable(Token.class.getClassLoader());
        expiresAt = in.readParcelable(Token.class.getClassLoader());
        refreshToken = in.readParcelable(Token.class.getClassLoader());
        expiresIn = in.readParcelable(Token.class.getClassLoader());
        athlete = in.readParcelable(Athlete.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(token, flags);
        dest.writeParcelable(tokenType, flags);
        dest.writeParcelable(expiresAt, flags);
        dest.writeParcelable(refreshToken, flags);
        dest.writeParcelable(expiresIn, flags);
        dest.writeParcelable(athlete, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LoginResult> CREATOR = new Creator<LoginResult>() {
        @Override
        public LoginResult createFromParcel(Parcel in) {
            return new LoginResult(in);
        }

        @Override
        public LoginResult[] newArray(int size) {
            return new LoginResult[size];
        }
    };

    public Token getToken() {
        return token;
    }

    public Athlete getAthlete() {
        return athlete;
    }
    public Token gettokenType() {
        return tokenType;
    }

    public Token getexpiresAt() {
        return expiresAt;
    }

    public Token getrefreshToken() {
        return refreshToken;
    }

    public Token getexpiresIn() {
        return expiresIn;
    }
}
