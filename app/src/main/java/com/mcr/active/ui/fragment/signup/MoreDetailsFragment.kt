package com.mcr.active.ui.fragment.signup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.mcr.active.R
import com.mcr.active.databinding.SignUpMoreDetailBinding
import com.mcr.active.ui.MainActivity
import com.mcr.active.ui.fragment.signup.MoreDetailsFragmentDirections.actionMoredetailsToNavHome


class MoreDetailsFragment : Fragment() {


    var viewmodel: RegViewModel? = null
    var binding: SignUpMoreDetailBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewmodel = ViewModelProviders.of(activity!!).get(RegViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.sign_up_more_detail, container, false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    private fun initObservables() {
        (activity as MainActivity).sidemenuHide(false)
        viewmodel?.launchSinInActivity?.observe(this, Observer {
            if (it!!)
            {
                Navigation.findNavController(requireView()).navigate(R.id.nav_login)
            }
        })
        binding?.close?.setOnClickListener {
//            Navigation.findNavController(requireView()).navigate(R.id.register)
            (activity as MainActivity).onBackPressed()
        }

    }

}