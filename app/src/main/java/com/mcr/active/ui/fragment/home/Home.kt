package com.mcr.active.ui.fragment.home

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.fitness.request.SessionReadRequest
import com.google.android.gms.fitness.result.SessionReadResponse
import com.google.android.gms.tasks.Task
import com.mcr.active.ConnectionLiveData
import com.mcr.active.R
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.Util.Util.Companion.getImageUri
import com.mcr.active.Util.Util.Companion.getRealPathFromURI
import com.mcr.active.Util.Util.Companion.makeRequestGallery
import com.mcr.active.Util.Util.Companion.setupPermissionsGallery
import com.mcr.active.customView.RecyclerItemClickListenr
import com.mcr.active.databinding.HomeBinding
import com.mcr.active.databinding.HomeDialogBinding
import com.mcr.active.model.ConnectionModel
import com.mcr.active.model.earnKudosList.KudosDataItem
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.model.noliList
import com.mcr.active.ui.MainActivity
import com.mcr.active.ui.fragment.fitnessApps.MyApps
import com.mcr.active.ui.fragment.home.HomeDirections.actionNavHomeToNavReward
import com.mcr.active.ui.fragment.home.adadpter.EarnKudosAdapter
import com.mcr.active.ui.fragment.home.adadpter.NotificationAdapter
import com.mcr.active.ui.fragment.home.bottomSheetFragment.favourit.FavouritBottomSheetFragmentScrool
import com.mcr.active.ui.fragment.home.bottomSheetFragment.location.LocationBottomSheetFragmentSplash
import com.mcr.active.ui.fragment.home.bottomSheetFragment.search.SearchBottomSheetFragmentScrool
import com.orhanobut.hawk.Hawk
import java.io.File
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit

class Home : Fragment() {

    var TAG = "Sessions"
    var dialog: Dialog? = null
    var finalFile: File? = null
    var binding: HomeBinding? = null
    var mClient: GoogleApiClient? = null
    var viewmodel1: HomeViewmodel? = null
    var binding1: HomeDialogBinding? = null
    var userData: LoginModel = LoginModel()
    var SAMPLE_SESSION_NAME = "Afternoon run"
    var REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    companion object {
        var viewmodel: HomeViewmodel? = null
        fun setkudo() {
            viewmodel?.kidos?.value = Hawk.get("Kudos", "0")
        }

        init {
            viewmodel?.kidos?.value = Hawk.get("Kudos", "0")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Util.hawk(this.context!!)
        Hawk.put("url", "imin")
        Hawk.put("location", true)
        viewmodel = ViewModelProviders.of(activity!!).get(HomeViewmodel::class.java)
        var connectionLiveData = context?.let { ConnectionLiveData(it) }
        connectionLiveData?.observe(this,
            Observer<ConnectionModel> { connection ->
                if (connection.isConnected) {
                    when (connection.type) {
                        0 -> {
                            Toast.makeText(
                                context,
                                String.format("Connection turned OFF"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else -> {
                            // wifi=1  mobile=2
//                            Toast.makeText(
//                                context,
//                                String.format("Connection Available"),
//                                Toast.LENGTH_SHORT
//                            ).show()
                            // Do some code here
                        }
                    }
                } else {

                    Toast.makeText(
                        context,
                        String.format("Connection Error"),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })


    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.home, container, false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initObservables() {
        binding?.circleImageView?.setOnClickListener {
            if (setupPermissionsGallery(this!!.context!!)) {
                proceedAfterPermission()
            } else {
                makeRequestGallery(requireActivity())
            }

        }
        Hawk.put("page", "home")
        if ( !Hawk.get("FirstLogin",false)){
            viewmodel?.getUser()

        }
        else{
            display()
//            viewmodel?.homedataObserver?.value = true
        }

        userData = Hawk.get("userData", userData)
        binding?.txtName?.text = userData.memberFirstname.toString() + "\n" + userData.memberLastname.toString()
        if (!Hawk.get("profil_pic", "").equals(""))
            Glide.with(context!!).load(Hawk.get("profil_pic", "").toString())
                .into(binding?.circleImageView!!)
        viewmodel?.kidos?.value = Hawk.get("Kudos", "0")
        viewmodel?.userData = Hawk.get("userData", userData)
//                viewmodel?.name?.value = userData.memberFirstname + "\n" + userData.memberLastname
        viewmodel?.activity?.value = viewmodel?.userData?.activities.toString()
        viewmodel?.bookings?.value = viewmodel?.userData?.booking.toString()
        viewmodel?.step?.value =  viewmodel?.userData?.steps.toString()
        binding?.txtCount?.text = Hawk.get("Kudos", "0")



        (activity as MainActivity).show()
        (activity as MainActivity).sidemenuHide(true)

        viewmodel?.homedataObserver?.observeForever {
            if (it){
                viewmodel?.homedataObserver?.value = false
                display()
            }
        }
        viewmodel?.EarnKudos?.observeForever {
            if (it) {
                binding?.txtCount?.text = Hawk.get("Kudos", "0")
            }
        }
        viewmodel?.fetchList()?.observe(
            this,
            Observer<List<noliList>?> { notiList ->
                if (notiList!!.size == 0) {
                } else {
                    binding?.recyNotification?.layoutManager = GridLayoutManager(context, 1)
                    binding?.recyNotification?.adapter = NotificationAdapter(viewmodel!!, notiList)
                    viewmodel!!.setNotificationInAdapter(notiList)
                }
            })
        viewmodel?.homeDialog?.observe(this, Observer {
            if (it!!) HomeDialod()
        })
        viewmodel?.LocationDialogClose?.observe(this, Observer {
            if (it!!) showLocationDialogFragment()
        })

        binding?.btLocation?.setOnClickListener(View.OnClickListener {

            //            showLocationDialogFragment()
            fragmentManager?.let { it1 -> viewmodel?.showLocationDialogFragment(it1) }
        })
        binding?.btStar?.setOnClickListener(View.OnClickListener {

            showFavouritDialogFragment()
        })
        binding?.btSearch?.setOnClickListener(View.OnClickListener {

            showSearchDialogFragment()
        })
        binding?.btSpent?.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(it).navigate(actionNavHomeToNavReward())
        })


        val fitnessOptions = FitnessOptions.builder()
            .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
            .build()
//
        if (!GoogleSignIn.hasPermissions(
                GoogleSignIn.getLastSignedInAccount(activity),
                fitnessOptions
            )
        ) {
//            GoogleSignIn.requestPermissions(
//                this, // your activity
//                100,
//                GoogleSignIn.getLastSignedInAccount(activity),
//                fitnessOptions)
        } else {

            readData()
        }

    }

    fun showLocationDialogFragment() {
        val bottomSheetFragment =
            LocationBottomSheetFragmentSplash()
        fragmentManager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    fun showFavouritDialogFragment() {
        val bottomSheetFragment =
            FavouritBottomSheetFragmentScrool()
        fragmentManager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    fun showSearchDialogFragment() {
        val bottomSheetFragment =
            SearchBottomSheetFragmentScrool()
        fragmentManager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 100) {
                readData()
            } else if (requestCode == 103) {
//                val imageBitmap = data.extras.get("data") as Bitmap
                val photo = data?.extras?.get("data") as Bitmap
                val tempUri: Uri = getImageUri(context!!, photo)!!
                finalFile = File(getRealPathFromURI(tempUri, context!!))
//                Glide.with(context!!).load(finalFile.absolutePath).into(binding?.circleImageView!!)
                binding?.circleImageView!!.setImageBitmap(photo)
                viewmodel?.picUploads(finalFile!!, userData?.memberId.toString())
//                val path: String = CompressImage.compressImage(context, finalFile.absolutePath)
//                ImagePath = File(path)
            } else {
//            photo = (Bitmap) result.getExtras().get("data");
                val selectedMediaUri = data?.getData()
                //            img_List.add(selectedMediaUri);
                try {
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(
                            activity!!.contentResolver,
                            selectedMediaUri
                        )
                    val tempUri = getImageUri(context!!, bitmap)
                    finalFile = File(getRealPathFromURI(tempUri, context!!))
////                        val path: String =
////                            CompressImage.compressImage(context, finalFile.absolutePath)
//                        Log.e("file_path", path)
//                        val ImagePath = File(path)
                    Glide.with(context!!).load(finalFile!!.absolutePath)
                        .into(binding?.circleImageView!!)

                    viewmodel?.picUploads(finalFile!!, userData?.memberId.toString())
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
    }

    fun HomeDialod() {

        viewmodel?.earnKudos()
        dialog = Dialog(this.context!!)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        viewmodel = ViewModelProviders.of(activity!!).get(HomeViewmodel::class.java)
        binding1 = DataBindingUtil.inflate(
            LayoutInflater.from(dialog!!.context),
            R.layout.home_dialog,
            null,
            false
        )
        binding1?.root?.let { dialog!!.setContentView(it) }
//        dialog .setContentView(R.layout.home_dialog)
        binding1?.viewModel = viewmodel1
        viewmodel?.EarnKudos?.observe(this, Observer {
            if (it!!) {
                dialog!!.dismiss()
                viewmodel?.isProgress?.set(View.GONE)
                viewmodel?.EarnKudos?.value = false
            }
        })
        binding1?.btSubmit?.setOnClickListener(View.OnClickListener {
            if (viewmodel?.earnKudosList?.value != null && viewmodel?.row_index != -1)
                viewmodel?.earnKudosApi()

        })
        binding1?.txtClose?.setOnClickListener(View.OnClickListener {

            dialog!!.dismiss()
        })
        viewmodel?.earnKudosList?.observe(
            this,
            Observer<List<KudosDataItem>?> { favList ->
                if (favList!!.size == 0) {
                } else {
                    viewmodel?.isProgress?.set(View.GONE)
                    binding1?.loadr?.visibility = GONE
                    binding1?.recy?.layoutManager = GridLayoutManager(context, 3)
                    binding1?.recy?.adapter = EarnKudosAdapter(viewmodel!!, favList)
                    viewmodel!!.setKudosInAdapter(favList)
                }
            })

        this!!.context?.let {
            RecyclerItemClickListenr(
                it,
                binding1!!.recy,
                object : RecyclerItemClickListenr.OnItemClickListener {

                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        viewmodel?.row_index = position
                        binding1?.recy?.adapter?.notifyDataSetChanged()
                    }

                    override fun onItemLongClick(view: View?, position: Int) {
                        TODO("do nothing")
                    }

                })
        }?.let { binding1?.recy?.addOnItemTouchListener(it) }
        dialog!!.show()


    }


    fun subscribe() { // To create a subscription, invoke the Recording API. As soon as the subscription is
// active, fitness data will start recording.
        activity?.let {
            Fitness.getRecordingClient(
                it,
                GoogleSignIn.getLastSignedInAccount(activity)!!
            )
                .subscribe(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.e("success", "Successfully subscribed!")
                        readData()
                    } else {
                        Log.e(
                            "success",
                            "There was a problem subscribing.",
                            task.exception
                        )
//                        someTask().execute()
                    }
                }
        }
    }

    private fun readData() {
        activity?.let {
            Fitness.getHistoryClient(
                it,
                GoogleSignIn.getLastSignedInAccount(activity)!!
            )
                .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
                .addOnSuccessListener { dataSet ->
                    val total =
                        if (dataSet.isEmpty) 0 else dataSet.dataPoints[0].getValue(
                            Field.FIELD_STEPS
                        ).asInt().toLong()
                    Log.e("step_count", "Total steps: $total")
                }
                .addOnFailureListener { e ->
                    Log.e(
                        "step_count",
                        "There was a problem getting the step count.",
                        e
                    )
                }
        }
    }

    private fun verifySession(): Task<SessionReadResponse?>? { // Begin by creating the query.
        val readRequest: SessionReadRequest = readFitnessSession()
        // [START read_session]
// Invoke the Sessions API to fetch the session with the query and wait for the result
// of the read request. Note: Fitness.SessionsApi.readSession() requires the
// ACCESS_FINE_LOCATION permission.
        return activity?.let {
            Fitness.getSessionsClient(
                it,
                GoogleSignIn.getLastSignedInAccount(activity)!!
            )
                .readSession(readRequest)
                .addOnSuccessListener { sessionReadResponse ->
                    // Get a list of the sessions that match the criteria to check the result.
                    val sessions =
                        sessionReadResponse.sessions
                    Log.e(
                        TAG,
                        "Session read was successful. Number of returned sessions is: "
                                + sessions.size
                    )
                    for (session in sessions) { // Process the session
//                        dumpSession(session)
                        // Process the data sets for this session
                        val dataSets =
                            sessionReadResponse.getDataSet(session)
                        for (dataSet in dataSets) {
//                            dumpDataSet(dataSet)
                        }
                    }
                }
                .addOnFailureListener { Log.e(TAG, "Failed to read session") }
        }
        // [END read_session]
    }

    private fun readFitnessSession(): SessionReadRequest {
        Log.e(
            TAG,
            "Reading History API results for session: " + "SAMPLE_SESSION_NAME"
        )
        // [START build_read_session_request]
// Set a start and end time for our query, using a start time of 1 week before this moment.
        val cal = Calendar.getInstance()
        val now = Date()
        cal.time = now
        val endTime = cal.timeInMillis
        cal.add(Calendar.WEEK_OF_YEAR, -1)
        val startTime = cal.timeInMillis
        // Build a session read request
        // [END build_read_session_request]
        return SessionReadRequest.Builder()
            .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS)
            .read(DataType.TYPE_SPEED)
            .setSessionName(SAMPLE_SESSION_NAME)
            .build()
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestRuntimePermissions() {
        val shouldProvideRationale: Boolean =
            activity?.shouldShowRequestPermissionRationale(
                Manifest.permission.ACCESS_FINE_LOCATION
            )!!
        // Provide an additional rationale to the user. This would happen if the user denied the
// request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.e(
                TAG,
                "Displaying permission rationale to provide additional context."
            )
            Toast.makeText(activity, "permission_rationale", Toast.LENGTH_SHORT).show()
        } else {
            Log.e(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
// sets the permission in a given state or the user denied the permission
// previously and checked "Never ask again".
            activity?.requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODE
            )
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    private fun hasRuntimePermissions(): Boolean {
        val permissionState: Int = activity?.checkSelfPermission(
            Manifest.permission.ACCESS_FINE_LOCATION
        )!!
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun proceedAfterPermission() { // Toast.makeText(getContext(), "We got All Permissions", Toast.LENGTH_LONG).show();
        val items = arrayOf<CharSequence>(
            "Take Photo", "Choose from Library",
            "Cancel"
        )
        val builder = AlertDialog.Builder(this!!.context!!)
        builder.setTitle("Add Photo!")
        builder.setItems(items, DialogInterface.OnClickListener { dialog, item ->
            //  boolean result=Utility.checkPermission(getActivity());
            if (items[item] == "Take Photo") {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(
                    cameraIntent,
                    103
                )

            } else if (items[item] == "Choose from Library") {
                val galleryIntent =
                    Intent(Intent.ACTION_GET_CONTENT)
                galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                galleryIntent.type = "image/*"
                galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                galleryIntent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(
                        galleryIntent,
                        "Select Picture"
                    ), 104
                )
                // startActivityForResult(galleryIntent, MY_GALLERY_REQUEST_CODE);
            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        })
        builder.show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            102 -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Log.i(ContentValues.TAG, "Permission has been denied by user")
                } else {
                    proceedAfterPermission()
                }
            }
        }
    }
    fun display(){
        viewmodel?.homedataObserver?.value = false

        viewmodel?.kidos?.value = Hawk.get("Kudos", "0")
        viewmodel?.userData = Hawk.get("userData", userData)
//                viewmodel?.name?.value = userData.memberFirstname + "\n" + userData.memberLastname
        viewmodel?.activity?.value = viewmodel?.userData?.activities.toString()
        viewmodel?.bookings?.value = viewmodel?.userData?.booking.toString()
        viewmodel?.step?.value =  viewmodel?.userData?.steps.toString()
        binding?.txtCount?.text = Hawk.get("Kudos", "0")
    }


}


