package com.mcr.active.ui.fragment.fitnessApps

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mcr.active.R
import com.mcr.active.databinding.MyAppsBinding
import com.mcr.active.databinding.MyAppsStarBinding
import com.mcr.active.ui.activity.strava.StravaActivity


class MyAppsAdapter(val ctx: Context): RecyclerView.Adapter<MyAppsAdapter.AppsViewHolder>() {

    private var apps: ArrayList<String>? = null



    @NonNull
    override fun onCreateViewHolder(@NonNull viewGroup: ViewGroup, i: Int): AppsViewHolder {
        val myAppsBinding  = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.context),  R.layout.my_apps,viewGroup ,false)as MyAppsBinding
        return AppsViewHolder(myAppsBinding)
    }

    override fun onBindViewHolder(@NonNull appsViewHolder: AppsViewHolder, i: Int) {
        val texT = apps!!.get(i)
//        appsViewHolder.myAppsBinding.btLink.text = texT.toString()
//        Glide.with(ctx).load(R.drawable.nrc).into(appsViewHolder.myAppsBinding.imageApps)

        when (texT) {
            "LINK1" -> {
                appsViewHolder.myAppsBinding.btLink.text = texT.toString()
                Glide.with(ctx).load(R.drawable.strava).into(appsViewHolder.myAppsBinding.imageApps)
                appsViewHolder.myAppsBinding.dwlCloud.visibility=View.GONE
                appsViewHolder.myAppsBinding.btLink.setOnClickListener {
                    val i = Intent(ctx, StravaActivity::class.java)
                    startActivity(ctx,i,null)

                }
            }
            "LINK2" -> {
                appsViewHolder.myAppsBinding.btLink.text = texT.toString()
                Glide.with(ctx).load(R.drawable.r).into(appsViewHolder.myAppsBinding.imageApps)
                appsViewHolder.myAppsBinding.dwlCloud.visibility=View.VISIBLE

            }
            "LINK3" -> {
                appsViewHolder.myAppsBinding.btLink.text = texT.toString()
                Glide.with(ctx).load(R.drawable.nrc).into(appsViewHolder.myAppsBinding.imageApps)
                appsViewHolder.myAppsBinding.dwlCloud.visibility=View.VISIBLE

            }
            "LINK4" -> {
                appsViewHolder.myAppsBinding.btLink.text = texT.toString()
                Glide.with(ctx).load(R.drawable.like).into(appsViewHolder.myAppsBinding.imageApps)
            }
            "LINK5" -> {
                appsViewHolder.myAppsBinding.btLink.text = texT.toString()
                Glide.with(ctx).load(R.drawable.walk).into(appsViewHolder.myAppsBinding.imageApps)
                appsViewHolder.myAppsBinding.dwlCloud.visibility=View.VISIBLE

            }
            "LINK6" -> {
                appsViewHolder.myAppsBinding.btLink.text = texT.toString()
                Glide.with(ctx).load(R.drawable.jawbone)
                    .into(appsViewHolder.myAppsBinding.imageApps)
            }
        }
    }

    override fun getItemCount(): Int {
        return apps!!.size
//        return 0
    }

    fun setAppList(apps: ArrayList<String>) {
        this.apps = apps
        notifyDataSetChanged()
    }

    inner class AppsViewHolder(@param:NonNull val myAppsBinding: MyAppsBinding) :
        RecyclerView.ViewHolder(myAppsBinding.getRoot())
    inner class AppsStarViewHolder(@param:NonNull val myAppsStarBinding: MyAppsStarBinding) :
        RecyclerView.ViewHolder(myAppsStarBinding.getRoot())

}