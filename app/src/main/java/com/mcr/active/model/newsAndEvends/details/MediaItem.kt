package com.mcr.active.model.newsAndEvends.details


import com.google.gson.annotations.SerializedName


data class MediaItem(

	@field:SerializedName("post_id")
	val postId: String? = null,

	@field:SerializedName("mime_type")
	val mimeType: String? = null,

	@field:SerializedName("attachment_type")
	val attachmentType: String? = null,

	@field:SerializedName("media_id")
	val mediaId: String? = null,

	@field:SerializedName("post_type")
	val postType: String? = null,

	@field:SerializedName("media_name")
	val mediaName: String? = null,

	@field:SerializedName("media_url")
	val mediaUrl: String? = null,

	@field:SerializedName("is_thumbnail")
	val isThumbnail: String? = null
)