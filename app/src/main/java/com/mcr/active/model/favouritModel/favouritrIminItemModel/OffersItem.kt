package com.mcr.active.model.favouritModel.favouritrIminItemModel


import com.google.gson.annotations.SerializedName


data class OffersItem(

	@field:SerializedName("identifier")
	val identifier: String? = null,

	@field:SerializedName("priceCurrency")
	val priceCurrency: String? = null,

	@field:SerializedName("price")
	val price: Double? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)