package com.mcr.active.ui.fragment.newsAndEvents

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mcr.active.databinding.NewsEvevntsItemsBinding
import com.mcr.active.model.newsAndEvends.list.EventsItem
import com.mcr.active.model.newsAndEvends.list.FeaturedItem
import com.mcr.active.model.newsAndEvends.list.NewsItem


class NewsEventsAdapter(
    private val context : Context,
    private val viewModel: NewsViewModel,
    private var items: List<FeaturedItem>?,
    private var newsList: List<NewsItem>?,
    private var eventList: List<EventsItem>?
)
    : RecyclerView.Adapter<NewsEventsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = NewsEvevntsItemsBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position)
        if(viewModel?.type?.get()==0)
            Glide.with(context).load(items?.get(position)?.mediaUrl).into(holder.binding.img)
        else if(viewModel?.type?.get()==1)
            Glide.with(context).load(newsList?.get(position)?.mediaUrl).into(holder.binding.img)
        else
            Glide.with(context).load(eventList?.get(position)?.mediaUrl).into(holder.binding.img)
    }

    override fun getItemCount(): Int  {
        if (items!=null)
            return items!!.size
        else if (newsList!=null)
            return newsList!!.size
        else if (eventList!=null)
            return eventList!!.size
        else
            return 0
    }
    fun setNews(
        fea: List<FeaturedItem?>?,news : List<NewsItem>,event : List<EventsItem>
    ) {
        this.items = fea as List<FeaturedItem>
        this.newsList = news as List<NewsItem>
        this.eventList = news as List<EventsItem>
        notifyDataSetChanged()
    }

    class ViewHolder(var binding: NewsEvevntsItemsBinding):
            RecyclerView.ViewHolder(binding.root) {
        fun bind(viewModel: NewsViewModel?, position: Int?
        ) {

            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

