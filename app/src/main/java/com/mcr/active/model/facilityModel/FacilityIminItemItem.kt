package com.mcr.active.model.facilityModel


import com.google.gson.annotations.SerializedName


data class FacilityIminItemItem(

	@field:SerializedName("identifier")
	val identifier: String? = null,

	@field:SerializedName("imin:aggregateOffer")
	val iminAggregateOffer: IminAggregateOffer? = null,

	@field:SerializedName("provider")
	val provider: Provider? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("location")
	val location: Location? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("category")
	val category: List<String?>? = null,

	@field:SerializedName("event")
	val event: List<EventItem?>? = null,

	@field:SerializedName("telephone")
	val telephone: String? = null
)