package com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.mcr.active.R
import com.mcr.active.databinding.HomeDialogBinding
import com.mcr.active.databinding.RewardSubBinding
import com.mcr.active.model.rewardModels.DataItem
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.adapter.RewardAdapter
import com.orhanobut.hawk.Hawk


class Reward : Fragment() {

    var binding: RewardSubBinding? = null
    var viewmodel: RewardSubViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Hawk.put("url","normal")
        viewmodel = ViewModelProviders.of(activity!!).get(RewardSubViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater,R.layout.reward_sub,container,false)

        binding?.viewModel = viewmodel
//        initTab()
        initObservables()

        return binding?.getRoot()
    }
    private fun initObservables() {

//        viewmodel?.kidos?.observe(this,  Observer{ intValue ->
//            binding?.txtEarned!!.text=intValue
//        })




//        viewmodel?.fetchList()?.observe(
//            this,
//            Observer<List<challengeList>?> { challenfList ->
//                if (challenfList!!.size == 0) {
//                } else {
//                    binding?.recyPrice?.layoutManager = GridLayoutManager(context,1)
//                    binding?.recyPrice?.adapter = RewardAdapter(viewmodel!!,challenfList)
//                    viewmodel!!.setNotificationInAdapter(challenfList)
//                }
//            })

        viewmodel?.fetchRewards()?.observe(
            this,
            Observer<List<DataItem>?>(fun(challenfList: List<DataItem>?) {
                if (challenfList!!.size == 0) {
                } else {
                    binding?.recyPrice?.layoutManager = GridLayoutManager(context, 1)
//                    binding?.recyPrice?.adapter = RewardAdapter(context,viewmodel!!,challenfList)
                    binding?.recyPrice?.adapter =
                        context?.let {
                            RewardAdapter(it, viewmodel!!, challenfList)
                        }
                    viewmodel!!.setNotificationInAdapter(challenfList)
                }
            })
        )
        viewmodel?.openDetails?.observeForever {

        }

        viewmodel?.refreshAdapter?.observe(this, Observer {
            if (it!!) {
                binding?.recyPrice?.adapter?.notifyDataSetChanged()
                viewmodel?.refreshAdapter?.value = false
            }
        })

    }
    }


