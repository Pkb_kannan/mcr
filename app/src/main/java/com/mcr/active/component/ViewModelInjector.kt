package com.mcr.active.component

import com.mcr.active.MCRApp
import com.mcr.active.googleFitManager.GoogleFitManager
import com.mcr.active.ui.MainViewModel
import com.mcr.active.ui.activity.splash.SplashViewModel
import com.mcr.active.ui.fragment.findActivity.findActivityResult.findActivitiesMap.FindMapViewModel
import com.mcr.active.ui.fragment.findActivity.FindViewmodel
import com.mcr.active.ui.fragment.findActivity.findActivityResult.findActivityList.FindListViewModel
import com.mcr.active.ui.fragment.forgotPassword.FPViewModel
import com.mcr.active.ui.fragment.home.HomeViewmodel
import com.mcr.active.ui.fragment.login.LoginViewModel
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.previous.PreviousDrawsViewModel
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.challenge.ChallengeSubViewModel
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.RewardSubViewModel
import com.mcr.active.ui.fragment.signup.RegViewModel
import com.mcr.active.ui.fragment.splashScrool.SplashScroolViewModel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.favourit.FavouritViewModel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.LocationWhiteViewModel
import dagger.Component
import com.mcr.active.injection.module.NetworkModule
import com.mcr.active.ui.fragment.fitnessApps.MyAppsViewModel
import com.mcr.active.ui.fragment.newsAndEvents.NewsViewModel
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified PostListViewModel.
     * @param loginvieModel LoginViewModel in which to inject the dependencies
     */
    fun inject(loginvieModel: LoginViewModel)
    /**
     * Injects required dependencies into the specified PostViewModel.
     * @param postViewModel PostViewModel in which to inject the dependencies
     */
    fun inject(googlgFit: GoogleFitManager)
    fun inject(findView: FindViewmodel)
    fun inject(mapVie: FindMapViewModel)
    fun inject(splasScrool: SplashScroolViewModel)
    fun inject(MainActivity: MainViewModel)
    fun inject(Splash: SplashViewModel)
    fun inject(LocationBottomSheet: LocationWhiteViewModel)
    fun inject(findListViewModel: FindListViewModel)
    fun inject(challengeSubViewModel: ChallengeSubViewModel)
    fun inject(rewardSubViewModel: RewardSubViewModel)
    fun inject(favouritViewModel: FavouritViewModel)
    fun inject(registerViewModel: RegViewModel)
    fun inject(homeViewmodel: HomeViewmodel)
    fun inject(fpViewModel: FPViewModel)
    fun inject(previousDrawsViewModel: PreviousDrawsViewModel)
    fun inject(newsViewModel: NewsViewModel)
    fun inject(myAppsViewModel: MyAppsViewModel)
    @Component.Builder
    interface Builder {

        fun build(): ViewModelInjector
        fun networkModule(networkModule: NetworkModule): Builder

    }
}