package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.favourit

import android.app.Application
import android.util.Log
import android.view.View
import androidx.databinding.ObservableInt
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.Util.Util.Companion.getDayname1
import com.mcr.active.Util.Util.Companion.getDaynameLeisure
import com.mcr.active.model.favouritModel.FavouritDataItem
import com.mcr.active.model.favouritModel.favActivityListModel.DataActivityItem
import com.mcr.active.model.favouritModel.favActivityListModel.DataLeisureItem
import com.mcr.active.model.favouritModel.favActivityListModel.FavActivityListModel
import com.mcr.active.model.favouritModel.favLeisureModel.FavLeisureListModel
import com.mcr.active.model.favouritModel.favouritrIminItemModel.FavActivityModel
import com.mcr.active.model.favouritModel.removeModel.RemoveFavModel
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.network.Api
import com.mcr.active.ui.fragment.home.bottomSheetFragment.favourit.FavouritBottomSheetFragmentScrool
import com.mcr.active.ui.fragment.home.bottomSheetFragment.location.LocationBottomSheetFragmentSplash
import com.mcr.active.ui.fragment.home.bottomSheetFragment.search.SearchBottomSheetFragmentScrool
import com.orhanobut.hawk.Hawk
import com.mcr.active.base.BaseViewModel
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class FavouritViewModel(application: Application) : BaseViewModel(application) {

    @Inject
    lateinit var api: Api

    private var FavouriteListLive: MutableLiveData<List<FavouritDataItem>> = MutableLiveData<List<FavouritDataItem>>()
    private var FavouriteActivityListLive: MutableLiveData<List<FavActivityListModel?>> = MutableLiveData<List<FavActivityListModel?>>()
    var FavouriteIminListLive: MutableLiveData<List<FavActivityModel?>> = MutableLiveData<List<FavActivityModel?>>()
    var FavouriteLeisureIminListLive: MutableLiveData<List<FavLeisureListModel?>> = MutableLiveData<List<FavLeisureListModel?>>()
    val FavouriteIminList = ArrayList<FavActivityModel>()
    var FavouriteActivityList : ArrayList<DataActivityItem> ? = null
    var FavouriteLeisureList : ArrayList<DataLeisureItem> ? = null
    var FavouriteLeisureIminList = ArrayList<FavLeisureListModel>()
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    var listStatus: ObservableInt = ObservableInt(View.VISIBLE)
    var listStatusSign_item: ObservableInt = ObservableInt(View.VISIBLE)
    private var adapterLeisure: FavouritLeisureAdapter? = null
    private var adapter: FavouritActivityAdapter? = null
    var loadComplect: SingleLiveEvent<Boolean>? = null
    var activityTyp : Int =0

    init {
     }

    fun listLocation(): MutableLiveData<List<FavActivityModel?>> {
        return FavouriteIminListLive
    }




    fun  showLocationDialogFragment(fragmager : FragmentManager) {
        val bottomSheetFragment =
            LocationBottomSheetFragmentSplash()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }
    fun  showSearchDialogFragment(fragmager : FragmentManager) {
        val bottomSheetFragment =
            SearchBottomSheetFragmentScrool()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }
    fun  showFavouritDialogFragment(fragmager : FragmentManager) {
        val bottomSheetFragment =
            FavouritBottomSheetFragmentScrool()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    fun getFavouritsList(){
        loadComplect?.value = false
        isProgress.set(View.VISIBLE)
        val member_id = Hawk.get("userData",LoginModel())
        val call = api.getFavouriteList(Util.getBaseUrl()+"listfavourite_activity",member_id?.memberId!!)

        call?.enqueue(object : retrofit2.Callback<FavActivityListModel> {
            override fun onFailure(call: Call<FavActivityListModel>?, t: Throwable?) {
                Log.e("response", t?.message)

                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(
                call: Call<FavActivityListModel>?,
                response: Response<FavActivityListModel>?
            ) {

                if (response?.code()==200) {
                    FavouriteActivityListLive.value = listOf(response.body())
                    FavouriteActivityList = (response.body()?.dataActivity as ArrayList<DataActivityItem>?)!!
                    FavouriteLeisureList = (response.body()?.dataLeisure as ArrayList<DataLeisureItem>?)!!
                    Log.e("list_size",FavouriteLeisureList?.size.toString())
//                    isProgress.set(View.GONE)
                    if (FavouriteActivityListLive.value==null || FavouriteActivityList!!.size == 0){
                        if (FavouriteActivityList?.size!=0||FavouriteLeisureList?.size!=0)
                            listStatusSign_item.set(View.VISIBLE)
                        else
                            listStatusSign_item.set(View.GONE)
                        isProgress.set(View.GONE)
                        loadComplect?.value = true
                    }
                    else {

                        getFavouriteActivituImin()
                    }
                }
                else{
                    listStatusSign_item.set(View.GONE)
                    isProgress.set(View.GONE)
                    FavouriteLeisureIminListLive.value = null
                    FavouriteIminListLive .value = null
                    FavouriteActivityList?.clear()
                    FavouriteLeisureList?.clear()
                    loadComplect?.value = true
                }

            }

        })

    }
/***************************Favourite Activity**************************************/
    fun getFavouriteActivituImin(){
    if (!FavouriteActivityList.isNullOrEmpty()){
        isProgress.set(View.VISIBLE)
        for (n in FavouriteActivityList?.indices!!) {
            val call = api.getFavouriteListActivityImin(
                FavouriteActivityList?.get(n)?.activityUrl!!,
                Util.getIminToken()
            )

            call?.enqueue(object : retrofit2.Callback<FavActivityModel> {
                override fun onFailure(call: Call<FavActivityModel>?, t: Throwable?) {
                    Log.e("response", t?.message)
                    isProgress.set(View.GONE)
                    isProgress.set(View.GONE)
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onResponse(
                    call: Call<FavActivityModel>?,
                    response: Response<FavActivityModel>?
                ) {

                    val gson = Gson()
                    val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                    if (jsonElement != null) {
                        FavouriteIminList.add(response?.body()!!)
                        FavouriteIminListLive.value = FavouriteIminList

                    }
                    if (n+1  == FavouriteActivityList?.size) {
                        if (FavouriteIminList.size == 0) {
                            listStatus?.set(View.VISIBLE)
                        } else {
                            listStatus?.set(View.GONE)
                        }
                        isProgress.set(View.GONE)
                        loadComplect?.value = true
                    }
                    isProgress.set(View.GONE)
                }
            })
        }
    }


//        }


    }

    fun setFavActivityInAdapter(fav: List<FavActivityModel?>) {
        adapter?.setFav(fav)
        adapter?.notifyDataSetChanged()
    }

        fun getItem(index: Int?): FavActivityModel? {
            return if (FavouriteIminListLive.getValue() != null && index != null && FavouriteIminListLive.getValue()!!.size > index
            ) {
                FavouriteIminListLive.value!!.get(index)
            } else null
        }
    fun getItemAddress(index: Int?): String? {
            return if (FavouriteIminListLive.getValue() != null && index != null && FavouriteIminListLive.getValue()!!.size > index
            ) {
                FavouriteIminListLive.value!!.get(index)?.iminLocationSummary?.get(0)?.address?.streetAddress
            } else null
        }
    fun getNextSession(index: Int?): String? {
            return if (FavouriteIminListLive.getValue() != null && index != null && FavouriteIminListLive.getValue()!!.size > index
            ) {
                Log.e("time_dt",FavouriteIminListLive.value?.get(index)?.subEvent?.size.toString())
                getDayname1( FavouriteIminListLive.value!!.get(index)?.subEvent?.get(0)?.subEvent1)
            } else null
        }
    fun getTimeCurrent(index: Int?): String? {
        return if(FavouriteIminListLive.getValue()?.get(0)?.subEvent?.get(0)?.eventSchedule?.get(0) != null && index != null && FavouriteIminListLive.getValue()!!.size > index) {
            FavouriteIminListLive.getValue()?.get(0)?.subEvent?.get(0)?.eventSchedule?.get(0)
                ?.startTime
        }
        else
            return ""
    }



/***********************************Favourite Leisure****************************************/
    fun getFavouriteLeisureImin(){

    if(!FavouriteLeisureList.isNullOrEmpty()){
        loadComplect?.value = false
        isProgress.set(View.VISIBLE)
        for (n in FavouriteLeisureList?.indices!!){

            val call = api.getFavouriteLeisureListImin(
                FavouriteLeisureList?.get(n)?.activityUrl!!,
                Util.getIminToken()
            )

            call?.enqueue(object : retrofit2.Callback<FavLeisureListModel> {
                override fun onFailure(call: Call<FavLeisureListModel>?, t: Throwable?) {
                    Log.e("response", t?.message)
                    isProgress.set(View.GONE)
                }

                override fun onResponse(
                    call: Call<FavLeisureListModel>?,
                    response: Response<FavLeisureListModel>?
                ) {

                    val gson = Gson()
                    val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                    if (jsonElement != null) {
                        FavouriteLeisureIminList.add(response?.body()!!)
                        FavouriteLeisureIminListLive.value = FavouriteLeisureIminList

                    }
                    if (n+1  == FavouriteLeisureList?.size) {
                        if (FavouriteLeisureIminList.size == 0) {
                            listStatus?.set(View.VISIBLE)
                        } else {
                            listStatus?.set(View.GONE)
                        }
                        isProgress.set(View.GONE)
                        loadComplect?.value = true
                        Log.e("itemsImin",FavouriteLeisureIminListLive.value?.get(0)?.name)
                    }
                    isProgress.set(View.GONE)
                }
            })
        }
    }



    }
    fun getDatList(): ArrayList<FavLeisureListModel> {
        return FavouriteLeisureIminList
    }

    fun setFavLeisureInAdapter(fav: List<FavLeisureListModel?>?) {
        adapterLeisure?.setFav(fav!!)
        adapterLeisure?.notifyDataSetChanged()
    }

    fun getItemLeisure(index: Int?): FavLeisureListModel? {
        return if (FavouriteLeisureIminListLive.getValue() != null && index != null && FavouriteLeisureIminListLive.getValue()!!.size > index
        ) {
            FavouriteLeisureIminListLive.value!!.get(index)
        } else null
    }
    fun getCurrentDayTime(index: Int?): String{
        var time : String = ""
        return if (FavouriteLeisureIminListLive.getValue() != null && index != null && FavouriteLeisureIminListLive.getValue()!![index]?.hoursAvailable?.size !=0
        ) {

            val day = Util.getCurrentWeeknae()
            val weekarry = FavouriteLeisureIminListLive.getValue()!![index]?.hoursAvailable
            for (n in weekarry?.indices!!){
                if(weekarry.get(n)?.dayOfWeek.equals(day)){
                    time =  weekarry[n]?.opens.toString()+"-"+weekarry[n]?.closes.toString()
                    break
                }
            }
            time

        } else time
    }
    fun getNextSessionLeisure(index: Int?): String? {
        return if (FavouriteLeisureIminListLive.getValue() != null && index != null && FavouriteLeisureIminListLive.getValue()!![index]?.hoursAvailable?.size !=0
        ) {
//            Log.e("time_dt",FavouriteLeisureIminListLive.value?.get(index)?.subEvent?.size.toString())
            getDaynameLeisure( FavouriteLeisureIminListLive.getValue()!![index]?.event)
        } else null
    }

    fun getItemAddressLeisure(index: Int?): String? {
        return if (FavouriteLeisureIminListLive.getValue() != null && index != null && FavouriteLeisureIminListLive.getValue()!!.size > index
        ) {
            FavouriteLeisureIminListLive.value!!.get(index)?.location?.name
        } else null
    }

/****************************Remove Favourite********************************************************/
    fun removFav(position: Int){
        loadComplect?.value = false
        isProgress.set(View.VISIBLE)
    var id : String? = null
    if (activityTyp==0){
        id =FavouriteActivityList?.get(position)?.id
    }
    else if (activityTyp==1){
        id =FavouriteLeisureList?.get(position)?.id
    }
        val call = api.removeFavouriteList(Util.getBaseUrl()+"deletefavourite_activity",
            id!!
        )

        call?.enqueue(object : retrofit2.Callback<RemoveFavModel> {
            override fun onFailure(call: Call<RemoveFavModel>?, t: Throwable?) {
                Log.e("response", t?.message)
                isProgress.set(View.GONE)
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(
                call: Call<RemoveFavModel>?,
                response: Response<RemoveFavModel>?
            ) {

                if (response?.code()==200) {
                    if (activityTyp==0){
                        FavouriteActivityList?.removeAt(position)
                        FavouriteIminList.removeAt(position)
                        FavouriteIminListLive.value = FavouriteIminList
                    }
                    else if (activityTyp==1){
                        FavouriteLeisureList?.removeAt(position)
                        FavouriteLeisureIminList.removeAt(position)
                        FavouriteLeisureIminListLive.value = FavouriteLeisureIminList
                    }


                }
                else{

                }
                isProgress.set(View.GONE)

            }

        })
    }
}