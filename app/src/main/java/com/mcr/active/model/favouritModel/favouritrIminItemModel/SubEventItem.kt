package com.mcr.active.model.favouritModel.favouritrIminItemModel


import com.google.gson.annotations.SerializedName


data class SubEventItem(

	@field:SerializedName("offers")
	val offers: List<OffersItem?>? = null,

	@field:SerializedName("identifier")
	val identifier: String? = null,

	@field:SerializedName("eventSchedule")
	val eventSchedule: List<EventScheduleItem?>? = null,

	@field:SerializedName("subEvent")
	val subEvent1: List<SubEventItem1?>? = null,

	@field:SerializedName("location")
	val location: Location? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("@context")
	val context: List<String?>? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("duration")
	val duration: String? = null,

	@field:SerializedName("endDate")
	val endDate: String? = null,

	@field:SerializedName("eventStatus")
	val eventStatus: String? = null,

	@field:SerializedName("maximumAttendeeCapacity")
	val maximumAttendeeCapacity: Int? = null,

	@field:SerializedName("startDate")
	val startDate: String? = null,

	@field:SerializedName("remainingAttendeeCapacity")
	val remainingAttendeeCapacity: Int? = null
)