package com.mcr.active.model.findActivityModel


import com.google.gson.annotations.SerializedName


data class FindActivity(

	@field:SerializedName("view")
	val view: View? = null,

	@field:SerializedName("imin:item")
	val iminItem: List<IminItemItem>? = null
)