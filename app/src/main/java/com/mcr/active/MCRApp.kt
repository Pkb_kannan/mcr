package com.mcr.active

import android.annotation.SuppressLint
import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import androidx.core.app.NotificationCompat.getExtras
import androidx.lifecycle.LiveData
import com.mcr.active.model.ConnectionModel


/**
 * Created by PKB on 10:07 12/20/19
 */
open class MCRApp : Application() {

    private var defaultSubscribeScheduler: Scheduler? = null
//    private var refWatcher: RefWatcher? = null
    override fun onCreate() {
        super.onCreate()
        instance    = this
//        refWatcher = LeakCanary.install(this);
//        val D LeakCanary: Installing AppWatcher
    }

//    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//    override fun applicationInjector(): AndroidInjector<out MCRApp> {
//        return DaggerAppComponent.builder().create(this)
//    }

    companion object {
        var instance: MCRApp? = null
            private set

        fun hasNetwork(): Boolean {
            return instance!!.checkIfHasNetwork()
        }
    }
    @SuppressLint("MissingPermission")
    fun checkIfHasNetwork(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    fun defaultSubscribeScheduler(): Scheduler? {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io()
        }
        return defaultSubscribeScheduler
    }


}
class ConnectionLiveData(val context: Context) : LiveData<ConnectionModel>() {

    private val networkReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.getExtras() != null) {
                val activeNetwork =
                    intent.getExtras()!!.get(ConnectivityManager.EXTRA_NETWORK_INFO) as NetworkInfo
                val isConnected =
                    activeNetwork.isConnectedOrConnecting()
                if (isConnected) {
                    when (activeNetwork.getType()) {
                        ConnectivityManager.TYPE_WIFI -> postValue(
                            ConnectionModel(
                                1,
                                true
                            )
                        )
                        ConnectivityManager.TYPE_MOBILE -> postValue(
                            ConnectionModel(
                                2,
                                true
                            )
                        )
                    }
                } else {
                    postValue(ConnectionModel(0, false))
                }
            }
        }
    }

    override fun onActive() {
        super.onActive()
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        context.registerReceiver(networkReceiver, filter)
    }

    override fun onInactive() {
        super.onInactive()
        context.unregisterReceiver(networkReceiver)
    }
}