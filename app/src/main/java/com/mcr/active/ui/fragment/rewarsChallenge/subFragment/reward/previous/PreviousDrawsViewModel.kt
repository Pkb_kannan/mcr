package com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.previous

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import com.mcr.active.Util.Util
import com.mcr.active.model.*
import com.mcr.active.model.PreviousDawsModel.PreviousDawsItem
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.network.Api
import com.orhanobut.hawk.Hawk
import com.mcr.active.base.BaseViewModel
import retrofit2.Call
import retrofit2.Response
import java.util.ArrayList
import javax.inject.Inject
import java.text.SimpleDateFormat


class PreviousDrawsViewModel (application: Application) : BaseViewModel(application) {

    @Inject
    lateinit var api: Api
    private var adapter: DawsAdapter? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    private var rewardList :List<PreviousDawsItem> = ArrayList()
    private val rewardsLive: MutableLiveData<List<PreviousDawsItem>> = MutableLiveData()

    init {
        fetchRewards()
    }



    fun setNotificationInAdapter(noti: List<PreviousDawsItem>?) {
        if (noti != null) {
            adapter?.setNotification(noti)
        }
        adapter?.notifyDataSetChanged()
    }
    fun goneUnless(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }
    //    fun setVisiblity(index: Int?,type: Int?): Int {
//        if (getNotification(index)?.getseekVisible()!!){
//            if (type==1)
//            return View.VISIBLE
//            else return View.GONE
//        }
//        else {
//            if (type==1)
//                return View.GONE
//            else return View.VISIBLE
//        }
//    }
    fun getNotification(index: Int?): PreviousDawsItem? {
        return if (rewardsLive.getValue() != null && index != null && rewardsLive.getValue()!!.size > index
        ) {
            rewardsLive.value?.get(index)
        } else null
    }

    fun getMaxKudos(index: Int?): Int? {
        return if (rewardsLive.getValue() != null && index != null && rewardsLive.getValue()!!.size > index
        ) {
            rewardsLive.value?.get(index)?.rewardsKudos?.toInt()
        } else null
    }

    fun getKudosText(index: Int?): String? {
        return if (rewardsLive.getValue() != null && index != null && rewardsLive.getValue()!!.size > index
        ) {
            rewardsLive.value?.get(index)?.rewardsKudos + " KUDOS ALLOCATED TO THIS PRICE DRAW"
        } else null
    }



    fun getCloseDateText(index: Int?): String? {
        return if (rewardsLive.getValue() != null && index != null && rewardsLive.getValue()!!.size > index
        ) {
            val inputFormat = SimpleDateFormat("yyyy-MM-dd")
            val outputFormat = SimpleDateFormat("dd/MM/yyyy")
            val inputDateStr = rewardsLive.value?.get(index)?.rewardsCloseDate
            val date = inputFormat.parse(inputDateStr)
            val outputDate = outputFormat.format(date)
            val claseTime=rewardsLive.value?.get(index)?.rewardsCloseTime
            "Entry closes at " + claseTime + " on " + outputDate

        } else null
    }


    fun fetchRewards() : MutableLiveData<List<com.mcr.active.model.PreviousDawsModel.PreviousDawsItem>>{
        isProgress.set(View.VISIBLE)
        val member_id = Hawk.get("userData", LoginModel())
        val call = api.getDaws(Util.getBaseUrl()+"previousdraws",member_id.memberId.toString())
        call.enqueue(object :retrofit2.Callback<com.mcr.active.model.PreviousDawsModel.Response> {
            override fun onFailure(call: Call<com.mcr.active.model.PreviousDawsModel.Response>, t: Throwable) {
                Log.e("response", t?.message)
                isProgress.set(View.GONE)
                Toast.makeText(getApplication(), "Connection failed !!!", Toast.LENGTH_LONG).show()
            }
            override fun onResponse(call: Call<com.mcr.active.model.PreviousDawsModel.Response>, response: Response<com.mcr.active.model.PreviousDawsModel.Response>) {

                if (response.isSuccessful && response.body() != null) {

                    rewardList= response.body()!!.data as List<com.mcr.active.model.PreviousDawsModel.PreviousDawsItem>
                    rewardsLive.value=rewardList

                } else {
                }
                isProgress.set(View.GONE)

            }

        })
        return rewardsLive
    }
}