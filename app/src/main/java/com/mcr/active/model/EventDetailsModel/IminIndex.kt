package com.mcr.active.model.EventDetailsModel


import com.google.gson.annotations.SerializedName


data class IminIndex(

	@field:SerializedName("genderRestriction")
	val genderRestriction: GenderRestriction? = null,

	@field:SerializedName("dayOfWeek")
	val dayOfWeek: DayOfWeek? = null,

	@field:SerializedName("activity")
	val activity: Activity? = null,

	@field:SerializedName("leaderName")
	val leaderName: LeaderName? = null,

	@field:SerializedName("accessibilitySupport")
	val accessibilitySupport: AccessibilitySupport? = null,

	@field:SerializedName("activityConceptCollection")
	val activityConceptCollection: ActivityConceptCollection? = null
)