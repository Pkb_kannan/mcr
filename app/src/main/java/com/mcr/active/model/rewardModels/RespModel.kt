package com.mcr.active.model.rewardModels


import com.google.gson.annotations.SerializedName


data class RespModel(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,
	@field:SerializedName("total_earned_kudos")
	val totalKudos: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)