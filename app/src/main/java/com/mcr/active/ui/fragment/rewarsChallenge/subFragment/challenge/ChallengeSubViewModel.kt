package com.mcr.active.ui.fragment.rewarsChallenge.subFragment.challenge

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import com.mcr.active.Util.Util


import com.mcr.active.model.challengeModels.ChallengeItem
import com.mcr.active.model.challengeModels.ChallengeResp
import com.mcr.active.model.challengeModels.TaskListItem
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.network.Api
import com.orhanobut.hawk.Hawk
import com.mcr.active.base.BaseViewModel
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class ChallengeSubViewModel (application: Application) : BaseViewModel(application){

    @Inject
    lateinit var api: Api
    var progress_list = arrayListOf<String>()
    var taskLists = arrayListOf<TaskListItem>()
    var detailsList = arrayListOf<ChallengeItem>()
    var progressList_item: ObservableField<String>? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    var mtaskListsorted = MutableLiveData<List<TaskListItem>>()
    private val challengesLive: MutableLiveData<List<ChallengeItem>> = MutableLiveData()

    init {
        getChallenges()
        progressList_item = ObservableField("")

    }
    fun taskList_item(pos: Int): TaskListItem? {
        return mtaskListsorted.value?.get(pos)

    }

    fun getChallenges() : MutableLiveData<List<ChallengeItem>> {
        isProgress.set(View.VISIBLE)
        val member_id = Hawk.get("userData", LoginModel())
        val call = api.getChallenge(Util.getBaseUrl()+"getchallenge",member_id.memberId.toString())
//        val call = api.getChallenge("22485105")
        call.enqueue(object :retrofit2.Callback<ChallengeResp> {
            override fun onFailure(call: Call<ChallengeResp>, t: Throwable) {
                Log.e("response", t?.message)
                isProgress.set(View.GONE)
                Toast.makeText(getApplication(), "Connection failed !!!", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ChallengeResp>, response: Response<ChallengeResp>) {

                if (response.isSuccessful && response.body() != null) {
                    progress_list.clear()
                    taskLists.clear()
                    detailsList= response.body()!!.data as ArrayList<ChallengeItem>
                    challengesLive.value=detailsList
                    for (i in 0 until response.body()?.data!!.size) {
                        Log.d("array", response.body()?.data!![i]!!.challengeName.toString())
                        progress_list.add(response.body()?.data!![i]!!.challengeName.toString())
                        progressList_item?.set(response.body()?.data!![i]!!.challengeName.toString())
                        for (j in 0 until detailsList.get(i).taskList!!.size){
                            taskLists.add((detailsList.get(i).taskList as ArrayList<TaskListItem>)[j])
                        }
                    }
                    isProgress.set(View.GONE)
                } else {

                }
            }
        })
         return challengesLive
    }


}