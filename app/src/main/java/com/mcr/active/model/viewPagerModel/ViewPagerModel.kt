package com.mcr.active.model.viewPagerModel

import androidx.databinding.BaseObservable
import retrofit2.Callback

class ViewPagerModel () {
    var img: Int? = null
    var content: String? = null


    fun getimg(): Int? {
        return img
    }

    fun setimg(img1: Int) {
        img = img1
    }
    fun getConten(): String? {
        return content
    }
    fun setConten(contents: String) {
        content = contents
    }





}