package com.mcr.active.model.findActivityListModel


import com.google.gson.annotations.SerializedName


data class ActivityItem(

	@field:SerializedName("prefLabel")
	val prefLabel: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)