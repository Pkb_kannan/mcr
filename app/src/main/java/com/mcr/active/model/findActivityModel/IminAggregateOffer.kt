package com.mcr.active.model.findActivityModel


import com.google.gson.annotations.SerializedName


data class IminAggregateOffer(

	@field:SerializedName("publicAdult")
	val publicAdult: PublicAdult? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("publicJunior")
	val publicJunior: PublicJunior? = null
)