package com.mcr.active.model.findActivityListModel


import com.google.gson.annotations.SerializedName


data class Logo(

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)