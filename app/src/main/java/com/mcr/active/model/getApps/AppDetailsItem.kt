package com.mcr.active.model.getApps

import com.google.gson.annotations.SerializedName

data class AppDetailsItem(

    @field:SerializedName("id")
    val id: String? = null,
    @field:SerializedName("name")
    val name: String? = null,
    @field:SerializedName("logo")
    val logo: String? = null,
    @field:SerializedName("connected")
    val connected: Boolean? = null
)