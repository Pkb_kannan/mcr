package com.mcr.active.network

import com.mcr.active.model.CommonResponseModel
import com.mcr.active.model.EventDetailsModel.EventDetailsModel
import com.mcr.active.model.FindActivityMapModel.FindMapModel
import com.mcr.active.model.LocationDetailModel.LocationDetailsModel
import com.mcr.active.model.PreviousDawsModel.Response
import com.mcr.active.model.RegisterModel.RegisterModel
import com.mcr.active.model.VenuListModel.VenuListModel
import com.mcr.active.model.challengeModels.ChallengeResp
import com.mcr.active.model.earnKudosList.EarnKidosMode
import com.mcr.active.model.earnKudosList.EarnKudosList
import com.mcr.active.model.ethnicityModel.EthniCityModel
import com.mcr.active.model.facilityModel.FacilityModel
import com.mcr.active.model.favouritModel.favActivityListModel.FavActivityListModel
import com.mcr.active.model.favouritModel.favLeisureModel.FavLeisureListModel
import com.mcr.active.model.favouritModel.favouritrIminItemModel.FavActivityModel
import com.mcr.active.model.favouritModel.removeModel.RemoveFavModel
import com.mcr.active.model.findActivityListModel.FindActivityListModel
import com.mcr.active.model.getApps.GetAllAppsModel
import com.mcr.active.model.getApps.fitBitModel.FitBitModel
import com.mcr.active.model.getApps.googleFit.GoogleFit
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.model.newsAndEvends.details.NewsEventsDetails
import com.mcr.active.model.newsAndEvends.list.NewsEvents
import com.mcr.active.model.picUload.PicUpload
import com.mcr.active.model.rewardModels.RespModel
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import java.util.*


/**
 * The interface which provides methods to get result of webservices
 */
interface Api {
    /**
     * Get the list of the pots from the API
     */
//    @FormUrlEncoded
//    @POST("member_login")
//    fun getUser(@Field("user_name") username: String,@Field("password") password: String): Call<LoginModel>

    @FormUrlEncoded
//    @POST("member_login")
    @POST
    fun getUser(
        @Url url:String,
        @Field("user_name") username: String,
        @Field("password") password: String
    ):Call<LoginModel>

    @FormUrlEncoded
//    @POST("member_login")
    @POST
    fun getUser1(
        @Url url:String,
        @Field("member_id") member_id: String
           ):Call<LoginModel>

    @POST
    fun earnkudosList(@Url url:String):Call<EarnKudosList>

    @FormUrlEncoded
    @POST()
    fun earnkudos(@Url url:String,@Field("member_id") member_id : String,@Field("sports_id") sports_id : String,@Field("activity_type") activity_type : String,@Field("activity_url") activity_url : String,@Field("earned_kudos") earned_kudos : String):Call<EarnKidosMode>

    @FormUrlEncoded
    @POST("")
    fun spentKudos(@Url url : String ,@Field("member_id") member_id : String, @Field("rewards_id") rewards_id : String, @Field("spend_kudos") spend_kudos : String):Call<RemoveFavModel>


    /********************* Event Search ******************************************/
    @GET("events-api/v2/event-series")
    fun getPlace(@Query("geo[radial]") geo : String,@Query("mode") mode: String,@Query("page") page: String,@Query("limit") limit: String,@Header("X-API-KEY") X_API_KEY : String): Call<FindActivityListModel>

    @GET("events-api/v2/event-series")
        fun geteventListFullFilter(
        @Query("geo[radial]") geo : String,
        @Query("mode") mode: String,
        @Query("page") page: String,
        @Query("limit") limit: String,
        @Query("startTime[gte]") timeFrom: String,
        @Query("startTime[lte]") timeto: String,
        @Query("dayOfWeek") week: ArrayList<String>,
        @Header("X-API-KEY") X_API_KEY : String): Call<FindActivityListModel>

    @GET("events-api/v2/event-series")
            fun geteventListTimeFilter(@Query("geo[radial]") geo : String,@Query("mode") mode: String,@Query("page") page: String,@Query("limit") limit: String,@Query("startTime[gte]") timeFrom: String,@Query("startTime[lte]") timeto: String,@Header("X-API-KEY") X_API_KEY : String): Call<FindActivityListModel>


    /********************************* Place Search ******************************/
    @GET("events-api/v2/places")
    fun getPlaceLocation(@Query("geo[radial]") geo : String,@Query("page") page: String,@Query("limit") limit: String,@Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
    fun getPlaceLocationTime(@Query("geo[radial]") geo : String,@Query("page") page: String,@Query("limit") limit: String,@Query("startTime[gte]") timeFrom: String,@Query("startTime[lte]") timeto: String,@Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
    fun getPlaceLocationstartTime(@Query("geo[radial]") geo : String,@Query("page") page: String,@Query("limit") limit: String,@Query("startTime[gte]") timeFrom: String,@Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
        fun getPlaceLocationFilterFull(@Query("geo[radial]") geo : String, @Query("page") page: String, @Query("limit") limit: String, @Query("startTime[gte]") timeFrom: String, @Query("startTime[lte]") timeto: String, @Query("dayOfWeek") week: ArrayList<String>, @Query("activityPrefLabel") activity: String, @Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
        fun getPlaceLocationFilterdayandactiviy(@Query("geo[radial]") geo : String, @Query("page") page: String, @Query("limit") limit: String,  @Query("dayOfWeek") week: ArrayList<String>, @Query("activityPrefLabel") activity: String, @Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
        fun getPlc_day_activiy_actTyp_ma_fe(@Query("geo[radial]") geo : String, @Query("page") page: String, @Query("limit") limit: String,  @Query("dayOfWeek") week: ArrayList<String>, @Query("activityPrefLabel") activity: String,@Query("genderRestriction") genter: String, @Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
        fun getPlc_day_activiy_time(@Query("geo[radial]") geo : String, @Query("page") page: String, @Query("limit") limit: String,  @Query("dayOfWeek") week: ArrayList<String>, @Query("activityPrefLabel") activity: String,@Query("startTime[gte]") timeFrom: String, @Query("startTime[lte]") timeto: String, @Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
        fun getPlc_day_time(@Query("geo[radial]") geo : String, @Query("page") page: String, @Query("limit") limit: String,  @Query("dayOfWeek") week: ArrayList<String>, @Query("startTime[gte]") timeFrom: String, @Query("startTime[lte]") timeto: String, @Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
        fun getPlc_day(@Query("geo[radial]") geo : String, @Query("page") page: String, @Query("limit") limit: String,  @Query("dayOfWeek") week: ArrayList<String>, @Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
        fun getPlc_day_actTyp_time(@Query("geo[radial]") geo : String, @Query("page") page: String, @Query("limit") limit: String,  @Query("dayOfWeek") week: ArrayList<String>,@Query("genderRestriction") genter: String, @Query("startTime[gte]") timeFrom: String,@Query("startTime[lte]") timeto: String, @Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
        fun getPlc_day_actTyp_time_level(@Query("geo[radial]") geo : String, @Query("page") page: String, @Query("limit") limit: String,  @Query("dayOfWeek") week: ArrayList<String>,@Query("levelType") genter: String, @Query("startTime[gte]") timeFrom: String,@Query("startTime[lte]") timeto: String, @Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET
    fun getPlc_day_actTyp_time_level1(@Url url:String, @Query("dayOfWeek") week: ArrayList<String>,@Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
        fun getPlaceLocationFilteronlystarttime(@Query("geo[radial]") geo : String, @Query("page") page: String, @Query("limit") limit: String, @Query("startTime[gte]") timeFrom: String,@Query("startTime[lte]") timeto: String, @Query("dayOfWeek") week: ArrayList<String>,@Query("activityPrefLabel") activity: String, @Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
    fun getPlaceLocationFilteronlygenter(@Query("geo[radial]") geo : String, @Query("page") page: String, @Query("limit") limit: String, @Query("startTime[gte]") timeFrom: String,@Query("startTime[lte]") timeto: String, @Query("dayOfWeek") week: ArrayList<String>,@Query("activityPrefLabel") activity: String,@Query("genderRestriction") genter: String, @Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET
    fun getPlaceLocationFilterUrl(@Url url:String, @Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET
    fun getlatLog(@Query("components") geo : String,@Query("page") page: String,@Query("limit") limit: String,@Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    @GET("events-api/v2/places")
    fun getPlaceFacility(@Query("geo[radial]") geo : String,@Query("limit") limit: String,@Query("page") page: String,@Query("startTime[gte]") time: String,@Header("X-API-KEY") X_API_KEY : String): Call<FindMapModel>

    /***************************** facility List ***********************************************/
    @GET("facilities-api/v2/facility-uses")
    fun getFacility(@Query("geo[radial]") geo : String,@Query("page") page: String,@Query("limit") limit: String,@Query("startDate[gte]") time: String,@Query("mode") mode: String,@Header("X-API-KEY") X_API_KEY : String): Call<FacilityModel>

    @FormUrlEncoded
    @POST("venue_list")
    fun getVenuList(@Field("lat") lat: String, @Field("lng") lng: String, @Field("distance") distance: String): Call<VenuListModel>

    @GET("events-api/v2/event-series/{id}")
    fun getPlaceDetails(@Path("id") id:String,@Header("X-API-KEY") X_API_KEY : String) : Call<LocationDetailsModel>

//    @GET("events-api/v2/event-series?")
//    fun getTimeTable(@Query("geo[radial]") geo : String,@Query("mode") mode : String,@Query("startTime[gte]") startTime : String,@Header("X-API-KEY") X_API_KEY : String) : Call<EventDetailsModel>

    @GET
    fun getTimeTable(@Url url:String, @Header("X-API-KEY") X_API_KEY : String) : Call<EventDetailsModel>

    @GET
    fun getTimeTableFilterWeek(@Url url:String, @Query("dayOfWeek") week: ArrayList<String>,@Header("X-API-KEY") X_API_KEY : String) : Call<EventDetailsModel>

    @FormUrlEncoded
    @POST()
    fun getFavouriteList(@Url url:String,@Field("member_id") lat: String): Call<FavActivityListModel>

    @FormUrlEncoded
    @POST()
    fun removeFavouriteList(@Url url:String,@Field("id") lat: String): Call<RemoveFavModel>

    @FormUrlEncoded
    @POST()
    fun addFavouriteList(@Url url:String,@Field("activity_url") activity_url: String,@Field("activity_type") activity_type: String,@Field("member_id") member_id: String): Call<RemoveFavModel>

    @GET("events-api/v2/event-series/{id}")
    fun getFavouriteListActivityImin(@Path("id") id:String,@Header("X-API-KEY") X_API_KEY : String) : Call<FavActivityModel>

    @GET("facilities-api/v2/facility-uses/{id}")
    fun getFavouriteLeisureListImin(@Path("id") id:String,@Header("X-API-KEY") X_API_KEY : String) : Call<FavLeisureListModel>

    @FormUrlEncoded
//    @POST("/kinetic_3/index.php/api/mcr_api/member/getchallenge")
    @POST
    fun getChallenge(@Url url:String,
        @Field("member_id") Id: String
    ): Call<ChallengeResp>

    @FormUrlEncoded
    @POST
    fun forgotpass(
        @Url url:String,
        @Field("user_email") useremail: String
    ): Call<LoginModel>


    @FormUrlEncoded
//    @POST("/kinetic_3/index.php/api/mcr_api/member/getrewards")
    @POST()
    fun getRewards(@Url url:String,
        @Field("member_id") Id: String
    ): Call<RespModel>

    @POST
    fun getNewsEventList(@Url url:String): Call<NewsEvents>

    @FormUrlEncoded
    @POST
    fun getNewsEventDetails(@Url url:String,@Field("id") Id: String): Call<NewsEventsDetails>

    @FormUrlEncoded
    @POST
    fun getDaws(@Url url : String,
        @Field("member_id") Id: String
    ): Call<Response>

    @FormUrlEncoded
    @POST
    fun regUser(@Url url :String,
                @Field("member_firstname") memberFirstname: String?, @Field("member_lastname") memberLastname: String?,
                @Field("gender") gender: String?, @Field("member_disability") memberDisability: String?,
                @Field("house_number") houseNumber: String,
                @Field("address_one") addressOne: String,
                @Field("address_two") addressTwo: String, @Field("postcode") postcode: String,
                @Field("emergency_number") emergencyNumber: String, @Field("ethnicity_id") ethnicity: String?,
                @Field("member_email") member_email: String,
                @Field("dob") dob: String, @Field("member_password") password: String ) : Call<RegisterModel>


    @POST
    fun getEthnicity(@Url url:String) : Call<EthniCityModel>

    @Multipart
    @POST
    fun updateProfile(@Url url:String,
        @Part("member_id") user_id: RequestBody,
        @Part Profile_image: MultipartBody.Part
    ): Call<PicUpload>

    @FormUrlEncoded
    @POST
    fun getAllapps(
        @Url url:String,
        @Field("member_id") Id: String
    ): Call<GetAllAppsModel>

    @FormUrlEncoded
    @POST
    fun getLinkUnlinkapps(
        @Url url:String,
        @Field("member_id") Id: String,
        @Field("type") Type: String,
        @Field("app_id") AppId: String,
        @Field("token") Token: String,
        @Field("token_type") token_type: String,
//        @Field("expires_at") expires_at: String,
        @Field("refresh_token") refresh_token: String,
        @Field("expires_in") expires: String

        ): Call<CommonResponseModel>

    @FormUrlEncoded
    @POST
    fun getLinkUnlinkStrava(
        @Url url:String,
        @Field("member_id") Id: String,
        @Field("type") Type: String,
        @Field("app_id") AppId: String,
        @Field("access_token") Token: String,
        @Field("token_type") token_type: String,
        @Field("expires_at") expires_at: String,
        @Field("refresh_token") refresh_token: String,
        @Field("expires") expires: String
//        @Field("code") code: String

        ): Call<CommonResponseModel>

    @FormUrlEncoded
    @POST
    fun getLinkUnlinkFitBit(
        @Url url:String,
        @Field("member_id") Id: String,
        @Field("type") Type: String,
        @Field("app_id") AppId: String,
        @Field("token") Token: String,
        @Field("token_type") token_type: String,
        @Field("refresh_token") refresh_token: String,
        @Field("expiration") expires: String

        ): Call<CommonResponseModel>

    @FormUrlEncoded
    @POST
    fun getUnlinkapps(
        @Url url:String,
        @Field("member_id") Id: String,
        @Field("type") Type: String,
        @Field("app_id") AppId: String
        ): Call<CommonResponseModel>

    @POST
    open fun fibitToken(@Url url : String , @Header("Content-Type") content_tpe : String, @Header("Authorization") Authorization : String): Call<FitBitModel>

    @FormUrlEncoded
    @POST
    open fun googltFitToken(@Url url : String , @Field("client_id") client_id: String,@Field("client_secret") client_secret: String,@Field("code") code: String,@Field("redirect_uri") redirect_uri: String, @Field("grant_type") grant_type : String): Call<GoogleFit>
}