package com.mcr.active.ui.fragment.home.bottomSheetFragment.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.databinding.LocationBotomSheetBlueBinding
import com.mcr.active.databinding.LocationBotomSheetWhiteBinding
import com.mcr.active.model.facilityModel.FacilityIminItemItem
import com.mcr.active.model.favouritModel.favLeisureModel.FavLeisureListModel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.favourit.FavouritLeisureAdapter
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.LocationWhiteViewModel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.PlaceAdapter
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.PlaceBlueAdapter
import com.orhanobut.hawk.Hawk
import java.util.*


class LocationBottomSheetFragmentSplash : BottomSheetDialogFragment() {
    var binding: LocationBotomSheetWhiteBinding? = null
    var bindingBlue: LocationBotomSheetBlueBinding? = null
    var viewmodel: LocationWhiteViewModel? = null
    private var FacilityList: List<FacilityIminItemItem> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CoffeeDialog)
        viewmodel = ViewModelProviders.of(activity!!).get(LocationWhiteViewModel::class.java)

        Util.hawk(this.context!!)
        Hawk.put("url","imin")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding  = DataBindingUtil.inflate(inflater,R.layout.location_botom_sheet_white,container,false)


        binding?.viewModel = viewmodel
        initObservables()
        return binding?.getRoot()
    }
    @SuppressLint("ResourceAsColor")
    private fun initObservables() {

        viewmodel?.locationClose?.observe(this, Observer {
            if (it!!) {
                dismiss()
                viewmodel?.locationClose?.value = false
            }

        })
        viewmodel?.permission?.observe(this, Observer {
            if (it!!) {
                Util.makeRequest(this!!.activity!!)
            }


        })
        viewmodel?.telephone_number?.observe(this, Observer {


                val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$it"))
                startActivity(intent)

        })

        FacilityList = Hawk.get("nearByFacility",FacilityList)
        viewmodel?.LocationiListLive =  FacilityList
        if (FacilityList.size!=0) {
            if (Hawk.get("page", "splash").equals("home")) {
                binding?.locationList?.visibility = View.GONE
                binding?.locationListBlue?.visibility = View.VISIBLE
                binding?.locationListBlue?.layoutManager = GridLayoutManager(context, 1)
                binding?.locationListBlue?.adapter = PlaceBlueAdapter(
                    viewmodel!!,
                    FacilityList
                )
                viewmodel!!.setLocationBlueAdapter(FacilityList, fragmentManager)
//                binding?.scrool?.setBackgroundResource(R.color.colorSplash)
                binding?.btLocation?.setBackgroundResource(R.color.colorSplash)
                binding?.btLocation?.setImageResource(R.drawable.ic_location_white_vt)
                binding?.txtBottomContent?.setTextColor(getResources().getColor(R.color.colorWhtie))
                binding?.txtBottomContent?.setBackgroundResource(R.color.colorSplash)
            }
            else {
                binding?.locationList?.visibility = View.VISIBLE
                binding?.locationListBlue?.visibility = View.GONE
                binding?.locationList?.layoutManager = GridLayoutManager(context, 1)
                binding?.locationList?.adapter = PlaceAdapter(viewmodel!!, FacilityList)
                viewmodel!!.setLocationInAdapter(FacilityList, fragmentManager)
//                binding?.scrool?.setBackgroundResource(R.color.colorWhtie)
                binding?.txtBottomContent?.setTextColor(getResources().getColor(R.color.colorSplash))
                binding?.txtBottomContent?.setBackgroundResource(R.color.colorWhtie)
                binding?.btLocation?.setBackgroundResource(R.color.colorWhtie)
                binding?.btLocation?.setImageResource(R.drawable.ic_location)
            }
        }
        else{
            binding?.lenProgress?.visibility = View.VISIBLE
            viewmodel?.lat?.value  =  Hawk.get("lat","")
            viewmodel?.log?.value = Hawk.get("log","")
            if (!viewmodel?.lat?.value.equals("") && !viewmodel?.log?.value.equals("")){
                viewmodel?.NearLocation()
            }

            viewmodel?.FacilityList1?.observe(
                this,
                Observer<List<FacilityIminItemItem?>?> { locationList ->
                    if (locationList==null || locationList!!.size == 0) {
//                        binding?.lenProgress?.visibility = View.GONE
                        binding?.spinKit?.visibility = View.GONE
                        binding?.txtNodata?.visibility = View.VISIBLE
                    }
                    else{
                        binding?.lenProgress?.visibility = View.GONE
                        if (Hawk.get("page", "splash").equals("home")) {
                            binding?.locationList?.visibility = View.GONE
                            binding?.locationListBlue?.visibility = View.VISIBLE
                            binding?.locationListBlue?.layoutManager = GridLayoutManager(context, 1)
                            binding?.locationListBlue?.adapter = PlaceBlueAdapter(
                                viewmodel!!,
                                locationList as List<FacilityIminItemItem>?
                            )
                            viewmodel!!.setLocationBlueAdapter(locationList, fragmentManager)
                            binding?.contant?.setBackgroundResource(R.color.colorSplash)
                            binding?.btLocation?.setBackgroundResource(R.color.colorSplash)
                            binding?.btLocation?.setImageResource(R.drawable.ic_location_white_vt)
                            binding?.txtBottomContent?.setTextColor(getResources().getColor(R.color.colorWhtie))


                        }
                        else {
                            binding?.locationList?.visibility = View.VISIBLE
                            binding?.locationListBlue?.visibility = View.GONE
                            binding?.locationList?.layoutManager = GridLayoutManager(context, 1)
                            binding?.locationList?.adapter = PlaceAdapter(viewmodel!!,
                                locationList as List<FacilityIminItemItem>?
                            )
                            viewmodel!!.setLocationInAdapter(locationList, fragmentManager)
                            binding?.contant?.setBackgroundResource(R.color.colorWhtie)
                            binding?.txtBottomContent?.setTextColor(getResources().getColor(R.color.colorSplash))
                            binding?.btLocation?.setBackgroundResource(R.color.colorWhtie)
                            binding?.btLocation?.setImageResource(R.drawable.ic_location)
                        }
                    }
                })
        }






        binding?.btLocation?.setOnClickListener(View.OnClickListener {

            dismiss()

        })
        binding?.btSearch?.setOnClickListener(View.OnClickListener {
            dismiss()
            fragmentManager?.let { it1 -> viewmodel?.showSearchDialogFragment(it1) }
        })
        binding?.btStar?.setOnClickListener(View.OnClickListener {
            dismiss()
            fragmentManager?.let { it1 -> viewmodel?.showFavouritDialogFragment(it1) }
        })


    }
//    fun dismis_location(){
//        dismiss()
//    }

//    fun  showLocatioInfoDialogFragment() {
//
//
//        val bottomSheetFragment = InfoBottomSheetSplashFragment()
//        fragmentManager?.let { bottomSheetFragment.show(it, bottomSheetFragment.getTag()) }
//
//    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode)
        {
            101 -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Log.i(ContentValues.TAG, "Permission has been denied by user")
                } else {
            viewmodel?.call(0)
                }
            }
        }
    }




}