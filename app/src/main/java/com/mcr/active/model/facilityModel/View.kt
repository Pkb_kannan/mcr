package com.mcr.active.model.facilityModel


import com.google.gson.annotations.SerializedName


data class View(

	@field:SerializedName("imin:itemsPerPage")
	val iminItemsPerPage: Int? = null,

	@field:SerializedName("last")
	val last: String? = null,

	@field:SerializedName("urlTemplate")
	val urlTemplate: String? = null,

	@field:SerializedName("imin:totalPages")
	val iminTotalPages: Int? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("first")
	val first: String? = null,

	@field:SerializedName("imin:currentPage")
	val iminCurrentPage: Int? = null
)