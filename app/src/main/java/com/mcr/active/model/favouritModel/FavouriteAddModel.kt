package com.mcr.active.model.favouritModel


import com.google.gson.annotations.SerializedName


data class FavouriteAddModel(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)