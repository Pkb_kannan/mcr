package com.mcr.active.model.newsAndEvends.list


import com.google.gson.annotations.SerializedName


data class NewsEvents(

	@field:SerializedName("news")
	val news: List<NewsItem?>? = null,

	@field:SerializedName("featured")
	val featured: List<FeaturedItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("events")
	val events: List<EventsItem?>? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)