package com.mcr.active.ui.fragment.splashScrool

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.mcr.active.MCRApp
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.databinding.GoogleFitPermissionDialogBinding
import com.mcr.active.databinding.SplasScroolBinding
import com.mcr.active.databinding.SplashBinding
import com.mcr.active.model.viewPagerModel.ViewPagerModel
import com.mcr.active.ui.MainActivity
import com.mcr.active.ui.activity.splash.SplashViewModel
import com.mcr.active.ui.fragment.home.HomeViewmodel
import com.mcr.active.ui.fragment.home.bottomSheetFragment.favourit.FavouritBottomSheetFragmentScrool
import com.mcr.active.ui.fragment.home.bottomSheetFragment.location.LocationBottomSheetFragmentSplash
import com.mcr.active.ui.fragment.home.bottomSheetFragment.search.SearchBottomSheetFragmentScrool
import com.mcr.active.ui.fragment.login.LoginDirections
import com.mcr.active.ui.fragment.splashScrool.SplashScroolDirections.*
import com.orhanobut.hawk.Hawk


class GoogleFilt : Fragment() {

    var binding: SplashBinding? = null
    var binding1: GoogleFitPermissionDialogBinding? = null
    var viewmodel: SplashViewModel? = null
    var dialog : Dialog? = null
    var views : View?= null
//    val navController = Navigation.findNavController(requireView())

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        Util.hawk(this.context!!)
        viewmodel = ViewModelProviders.of(activity!!).get(SplashViewModel::class.java)


    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding  = DataBindingUtil.inflate(inflater,R.layout.splash,container,false)
        binding?.viewModel = viewmodel


//        views = requireView()
//        if (Hawk.get("LoginStatus",false)) {

//        }

        initObservables()
        return binding?.root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initObservables() {
//        MainActivity()?.topHide(false)
        Util.hawk(this.context!!)
        Hawk.put("page","splash")
        permissiondialog()
//        Navigation.findNavController(requireView()).navigate(R.id.nav_home)

        viewmodel?.launchHome?.observeForever {
            if(it){
                viewmodel?.launchHome?.value = false
                (activity as MainActivity).naviControler()?.navigate(R.id.nav_home)
//                Navigation.findNavController(MainActivity().currentFocus!!).navigate(R.id.nav_home)
            }
        }
//        else {
//
////            subscribe()
////            requestRuntimePermissions()
//            if (hasRuntimePermissions()){
//                verifySession()
//            }
//            else{
//                requestRuntimePermissions()
//            }
//        }


    }

    @RequiresApi(Build.VERSION_CODES.M)
    override  fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 100) {
                readData()
            }
            else{
                viewmodel?.launchHome?.value = true
//                Navigation.findNavController(requireView()).navigate(R.id.nav_home)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun readData() {
        activity?.let {
            Fitness.getHistoryClient(
                it,
                GoogleSignIn.getLastSignedInAccount(activity)!!
            )
                .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
                .addOnSuccessListener { dataSet ->
                    val total =
                        if (dataSet.isEmpty) 0 else dataSet.dataPoints[0].getValue(
                            Field.FIELD_STEPS
                        ).asInt().toLong()
                    Log.e("step_count", "Total steps: $total")
                }
                .addOnFailureListener { e ->
                    Log.e(
                        "step_count",
                        "There was a problem getting the step count.",
                        e
                    )
                }
//            Navigation.findNavController(it.currentFocus!!).navigate(R.id.nav_home)
            viewmodel?.launchHome?.value = true
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestRuntimePermissions() {
        val shouldProvideRationale: Boolean =
            activity?.shouldShowRequestPermissionRationale(
                Manifest.permission.ACCESS_FINE_LOCATION
            )!!
        // Provide an additional rationale to the user. This would happen if the user denied the
// request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.e(
                TAG,
                "Displaying permission rationale to provide additional context."
            )
            Toast.makeText(activity,"permission_rationale", Toast.LENGTH_SHORT).show()
        } else {
            Log.e(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
// sets the permission in a given state or the user denied the permission
// previously and checked "Never ask again".
            activity?.requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                34
            )
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    private fun hasRuntimePermissions(): Boolean {
        val permissionState: Int = activity?.checkSelfPermission(
            Manifest.permission.ACCESS_FINE_LOCATION
        )!!
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    fun permissiondialog(){
        dialog = Dialog(this.context!!)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        viewmodel = ViewModelProviders.of(activity!!).get(SplashViewModel::class.java)
        binding1  = DataBindingUtil.inflate(LayoutInflater.from(dialog!!.context),R.layout.google_fit_permission_dialog,null,false)
        binding1?.root?.let { dialog!!.setContentView(it) }
//        dialog .setContentView(R.layout.home_dialog)
        binding1?.viewModel = viewmodel

        binding1?.btYes?.setOnClickListener {
            dialog!!.cancel()
            val fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .build()

            if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(activity), fitnessOptions)) {
                GoogleSignIn.requestPermissions(
                    this, // your activity
                    100,
                    GoogleSignIn.getLastSignedInAccount(activity),
                    fitnessOptions)
            }
            else{

                readData()
            }
        }
        binding1?.btNo?.setOnClickListener {
            dialog!!.cancel()
            viewmodel?.launchHome?.value = true
        }

        dialog!!.show()
    }

}