package com.mcr.active.model.findActivityListModel


import com.google.gson.annotations.SerializedName


data class Address(

	@field:SerializedName("imin:fullAddress")
	val iminFullAddress: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)