package com.mcr.active.model.EventDetailsModel


import com.google.gson.annotations.SerializedName


data class IminItem4Item(

	@field:SerializedName("imin:resultCount")
	val iminResultCount: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)