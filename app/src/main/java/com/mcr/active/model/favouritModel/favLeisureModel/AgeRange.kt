package com.mcr.active.model.favouritModel.favLeisureModel


import com.google.gson.annotations.SerializedName


data class AgeRange(

	@field:SerializedName("maxValue")
	val maxValue: Int? = null,

	@field:SerializedName("type")
	val type: String? = null
)