package com.mcr.active.model.LocationDetailModel


import com.google.gson.annotations.SerializedName


data class BetaVideoItem(

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)