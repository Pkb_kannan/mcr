package com.mcr.active.model.favouritModel.removeModel


import com.google.gson.annotations.SerializedName


data class RemoveFavModel(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)