package com.mcr.active.model.facilityModel


import com.google.gson.annotations.SerializedName


data class FacilityModel(

	@field:SerializedName("imin:totalItems")
	val iminTotalItems: Int? = null,

	@field:SerializedName("view")
	val view: View? = null,

	@field:SerializedName("imin:item")
	val iminItem: List<FacilityIminItemItem>? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("@context")
	val context: List<String?>? = null
)