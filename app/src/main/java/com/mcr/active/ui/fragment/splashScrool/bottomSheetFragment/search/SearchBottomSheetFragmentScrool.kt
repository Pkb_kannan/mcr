package com.mcr.active.ui.fragment.home.bottomSheetFragment.search

import `in`.madapps.placesautocomplete.PlaceAPI
import `in`.madapps.placesautocomplete.adapter.PlacesAutoCompleteAdapter
import `in`.madapps.placesautocomplete.listener.OnPlacesDetailsListener
import `in`.madapps.placesautocomplete.model.PlaceDetails
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import com.mcr.active.R
import com.mcr.active.databinding.SearchBottomSheetBinding
import com.mcr.active.ui.fragment.findActivity.DayAdapter
import com.mcr.active.ui.fragment.findActivity.findActivityResult.FindActAdapter
import com.mcr.active.ui.fragment.home.HomeViewmodel
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.search.DayAdapter2
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.search.DayAdapter3
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.search.SearchSpinAdapter
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.search.SearchViewModel
import com.orhanobut.hawk.Hawk
import java.util.*
import kotlin.collections.ArrayList

class SearchBottomSheetFragmentScrool : BottomSheetDialogFragment() ,OnRangeChangedListener {

    var binding: SearchBottomSheetBinding? = null
    var viewmodel: SearchViewModel? = null
    var placesApi : PlaceAPI? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CoffeeDialog)
        viewmodel = ViewModelProviders.of(activity!!).get(SearchViewModel::class.java)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding  = DataBindingUtil.inflate(inflater,R.layout.search_bottom_sheet,container,false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.getRoot()
    }

    private fun initObservables() {
        initDistanceSpinner()
        initSearchSpinner()

        viewmodel?.strDistance = ObservableField("")
        viewmodel?.strDistanceposition = ObservableField("")
        viewmodel?.strActivityType = ObservableField("")
        viewmodel?.timeFromTo = ObservableField("TIME")
//        viewmodel?.tvText= ObservableField("")
        viewmodel?.sportName= ObservableField("")

        placesApi = PlaceAPI.Builder()
            .apiKey("AIzaSyBLxxS1iGqJMu3y2KwOnbIwvR-8xBeYuig")
            .build(this.context!!)
        binding?.tvM1?.setAdapter(PlacesAutoCompleteAdapter(this.context!!, placesApi!!))
        binding?.tvM1?.onItemClickListener =
            AdapterView.OnItemClickListener { parent, _, position, _ ->
                val place = parent.getItemAtPosition(position) as `in`.madapps.placesautocomplete.model.Place
                if (!place.id.toString().equals("-1")) {
                    binding?.tvM1?.setText(place.description)
                    getPlaceDetails(place.id)
                }
            }

        viewmodel!!.selectedItem!!.clear()
//        locButton()
        val list =
            java.util.ArrayList<String>(Arrays.asList(*resources.getStringArray(R.array.dayList)))
        viewmodel?.list =
            java.util.ArrayList<String>(Arrays.asList(*resources.getStringArray(R.array.weekName)))
        viewmodel?.launchFindResult?.observe(this, androidx.lifecycle.Observer {
            if (it!!)
            {   Hawk.put("url","imin")
                Navigation.findNavController(this.view!!).navigate(R.id.nav_findactivity_result)
//                Navigation.findNavController(requireView()).navigate(actionNavLocationToNavFindactivityResult())
//                navController.navigate(R.id.)
            }
        })
        binding?.recyDay?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL ,false)
        binding?.recyDay?.adapter = DayAdapter3(viewmodel!!, list)
        viewmodel!!.setDayInAdapter(list)
        viewmodel?.refreshDayAdapter?.observe(this, androidx.lifecycle.Observer {
            if (it!!) {
                binding?.recyDay?.adapter?.notifyDataSetChanged()
                viewmodel?.refreshDayAdapter?.value = false
            }
        })


//        initSpinner()

        binding?.btLocation?.setOnClickListener(View.OnClickListener {

            dismiss()
            fragmentManager?.let { it1 -> viewmodel?.showLocationDialogFragment(it1) }

        })
        binding?.btSearch?.setOnClickListener(View.OnClickListener {
            dismiss()
        })
        binding?.btStar?.setOnClickListener(View.OnClickListener {
            dismiss()
            fragmentManager?.let { it1 -> viewmodel?.showFavouritDialogFragment(it1) }
        })

        binding?.seekbar?.setOnRangeChangedListener(this)

    }
    override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
    }

    override fun onRangeChanged(
        view: RangeSeekBar?,
        leftValue: Float,
        rightValue: Float,
        isFromUser: Boolean
    ) {
//        Log.e("Progress", ""+leftValue.toInt())
        if (leftValue.toInt()==9 && rightValue.toInt()==9)
            viewmodel?.timeFromTo?.set("09:00 - 09:00")

//        else
//        viewmodel?.timeFromTo?.set(""+leftValue.toInt()+":00 - "+rightValue.toInt()+":00")
        else if (leftValue.toInt()==9 )
            viewmodel?.timeFromTo?.set("09:00 - "+rightValue.toInt()+":00")
        else if (leftValue.toInt()==0 && rightValue.toInt()==0)
            viewmodel?.timeFromTo?.set("TIME")
        else
            viewmodel?.timeFromTo?.set(""+leftValue.toInt()+":00 - "+rightValue.toInt()+":00")

    }

    override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
    }

    private fun getPlaceDetails(placeId: String) {
        placesApi?.fetchPlaceDetails(placeId, object :
            OnPlacesDetailsListener {
            override fun onError(errorMessage: String) {
                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
            }

            override fun onPlaceDetailsFetched(placeDetails: PlaceDetails) {
                viewmodel?.tvText?.set(placeDetails.lat.toString()+","+placeDetails.lng.toString())

//                setupUI(placeDetails)
            }
        })
    }


    private fun initDistanceSpinner() {
        val distanceList =
            java.util.ArrayList<String>(Arrays.asList(*resources.getStringArray(R.array.Distance)))
        binding?.distanceSpinner!!.adapter = SearchSpinAdapter(this.requireActivity(), distanceList)

        binding?.distanceSpinner?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onNothingSelected(adapterView: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    adapterView: AdapterView<*>?,
                    p1: View?,
                    pos: Int,
                    p3: Long
                ) {
                    binding!!.txtDistance.text = adapterView?.selectedItem.toString()

                    viewmodel?.strDistance?.set(pos.toString())
                }

            }

    }

    private fun initSearchSpinner() {
        val daysList =
            java.util.ArrayList<String>(Arrays.asList(*resources.getStringArray(R.array.days)))
        binding?.searchSpinner!!.adapter = SearchSpinAdapter(this.requireActivity(), daysList)

        binding?.searchSpinner?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onNothingSelected(adapterView: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    adapterView: AdapterView<*>?,
                    p1: View?,
                    pos: Int,
                    p3: Long
                ) {
                    binding!!.spText.text = adapterView?.selectedItem.toString()

//                    viewmodel?.strDistance?.set(pos.toString())
                }

            }

    }

//    fun initSpinner() {
//
//        var spinnerList= ArrayList<String>()
//        spinnerList.add("WHAT WOULD YOU LIKE TO DO TODAY?")
//        spinnerList.add("Find organised sport or physical activity session")
//        spinnerList.add("Search local parks and leisure facilities")
//        spinnerList.add("Book a pitch, court or studio")
//        spinnerList.add("Join a club, team or group")
//        spinnerList.add("Find family activity")
//
//        binding?.searchSpinner!!.adapter=SearchSpinAdapter(this.requireActivity(),spinnerList)
//
//        binding?.searchSpinner?.onItemSelectedListener =
//            object : AdapterView.OnItemSelectedListener {
//
//                override fun onNothingSelected(adapterView: AdapterView<*>?) {
//
//                }
//                override fun onItemSelected(
//                    adapterView: AdapterView<*>?,
//                    p1: View?,
//                    pos: Int,
//                    p3: Long
//                ) {
//                 binding!!.spText.text= adapterView?.selectedItem.toString()
//                }
//
//            }
//
//
//    }
//    private fun locButton() {
//        if (  Hawk.get("location",false)){
//
//            binding?.btLocation?.setBackgroundResource(R.color.colorSplash)
//            binding?.btLocation?.setImageResource(R.drawable.ic_location_white_vt)
//        }
//        else
//        {
//            binding?.btLocation?.setBackgroundResource(R.color.colorWhtie)
//            binding?.btLocation?.setImageResource(R.drawable.ic_location_white_sp)
//        }
//    }
//


}