package com.sweetzpot.stravazpot.authenticaton.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Token implements Parcelable {
    private String value;

    public Token(String value) {
        this.value = value;
    }

    protected Token(Parcel in) {
        value = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Token> CREATOR = new Creator<Token>() {
        @Override
        public Token createFromParcel(Parcel in) {
            return new Token(in);
        }

        @Override
        public Token[] newArray(int size) {
            return new Token[size];
        }
    };

    @Override
    public String toString() {
        return "Bearer " + value;
    }
}
