package com.mcr.active.ui.fragment.findActivity.findActivityResult.findActivityList

import android.app.Application
import android.os.Build
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.model.FindActivityMapModel.FindMapModel
import com.mcr.active.model.LocationDetailModel.LocationDetailsModel
import com.mcr.active.model.findActivityListModel.FindActivityListModel
import com.mcr.active.model.findActivityListModel.IminItemItem
//import com.mcr.active.model.findActivityModel.FindActivity
import com.mcr.active.network.Api
import com.orhanobut.hawk.Hawk
import com.mcr.active.base.BaseViewModel
import retrofit2.Call
import retrofit2.Response
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import javax.inject.Inject


class FindListViewModel(application: Application) : BaseViewModel(application) {

    @Inject
    lateinit var api: Api
    private var adapter: ActivityListAdapter? = null
    var loadComplect: SingleLiveEvent<Boolean>? = null
    var filterList = ArrayList<HashMap<String, Any>>()
    var loadComplect1: SingleLiveEvent<Boolean>? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    var filterMap: HashMap<String, Any> = HashMap<String, Any>()
    private var LocationiList: List<IminItemItem> = ArrayList<IminItemItem>()
    private var LocationiListLive: MutableLiveData<List<IminItemItem>> = MutableLiveData<List<IminItemItem>>()
    private var LocationDetailsLive: MutableLiveData<LocationDetailsModel> = MutableLiveData<LocationDetailsModel>()

    init {
        loadComplect = SingleLiveEvent<Boolean>()
        loadComplect1 = SingleLiveEvent<Boolean>()
        filterList = Hawk.get("findactvities", filterList)
        filterMap = filterList.get(0)
        search()

    }

    fun listLocation(): MutableLiveData<List<IminItemItem>> {
        return LocationiListLive
    }


    fun search() {
//        launchFindMapFragment?.value = true
//
        loadComplect?.value = false
        isProgress.set(View.VISIBLE)
        var selectedItem: ArrayList<String>? = ArrayList()
        var call: Call<FindActivityListModel>? = null
        val list = filterMap["timeRange"].toString().split("-")
        selectedItem = filterMap["days"] as ArrayList<String>?
        if (!filterMap["sportName"].toString()?.equals("") && selectedItem?.size != 0 && !filterMap["actvityType"].toString().equals(
                "ACTIVITY TYPE"
            ) && !filterMap["timeRange"].toString().equals("TIME")
        ) {
            call = api.geteventListFullFilter(
                filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()),
                "upcoming-sessions",
                "1",
                "20",
                list[0].replace(" ", ""),
                list[1].replace(" ", ""),
                selectedItem!!, Util.getIminToken()
            )
        } else if (!filterMap["timeRange"].toString().equals("TIME")) {
            call = api.geteventListTimeFilter(
                filterMap["zip"].toString() + "," + Util.milToKm(filterMap["distance"].toString()),
                "upcoming-sessions",
                "1",
                "20", list[0].replace(" ", ""),
                list[1].replace(" ", ""),
                Util.getIminToken()
            )
        } else {
            call = api.getPlace(
                "51.7520131,-1.2578499,5",
                "upcoming-sessions",
                "1",
                "20",
                Util.getIminToken()
            )
        }
        call?.enqueue(object : retrofit2.Callback<FindActivityListModel> {
            override fun onFailure(call: Call<FindActivityListModel>?, t: Throwable?) {
                Log.e("response", t?.message)
                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<FindActivityListModel>?,
                response: Response<FindActivityListModel>?
            ) {

                val gson = Gson()
                val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                Log.e("list_value", jsonElement.toString())
                val respnsEvents = gson.fromJson(jsonElement, FindActivityListModel::class.java)
                if (response != null) {
                    if (response.body()!!.iminItem != null) {
                        isProgress.set(View.GONE)
                        LocationiListLive?.value = response?.body()?.iminItem!!
                        loadComplect?.value = true
                    } else {
                        isProgress.set(View.GONE)
                        loadComplect?.value = true
                    }
                }
            }

        })
    }

    fun LocationDetails(): MutableLiveData<LocationDetailsModel> {

        return LocationDetailsLive
    }

    fun searchDetails(url: String, location_id: String) {
//        launchFindMapFragment?.value = true
//
        loadComplect1?.value = false
        isProgress.set(View.VISIBLE)
        val call = api.getPlaceDetails(url, "dwlR7D50eTzWYGQnmzUZ4hD7Gu2KtxIj")
        call.enqueue(object : retrofit2.Callback<LocationDetailsModel> {
            override fun onFailure(call: Call<LocationDetailsModel>?, t: Throwable?) {
                Log.e("response", t?.message)
                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<LocationDetailsModel>?,
                response: Response<LocationDetailsModel>?
            ) {

                val gson = Gson()
                val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                Log.e("Location_details_value", jsonElement.toString())
                val respnsEvents = gson.fromJson(jsonElement, LocationDetailsModel::class.java)

                isProgress.set(View.GONE)
                LocationDetailsLive?.value = response?.body()
                loadComplect1?.value = true
            }

        })
    }

    fun setLocationInAdapter(noti: List<IminItemItem>) {
        adapter?.setLocation(noti)
        adapter?.notifyDataSetChanged()
    }

    fun getPlace(index: Int?): IminItemItem? {
        return if (LocationiListLive.getValue() != null && index != null && LocationiListLive.getValue()!!.size > index
        ) {
            LocationiListLive.getValue()!!.get(index)
        } else null
    }

    fun getPlaceCategory(index: Int?): String? {
        return if (LocationiListLive.getValue() != null && index != null && LocationiListLive.getValue()!!.size > index
        ) {
            LocationiListLive.value?.get(index)?.name
        } else null
    }

    fun getPlaceAddress(index: Int?): String? {
        return if (LocationiListLive.getValue() != null && index != null && LocationiListLive.getValue()!!.size > index
        ) {
            LocationiListLive.value?.get(index)?.iminLocationSummary?.get(0)?.name
        } else null
    }

    fun getPlaceDistance(index: Int?): String? {
        return if (LocationiListLive.getValue() != null && index != null && LocationiListLive.getValue()!!.size > index
        ) {
            (BigDecimal(0.6214 * LocationiListLive.value?.get(index)?.iminLocationSummary?.get(0)?.geo?.iminDistanceFromGeoQueryCenter?.value!!).setScale(
                2,
                RoundingMode.HALF_EVEN
            )).toString() + " Miles Away"
        } else null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getPlaceStartDate(index: Int?): String? {
        return if (LocationiListLive.getValue()?.get(0)?.subEvent?.get(0)?.subEvent1 != null && index != null && LocationiListLive.getValue()!!.size > index
        ) {
            val inFormat = SimpleDateFormat("yyy-MM-dd")
            Log.e(
                "date",
                LocationiListLive.getValue()?.get(0)?.subEvent?.get(0)?.subEvent1?.get(0)?.startDate
            )
            val parts = LocationiListLive.getValue()?.get(0)?.subEvent?.get(0)?.subEvent1?.get(0)
                ?.startDate?.split("T")
            val date = inFormat.parse(parts?.get(0)?.toString())
            val outFormat = SimpleDateFormat("EEEE")
            val goal = outFormat.format(date)

            val inputFormat: DateFormat = SimpleDateFormat("HH:mm:ss")
            val outputFormat: DateFormat =
                SimpleDateFormat("KK:mm a")
//            System.out.println()

            goal + " " + outputFormat.format(inputFormat.parse(parts?.get(1)?.replace("Z", "")))
        } else null
//        {
//            val current = LocalDateTime.now()
//            val inFormat = SimpleDateFormat("yyy-MM-dd")
//            val date = inFormat.parse(current.toString())
//            val outFormat = SimpleDateFormat("EEEE")
//            current.toString()?.split("T").get(0)
//        }
    }

    fun getPlaceEndDate(index: Int?): String? {
        return if (LocationiListLive.getValue()?.get(0)?.subEvent?.get(0)?.subEvent1?.get(0)?.endDate != null && index != null && LocationiListLive.getValue()!!.size > index
        ) {
            Log.e(
                "time_open",
                LocationiListLive.getValue()?.get(0)?.subEvent?.get(0)?.subEvent1?.get(0)?.endDate
            )
            val inFormat = SimpleDateFormat("yyy-MM-dd")
            val parts = LocationiListLive.getValue()?.get(0)?.subEvent?.get(0)?.subEvent1?.get(0)
                ?.endDate?.split("T")
            val date = inFormat.parse(parts?.get(0)?.toString())
            val outFormat = SimpleDateFormat("EEEE")
            val goal = outFormat.format(date)

            val inputFormat: DateFormat = SimpleDateFormat("HH:mm:ss")
            val outputFormat: DateFormat =
                SimpleDateFormat("KK:mm a")
//            System.out.println()

            goal + " " + outputFormat.format(inputFormat.parse(parts?.get(1)?.replace("Z", "")))
        } else null
    }
}