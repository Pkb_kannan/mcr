package com.mcr.active.model


import com.google.gson.annotations.SerializedName


data class DayModel(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("value")
	val value: String? = null
)