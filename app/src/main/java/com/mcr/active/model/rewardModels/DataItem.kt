package com.mcr.active.model.rewardModels


import com.google.gson.annotations.SerializedName


data class DataItem(

	@field:SerializedName("rewards_created_date")
	val rewardsCreatedDate: String? = null,

	@field:SerializedName("rewards_modified_date")
	val rewardsModifiedDate: String? = null,

	@field:SerializedName("rewards_close_date")
	val rewardsCloseDate: String? = null,

	@field:SerializedName("rewards_close_time")
	val rewardsCloseTime: String? = null,


	@field:SerializedName("rewards_image")
	val rewardsImage: String? = null,

	@field:SerializedName("rewards_kudos")
	val rewardsKudos: String? = null,

	@field:SerializedName("rewards_status")
	val rewardsStatus: String? = null,

	@field:SerializedName("rewards_id")
	val rewardsId: String? = null,

	@field:SerializedName("client_id")
	val clientId: String? = null,

	@field:SerializedName("rewards_name")
	val rewardsName: String? = null,

	@field:SerializedName("rewards_description")
	val rewardsDescription: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)