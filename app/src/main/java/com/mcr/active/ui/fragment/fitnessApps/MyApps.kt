package com.mcr.active.ui.fragment.fitnessApps

import android.app.Activity
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Scope
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.tasks.Task
//import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
//import com.google.api.client.json.jackson2.JacksonFactory
//import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest
//import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
//import com.google.api.client.http.javanet.NetHttpTransport
//import com.google.api.client.json.jackson2.JacksonFactory
import com.mcr.active.R
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.databinding.MyAppsFragmentBinding
import com.mcr.active.model.getApps.AppDetailsItem
import com.mcr.active.ui.activity.strava.StravaActivity
import com.uml.deercoderi.fitbitmodule.EasySocialFitbit
import com.uwanttolearn.easysocial.EasySocialAuthActivity
import com.uwanttolearn.easysocial.EasySocialCredential
import java.io.FileReader

@Suppress("DEPRECATION")
class MyApps : Fragment() {


    var signInIntent : Intent? = null
    var viewmodel: MyAppsViewModel? = null
    var mAdapter: MyLinkedAppsAdapter? = null
    var binding: MyAppsFragmentBinding? = null
    val listdetails: ArrayList<String>? = arrayListOf()
    var mGoogleSignInClient : GoogleSignInClient? = null
    private var mEasySocialFitbit: EasySocialFitbit? = null

    companion object {

        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }

        var fitibit: SingleLiveEvent<Boolean>? = null
        private val RQ_LOGIN = 1001
        private val CLIENT_ID = 40180
        private val CLIENT_SECRET = "2190196088f7f71949bcb54b96662c9e094b52c5"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        viewmodel = ViewModelProviders.of(this).get(MyAppsViewModel::class.java)
        viewmodel?.getLinkedApps()
        fitibit = SingleLiveEvent<Boolean>()
        Util.hawk(context!!)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.my_apps_fragment, container, false)
        binding?.viewModel = viewmodel
        return binding?.getRoot()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        var applist= arrayListOf<GetAllAppsModel>()
//      var  applist=viewmodel?.userData
        /** Initialize Objects, (AppId, secretId, redirect_url, responsetype)  */
        val mCredentials = EasySocialCredential.Builder(
            "227NCS",
            "a6f5a2f9c4c8333609be680b7ee976be",
            "http://www.cs.uml.edu",
            "code"
        ).setPermissions(
            arrayOf(
                "activity",
                "nutrition",
                "heartrate",
                "location",
                "sleep",
                "profile",
                "settings",
                "social",
                "weight"
            )
        )
            .build()

        val serverClientId = getString(R.string.default_web_client_id)
        val gso1 = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestScopes(Scope(Scopes.FITNESS_ACTIVITY_READ))
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestServerAuthCode(serverClientId)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this.requireContext(), gso1)


        mEasySocialFitbit = EasySocialFitbit.getInstance(mCredentials)
        viewmodel?.getLinkedApps()?.observe(
            this, Observer<List<AppDetailsItem>>
                (fun(allappsList: List<AppDetailsItem>) {
                if (allappsList.size == 0) {

                } else {
                    mAdapter = MyLinkedAppsAdapter(context, allappsList, viewmodel)
                    binding?.appRecyclerview!!.layoutManager = GridLayoutManager(activity, 3)
                    binding?.appRecyclerview!!.adapter = mAdapter
                }
            })
        )

        viewmodel?.stravalaung?.observeForever {
            if (it) {
                viewmodel?.stravalaung?.value = false
                val i = Intent(context, StravaActivity::class.java)
                startActivityForResult(i, 100)
            }

        }
        viewmodel?.refresAdadpter?.observeForever {
            if (it) {
                mGoogleSignInClient?.revokeAccess()
                mGoogleSignInClient?.signOut()

                viewmodel?.refresAdadpter?.value = false
                binding?.appRecyclerview!!.adapter?.notifyDataSetChanged()
            }
        }
        viewmodel?.refresAdadpterSignIn?.observeForever {
            if (it) {

                viewmodel?.refresAdadpterSignIn?.value = false
                binding?.appRecyclerview!!.adapter?.notifyDataSetChanged()
            }
        }
        viewmodel?.fitibit?.observeForever {

            if (it) {
                viewmodel?.fitibit?.value = false
                mEasySocialFitbit?.login(context as Activity?, 2)
            }

        }
        fitibit?.observeForever {
            if (it) {
                fitibit?.value = false
                viewmodel?.fitbitToken()
            }
        }

        viewmodel?.googlfit?.observeForever {

            if (it) {
                viewmodel?.googlfit?.value = false

                 signInIntent = mGoogleSignInClient?.signInIntent
                startActivityForResult(signInIntent, 101)

            }

        }
        viewmodel?.googlfitRlogin?.observeForever {

            if (it) {
                viewmodel?.googlfitRlogin?.value = false
                mGoogleSignInClient?.revokeAccess()
                mGoogleSignInClient?.signOut()
                 signInIntent = mGoogleSignInClient?.signInIntent
                startActivityForResult(signInIntent, 101)

            }

        }

    }


//    @RequiresApi(Build.VERSION_CODES.M)
//    private fun readData() {
//        activity?.let {
//            Fitness.getHistoryClient(
//                it,
//                GoogleSignIn.getLastSignedInAccount(activity)!!
//            )
//                .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
//                .addOnSuccessListener { dataSet ->
//                    val total =
//                        if (dataSet.isEmpty) 0 else dataSet.dataPoints[0].getValue(
//                            Field.FIELD_STEPS
//                        ).asInt().toLong()
//                    Log.e("step_count", "Total steps: $total")
//                }
//                .addOnFailureListener { e ->
//                    Log.e(
//                        "step_count",
//                        "There was a problem getting the step count.",
//                        e
//                    )
//                }
//
//            GoogleSignIn.getLastSignedInAccount(activity)?.idToken
//
////            viewmodel?.getLinkUnlinkApps(2,
////                "fghkjhASDKLMNXL;CJFLAS/;RLK;LSDF"
////            )
////            Navigation.findNavController(it.currentFocus!!).navigate(R.id.nav_home)
////            viewmodel?.launchHome?.value = true
//        }
//
//    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == 100) {
                val returnValue = data!!.getStringExtra("token")
                val expiresAt = data!!.getStringExtra("expiresAt")
                val expiresIn = data!!.getStringExtra("expiresIn")
                val tokenType = data!!.getStringExtra("tokenType")
                val refreshToken = data!!.getStringExtra("refreshToken")
                val code = data!!.getStringExtra("code")

                listdetails?.add(data!!.getStringExtra("token"))
                listdetails?.add(expiresAt)
                listdetails?.add(expiresIn)
                listdetails?.add(tokenType)
                listdetails?.add(refreshToken)
                listdetails?.add(code)
                viewmodel?.getLinkUnlinkApps(0, listdetails)
            } else if (requestCode == 2) {
                /** Handle the authentication response  */
                mEasySocialFitbit!!.loginResponseHandler(activity, data)
                val t = data?.getStringExtra(EasySocialAuthActivity.ACCESS_TOKEN)
                Toast.makeText(context, t, Toast.LENGTH_SHORT).show()
            } else if (requestCode == 101) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task)

            }


        } else {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)


        }
    }



    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)


            if (account != null) {
//                getrefes(account?.serverAuthCode!!)
                account?.idToken
//                var listdetails: ArrayList<String> = arrayListOf()
//                listdetails?.add(account?.idToken!!)
//                listdetails?.add(account?.serverAuthCode!!!!)
                viewmodel?.getGoogleFitDetails(account?.serverAuthCode!!, account?.idToken!!)
//                viewmodel?.getLinkUnlinkApps(2,listdetails)
//
            }
            // updateUI(account);
        } catch (e: ApiException) {


            val xyz = e.message.toString()
//            val j = xyz
//            Log.v("xyz", xyz)

        }

    }
}
