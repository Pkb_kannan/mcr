package com.mcr.active.ui.fragment.signup

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.mcr.active.MCRApp
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.databinding.SignUpBinding
import com.mcr.active.ui.MainActivity
import com.mcr.active.ui.fragment.signup.RegisterFragmentDirections.actionRegisterToMoredetails
import com.orhanobut.hawk.Hawk
import java.util.*


class RegisterFragment : Fragment(),AdapterView.OnItemSelectedListener {

    var binding: SignUpBinding? = null
    var viewmodel: RegViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewmodel = ViewModelProviders.of(activity!!).get(RegViewModel::class.java)
        context?.let { Util?.hawk(it) }
        Hawk.put("url","imin")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.sign_up, container, false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.root

    }



    private fun initObservables() {
        (activity as MainActivity).sidemenuHide(false)
//        try {
//            binding?.spinnerEthnicity!!.setOnItemSelectedListener(object :
//                AdapterView.OnItemSelectedListener {
//                override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
//                    viewmodel!!.ethnicty!!.set(adapterView.getItemAtPosition(i).toString())
//                }
//
//                override fun onNothingSelected(adapterView: AdapterView<*>) {
//
//                }
//            })
//        } catch (e: Exception) {
//        }
        binding?.spinnerEthnicity?.setOnItemSelectedListener(this)
        viewmodel?.getEthnicity()
        binding?.dob?.setOnClickListener(View.OnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = activity?.let { it1 ->
                DatePickerDialog(it1, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in TextView
                    //                    textView.setText("" + dayOfMonth + " " + month + ", " + year)
                    viewmodel?.dob?.set("" + dayOfMonth + "/" + monthOfYear+1 + "/" + year)
                }, year, month, day)
            }
            dpd!!.show()
            dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
        })

        viewmodel?.launchMoredetailActivity?.observe(this, Observer {
            if (it!!)
            {
                Navigation.findNavController(requireView()).navigate(R.id.moredetails)
            }
        })
        binding?.close?.setOnClickListener {
//            Navigation.findNavController(requireView()).navigate(R.id.nav_splash)
            (activity as MainActivity).onBackPressed()
        }
        binding?.txtSigin?.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        viewmodel!!.ethnicty!!.set(parent?.getItemAtPosition(position).toString())
    }

}