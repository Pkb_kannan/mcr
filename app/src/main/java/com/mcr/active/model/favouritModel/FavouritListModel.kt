package com.mcr.active.model.favouritModel


import com.google.gson.annotations.SerializedName


data class FavouritListModel(

	@field:SerializedName("data")
	val data: List<FavouritDataItem>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)