package com.mcr.active.ui.activity.splash

import android.app.Application
import android.util.Log
import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.model.FindActivityMapModel.FindMapModel
import com.mcr.active.model.FindActivityMapModel.IminItemItem
import com.mcr.active.model.VenuListModel.DataItem
import com.mcr.active.model.VenuListModel.VenuListModel
import com.mcr.active.model.facilityModel.FacilityIminItemItem
import com.mcr.active.model.facilityModel.FacilityModel
import com.mcr.active.model.favouritModel.favouritrIminItemModel.FavActivityModel
import com.mcr.active.model.findActivityModel.FindActivity
import com.mcr.active.network.Api
import com.orhanobut.hawk.Hawk
import com.mcr.active.base.BaseViewModel
import retrofit2.Call
import retrofit2.Response
import java.util.*
import java.util.Collections.addAll
import javax.inject.Inject


class SplashViewModel(application: Application) : BaseViewModel(application) {

    @Inject
    lateinit var api: Api
    var page = 1
    var item = 0
    var location  = ""
    val FacList = ArrayList<FacilityIminItemItem>()
    var launchHome: SingleLiveEvent<Boolean>? = null
    var launchMainActivity: SingleLiveEvent<Boolean>? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    var logitud: MutableLiveData<String> = MutableLiveData<String>()
    var latitius: MutableLiveData<String> = MutableLiveData<String>()
    private var LocationiList: List<DataItem> = ArrayList<DataItem>()
    private var LocationiListImin: List<FindActivity> = ArrayList<FindActivity>()
    private var LocationiListLive: List<IminItemItem> =  ArrayList<IminItemItem>()
    private var FacilityList: List<FacilityIminItemItem> = ArrayList<FacilityIminItemItem>()
    private var FacilityList1: MutableLiveData<List<FacilityIminItemItem>> =MutableLiveData<List<FacilityIminItemItem>>()

    init{

        launchMainActivity = SingleLiveEvent<Boolean>()
        launchHome = SingleLiveEvent<Boolean>()
//        launchMainActivity?.value = true
//        NearLocation()
    }

    fun ListPlaces(): MutableLiveData<List<FacilityIminItemItem>>? {

        return null
    }

    fun search(){
        launchMainActivity?.value = false
        isProgress.set(View.VISIBLE)
//        val call = api.getVenuList("53.5121803","-2.2363999","5")
        val call = api.getVenuList(latitius.value.toString(),logitud.value.toString(),"5")
        call.enqueue(object : retrofit2.Callback<VenuListModel>
        {
            override fun onFailure(call: Call<VenuListModel>?, t: Throwable?) {
                //Log.e("response", t?.message)
                isProgress.set(View.GONE)
                launchMainActivity?.value = false
            }

            override fun onResponse(
                call: Call<VenuListModel>?,
                response: Response<VenuListModel>?) {

//                val gson = Gson()
//                val jsonElement: JsonElement = gson.toJsonTree(response?.body())
//                //Log.e("list_value", jsonElement.toString())
//                val respnsEvents = gson.fromJson(jsonElement, VenuListModel::class.java)

                LocationiList = response?.body()?.data!!
//                //Log.e("list_value1", LocationiList.get(0)?.geo?.latitude.toString())
//                LocationiListLive.value = response?.body()?.data!!
                isProgress.set(View.GONE)
                launchMainActivity?.value = true
            }

        })
    }


    fun NearLocation(){

        launchMainActivity?.value = false
        isProgress.set(View.VISIBLE)
        val call = api.getPlaceFacility("51.588382973396,-0.074243442870947,5","10",page.toString(),Util.getTime(),"dwlR7D50eTzWYGQnmzUZ4hD7Gu2KtxIj")
//        val call = api.getPlaceFacility(latitius.value.toString()+","+logitud.value.toString()+",5","10",page.toString(),Util.getTime(),"dwlR7D50eTzWYGQnmzUZ4hD7Gu2KtxIj")
        call.enqueue(object : retrofit2.Callback<FindMapModel>
        {
            override fun onFailure(call: Call<FindMapModel>?, t: Throwable?) {
                //Log.e("response", t?.message)
                Hawk.put("nearByFacility", FacilityList)

                isProgress.set(View.GONE)
                launchMainActivity?.value = true
            }

            override fun onResponse(
                call: Call<FindMapModel>?,
                response: Response<FindMapModel>?
            ) {
                if (response?.isSuccessful!!) {
                    val gson = Gson()
                    val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                    //Log.e("pkb_facility", jsonElement.toString())
                    LocationiListLive = response?.body()?.iminItem!!
                    Hawk.put("nearByLocation", LocationiListLive)
                    if (LocationiListLive.size == 0) {
                        FacilityList = FacilityList1.value!!
                        Hawk.put("nearByFacility", FacilityList)

                        isProgress.set(View.GONE)
                        launchMainActivity?.value = true
                    } else {
                        FacilityFilter(0)
                    }
                }
                else{
                    Hawk.put("nearByFacility", FacilityList)

                    isProgress.set(View.GONE)
                    launchMainActivity?.value = true
                }
            }

        })
    }
    fun FacilityFilter(position : Int) {

        val count = position
        if (count + 1 != LocationiListLive.size){
            location = LocationiListLive.get(count).geo?.latitude.toString() + "," + LocationiListLive.get(
                    count
                ).geo?.longitude.toString() + ",10"
        val call = api.getFacility(
            location!!,//"51.588302,-0.074138,5",//location!!,
            "1",
            "20",
            Util.getDateTime(),
            "upcoming-slots",
            "dwlR7D50eTzWYGQnmzUZ4hD7Gu2KtxIj"
        )
        call.enqueue(object : retrofit2.Callback<FacilityModel> {
            override fun onFailure(call: Call<FacilityModel>?, t: Throwable?) {
                //Log.e("response", t?.message)
                isProgress.set(View.GONE)
                Hawk.put("nearByFacility", FacilityList)
                isProgress.set(View.GONE)
                launchMainActivity?.value = true
            }

            override fun onResponse(
                call: Call<FacilityModel>?,
                response: Response<FacilityModel>?
            ) {
                val gson = Gson()
                val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                //Log.e("list_facility1", jsonElement.toString())
                if (response?.isSuccessful!!) {
                    if (response?.body()?.iminTotalItems != 0) {
                        FacList.addAll(response?.body()?.iminItem!!)
                        if (FacList.size < 3)
                            FacilityFilter(count + 1)
                        else {
                            FacilityList = FacList
                            Hawk.put("nearByFacility", FacilityList)
                            isProgress.set(View.GONE)
                            launchMainActivity?.value = true
                        }
                    } else {
                        FacilityFilter(count + 1)
                    }
                }

            }
        })
    }
        else{
            Hawk.put("nearByFacility", FacilityList)
            isProgress.set(View.GONE)
            launchMainActivity?.value = true
        }

    }




}