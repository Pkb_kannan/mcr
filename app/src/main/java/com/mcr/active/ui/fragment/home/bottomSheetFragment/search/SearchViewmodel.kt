package com.mcr.active.ui.fragment.home.bottomSheetFragment.search

import android.app.Application
import android.view.View
import android.widget.AdapterView
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel


class SearchViewmodel (application: Application) : AndroidViewModel(application){
    var textspin: ObservableField<String>? = null

    init{
        textspin = ObservableField("")
    }
    fun onSelectItem(
        parent: AdapterView<*>?,
        view: View?,
        pos: Int,
        id: Long
    ) {
        //pos                                 get selected item position
//view.getText()                      get lable of selected item
//parent.getAdapter().getItem(pos)    get item by pos
//parent.getAdapter().getCount()      get item count
//parent.getCount()                   get item count
//parent.getSelectedItem()            get selected item
//and other...
    }

    fun  showSearchDialogFragment() {
        val bottomSheetFragment =
            SearchBottomSheetFragmentScrool()
        bottomSheetFragment.dismiss()
//        getFragmentManager()?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

}