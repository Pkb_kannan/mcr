package com.mcr.active.ui.fragment.findActivity

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.R
import com.mcr.active.databinding.DayItemFinactivityBinding
import com.orhanobut.hawk.Hawk
import kotlin.collections.ArrayList


class DayAdapter(private val viewModel: FindViewmodel,
                 private var items: ArrayList<String>)
    : RecyclerView.Adapter<DayAdapter.ViewHolder>() {

    var selectedItem : ArrayList<String>?= ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DayItemFinactivityBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(
            binding
        )
    }

//    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position,items,selectedItem)
    }

    override fun getItemCount(): Int = items.size




    fun setDayInAdapter(noti: ArrayList<String>) {
        this.items = noti
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: DayItemFinactivityBinding) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("ResourceAsColor")
        fun bind(
            viewModel: FindViewmodel?,
            position: Int?,
            items: ArrayList<String>,
            selectedItem: ArrayList<String>?
        ) { //            viewModel.fetchDogBreedImagesAt(position);

//            binding.texDay.setOnClickListener(View.OnClickListener {
//                if (selectedItem?.contains(position.toString())!!){
//                    binding.texDay.setBackgroundResource(R.color.colorWhtie)
//                    binding.texDay.setTextColor(R.color.colorBlack)
//                    selectedItem.remove(position.toString()!!)
//                }
//                else {
//                    binding.texDay.setBackgroundResource(R.color.colorRed)
//                    binding.texDay.setTextColor(R.color.colorWhtie)
//                    selectedItem.add(position.toString()!!)
//                }
//            })
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

