package com.mcr.active.model.EventDetailsModel


import com.google.gson.annotations.SerializedName


data class IminItem2Item(

	@field:SerializedName("imin:resultCount")
	val iminResultCount: Int? = null,

	@field:SerializedName("prefLabel")
	val prefLabel: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)