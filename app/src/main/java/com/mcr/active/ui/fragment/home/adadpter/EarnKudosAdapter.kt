package com.mcr.active.ui.fragment.home.adadpter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.databinding.EarnKudosItemBinding
import com.mcr.active.model.earnKudosList.KudosDataItem
import com.mcr.active.ui.fragment.home.HomeViewmodel


class EarnKudosAdapter(private val viewModel: HomeViewmodel,
                       private var items: List<KudosDataItem>)
    : RecyclerView.Adapter<EarnKudosAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = EarnKudosItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

//    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position)

    }

    override fun getItemCount(): Int = items.size




    fun setKudos(kudos: List<KudosDataItem>) {
        this.items = kudos
        notifyDataSetChanged()
    }
    fun refresh(kudos: List<KudosDataItem>) {
        this.items = kudos
        notifyDataSetChanged()
    }


    class ViewHolder(private var binding: EarnKudosItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: HomeViewmodel?, position: Int?
        )
        { //            viewModel.fetchDogBreedImagesAt(position);

//            binding.txtItem.setOnClickListener {
//                binding.notifyChange()
//            }


            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

