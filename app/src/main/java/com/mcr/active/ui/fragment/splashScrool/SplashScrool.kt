package com.mcr.active.ui.fragment.splashScrool

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.databinding.SplasScroolBinding
import com.mcr.active.model.viewPagerModel.ViewPagerModel
import com.mcr.active.ui.MainActivity
import com.mcr.active.ui.fragment.home.bottomSheetFragment.favourit.FavouritBottomSheetFragmentScrool
import com.mcr.active.ui.fragment.home.bottomSheetFragment.location.LocationBottomSheetFragmentSplash
import com.mcr.active.ui.fragment.home.bottomSheetFragment.search.SearchBottomSheetFragmentScrool
import com.mcr.active.ui.fragment.splashScrool.SplashScroolDirections.*
import com.orhanobut.hawk.Hawk


class SplashScrool : Fragment() {

    var binding: SplasScroolBinding? = null
    var viewmodel: SplashScroolViewModel? = null
    private var pagerAdapter: ViewPagerAdapter? = null
    private val myImageList = intArrayOf(R.drawable.scrool, R.drawable.scrool1, R.drawable.scrool2)
//    private val text = charArrayOf("dfg", R.drawable.scrool1, R.drawable.scrool1-2)

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        Util.hawk(this.context!!)
        viewmodel = ViewModelProviders.of(activity!!).get(SplashScroolViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding  = DataBindingUtil.inflate(inflater,R.layout.splas_scrool,container,false)
        binding?.viewModel = viewmodel
//        if (Hawk.get("LoginStatus",false)) {
//            Navigation.findNavController(binding?.root!!).navigate(actionNavSplashToNavHome())
//        }
        initObservables()
        return binding?.root
    }

    private fun initObservables() {
//        MainActivity()?.topHide(false)
        Util.hawk(this.context!!)
        Hawk.put("page","splash")

        viewmodel?.launchSinInActivity?.observe(this, Observer {
            if (it!!) {
//                Navigation.findNavController(it).navigate(HomeDirections.actionNavHomeToNavReward())
                Navigation.findNavController(requireView()).navigate(R.id.nav_login)
//                startActivity(Intent(activity, Login::class.java))
            }
        })
        viewmodel?.launchHome?.observe(this, Observer {
            if (it!!) {
                Navigation.findNavController(requireView()).navigate(actionNavSplashToNavHome())
            }
        })
        viewmodel?.launchRegisterActivity?.observe(this, Observer {
            (activity as MainActivity).sidemenuHide(false)
            Navigation.findNavController(requireView()).navigate(R.id.register)
//            if (it!!) startActivity(Intent(activity, RegisterActivity::class.java))
        })

//        binding?.locationInclud?.btStar?.setOnClickListener(View.OnClickListener {
//            showFavouritDialogFragment()
//        })
//        binding?.locationInclud?.btSearch?.setOnClickListener(View.OnClickListener {
//            showSearchDialogFragment()
//        })
//        binding?.locationInclud?.btLocation?.setOnClickListener(View.OnClickListener {
//            showLocationDialogFragment()
//        })


        binding?.btStar?.setOnClickListener(View.OnClickListener {
            Hawk.put("location",false)
            showFavouritDialogFragment()
        })
        binding?.btSearch?.setOnClickListener(View.OnClickListener {
            Hawk.put("location",false)
            showSearchDialogFragment()

        })
        binding?.btLocation?.setOnClickListener(View.OnClickListener {

            showLocationDialogFragment()
        })
        setupViews()
    }

    fun  showFavouritDialogFragment() {
        val bottomSheetFragment =
            FavouritBottomSheetFragmentScrool()
        fragmentManager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) } }

    fun  showLocationDialogFragment() {
        val bottomSheetFragment =
            LocationBottomSheetFragmentSplash()
        fragmentManager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }
    fun  showSearchDialogFragment() {
        val bottomSheetFragment =
            SearchBottomSheetFragmentScrool()
        fragmentManager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    private fun populateList(): ArrayList<ViewPagerModel> {

        val list = ArrayList<ViewPagerModel>()

        for (i in 0..2) {
            val imageModel = ViewPagerModel()
            imageModel.setimg(myImageList[i])
            if (i == 0)
            imageModel.setConten("")
            else if (i==1)
                imageModel.setConten("SEARCH FOR PARKS AND LEISURE FACILITIES IN YOUR LOCAL AREA")
            else
                imageModel.setConten("EARN REWARDS TAKE PART IN CHALLENGES")
            list.add(imageModel)
        }

        return list
    }

    private fun setupViews() {
       var imageModelArrayList : ArrayList<ViewPagerModel>? = null
        imageModelArrayList = populateList()
        pagerAdapter = ViewPagerAdapter(this.context!!,imageModelArrayList)
        binding?.pagerBanner?.adapter = pagerAdapter
        binding?.indicator?.setViewPager( binding?.pagerBanner)

        val density = resources.displayMetrics.density

        //Set circle indicator radius
        binding?.indicator?.setRadius(5 * density)


    }
}