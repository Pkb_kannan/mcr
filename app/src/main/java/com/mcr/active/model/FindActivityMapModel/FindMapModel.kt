package com.mcr.active.model.FindActivityMapModel


import com.google.gson.annotations.SerializedName


data class FindMapModel(

	@field:SerializedName("imin:totalItems")
	val iminTotalItems: Int? = null,

	@field:SerializedName("imin:item")
	val iminItem: List<IminItemItem>? = null
)