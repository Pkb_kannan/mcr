package com.mcr.active.ui.fragment.home.adadpter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.databinding.NotificationItemBinding
import com.mcr.active.model.noliList
import com.mcr.active.ui.fragment.home.HomeViewmodel


class NotificationAdapter(private val viewModel: HomeViewmodel,
                          private var items: List<noliList>)
    : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = NotificationItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

//    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemCount(): Int = items.size




    fun setNotification(noti: List<noliList>) {
        this.items = noti
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: NotificationItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: HomeViewmodel?, position: Int?
        ) { //            viewModel.fetchDogBreedImagesAt(position);
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

