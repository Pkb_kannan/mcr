package com.mcr.active.model.newsAndEvends.details


import com.google.gson.annotations.SerializedName


data class MoreItem(

	@field:SerializedName("more_newsevents_id")
	val moreNewseventsId: String? = null,

	@field:SerializedName("paragraph")
	val paragraph: String? = null,

	@field:SerializedName("quote")
	val quote: String? = null,

	@field:SerializedName("newsevents_id")
	val newseventsId: String? = null
)