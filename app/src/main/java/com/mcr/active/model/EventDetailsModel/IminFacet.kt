package com.mcr.active.model.EventDetailsModel


import com.google.gson.annotations.SerializedName


data class IminFacet(

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("imin:index")
	val iminIndex: IminIndex? = null
)