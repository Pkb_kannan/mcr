package com.mcr.active.model.EventDetailsModel


import com.google.gson.annotations.SerializedName


data class ActivityConceptCollection(

	@field:SerializedName("imin:totalItems")
	val iminTotalItems: Int? = null,

	@field:SerializedName("imin:source")
	val iminSource: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("imin:item")
	val iminItem3: List<IminItem3Item?>? = null
)