package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.search

import android.app.Application
import android.graphics.Color
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.Util.Util.Companion.getDayname1
import com.mcr.active.model.favouritModel.FavouritDataItem
import com.mcr.active.model.favouritModel.FavouritListModel
import com.mcr.active.model.favouritModel.favouritrIminItemModel.FavActivityModel
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.network.Api
import com.mcr.active.ui.fragment.home.bottomSheetFragment.favourit.FavouritBottomSheetFragmentScrool
import com.mcr.active.ui.fragment.home.bottomSheetFragment.location.LocationBottomSheetFragmentSplash
import com.mcr.active.ui.fragment.home.bottomSheetFragment.search.SearchBottomSheetFragmentScrool
import com.orhanobut.hawk.Hawk
import com.mcr.active.base.BaseViewModel
import com.mcr.active.ui.fragment.findActivity.DayAdapter
import retrofit2.Call
import retrofit2.Response
import java.util.ArrayList
import javax.inject.Inject

class SearchViewModel(application: Application) : BaseViewModel(application) {
    @Inject
    lateinit var api: Api
    var timeFromTo : ObservableField<String>? = null
    var visibility= ObservableBoolean()
    var bt_from= ObservableBoolean()
    var launchFindMapFragment: SingleLiveEvent<Boolean>? = null
    var launchFindResult: SingleLiveEvent<Boolean>? = null
    private var adapter: DayAdapter3? = null
    var days  : ArrayList<String>? =null
    var strDistance: ObservableField<String>? = null
    var strDistanceposition: ObservableField<String>? = null
    var strActivityType: ObservableField<String>? = null
    var tvText: ObservableField<String>? = null
    var sportName: ObservableField<String>? = null
    var selectedItem : ArrayList<String>?= ArrayList()
    var refreshDayAdapter: SingleLiveEvent<Boolean>? = null
    val color = Color.WHITE
    var list :ArrayList<String>  ?= ArrayList()
    init{

        strDistance = ObservableField("")
        strDistanceposition = ObservableField("")
        strActivityType = ObservableField("")
        timeFromTo = ObservableField("TIME")
        tvText= ObservableField("")
        sportName= ObservableField("")
        refreshDayAdapter = SingleLiveEvent<Boolean>()
        /*Time Range*/
        visibility.set(false)
        bt_from.set(false)

        launchFindMapFragment = SingleLiveEvent<Boolean>()
        launchFindResult=SingleLiveEvent<Boolean>()
    }

    fun  showLocationDialogFragment(fragmager : FragmentManager) {
        val bottomSheetFragment =
            LocationBottomSheetFragmentSplash()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }
    fun  showSearchDialogFragment(fragmager : FragmentManager) {
        val bottomSheetFragment =
            SearchBottomSheetFragmentScrool()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }
    fun  showFavouritDialogFragment(fragmager : FragmentManager) {
        val bottomSheetFragment =
            FavouritBottomSheetFragmentScrool()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    fun onSelectItem(
        parent: AdapterView<*>?,
        view: View?,
        pos: Int,
        id: Long
    ) { //pos                                 get selected item position
//view.getText()                      get lable of selected item
//parent.getAdapter().getItem(pos)    get item by pos
//parent.getAdapter().getCount()      get item count
//parent.getCount()                   get item count
//parent.getSelectedItem()            get selected item
//and other...
    }

    fun getValues(){



        var hashMap : HashMap<String, Any>
                = HashMap<String, Any> ()


        var filterList = ArrayList<HashMap<String, Any>>()


        hashMap.put("timeRange" , timeFromTo?.get().toString())
        hashMap.put("sportName" , sportName?.get().toString())
        hashMap.put("zip" , tvText?.get().toString())
        hashMap.put("distance" , strDistance?.get().toString())
        hashMap.put("actvityType" , strActivityType?.get().toString())
        hashMap.put("days" , this!!.selectedItem!!)
        filterList.add(hashMap)
        Log.e("select_value_time",hashMap["days"].toString())
        for(key in hashMap.keys){
//            println("Element at key $key : ${hashMap[key]}")
            Log.e("select_value",hashMap[key].toString())
        }

        Hawk.put("findactvities", filterList)
        if (strDistance?.get().toString().equals("0") || strDistance?.get().toString().equals(""))
            Toast.makeText(getApplication(),"Select Distance", Toast.LENGTH_SHORT).show()
        else if (tvText?.get().toString().equals(""))
            Toast.makeText(getApplication(),"Select Place", Toast.LENGTH_SHORT).show()
        else {
            selectedItem?.clear()
            launchFindResult?.value = true
        }


    }
    fun distanceSelectItem(
        parent: AdapterView<*>?,
        view: View?,
        pos: Int,
        id: Long
    ) { //pos                                 get selected item position

        strDistance?.set(parent?.getSelectedItem().toString())
        strDistanceposition?.set(pos.toString())
    }

    fun activitySelectItem(
        parent: AdapterView<*>?,
        view: View?,
        pos: Int,
        id: Long
    ) { //pos                                 get selected item position

        strActivityType?.set(parent?.getSelectedItem().toString())
    }


    fun timeRange(){
        if(!visibility.get())visibility.set(true)
        else visibility.set(false)
    }

    fun setDayInAdapter(noti: ArrayList<String>) {
        adapter?.setDayInAdapter(noti)
        adapter?.notifyDataSetChanged()
        this.days = noti
    }
    fun daybind(position : Int): String? {
        return days?.get(position)
    }
    fun  getResultColor(Index :Int): Int {
        var color = Color.WHITE
        if (selectedItem?.contains(list?.get(Index))!!){
            color = Color.RED
        }
        else {
            color = Color.WHITE
        }
        return color
    }
    fun weekSelect(Index :Int){
        if (selectedItem?.contains(list?.get(Index))!!){
            selectedItem!!.remove(list?.get(Index)!!)
        }
        else {
            selectedItem!!.add(list?.get(Index)!!)
        }
        refreshDayAdapter?.value = true
    }
}