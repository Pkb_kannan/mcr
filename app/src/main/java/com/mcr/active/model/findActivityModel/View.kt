package com.mcr.active.model.findActivityModel


import com.google.gson.annotations.SerializedName


data class View(

	@field:SerializedName("imin:itemsPerPage")
	val iminItemsPerPage: Int? = null,

	@field:SerializedName("imin:totalPages")
	val iminTotalPages: Int? = null,

	@field:SerializedName("imin:currentPage")
	val iminCurrentPage: Int? = null
)