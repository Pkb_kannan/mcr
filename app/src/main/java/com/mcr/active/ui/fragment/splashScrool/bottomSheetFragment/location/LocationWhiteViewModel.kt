package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location

import android.annotation.SuppressLint
import android.app.Application
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mcr.active.MCRApp
import com.mcr.active.R
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.Util.Util
import com.mcr.active.Util.Util.Companion.getCurrentWeeknae
import com.mcr.active.Util.Util.Companion.getcheck_out_time
import com.mcr.active.model.EventDetailsModel.EventDetailsModel
import com.mcr.active.model.EventDetailsModel.IminItemItemDetails
import com.mcr.active.model.FindActivityMapModel.FindMapModel
import com.mcr.active.model.FindActivityMapModel.IminItemItem
import com.mcr.active.model.facilityModel.FacilityIminItemItem
import com.mcr.active.model.facilityModel.FacilityModel
import com.mcr.active.model.favouritModel.favLeisureModel.AmenityFeatureItem
import com.mcr.active.model.favouritModel.favLeisureModel.FavLeisureListModel
import com.mcr.active.model.favouritModel.removeModel.RemoveFavModel
import com.mcr.active.model.findActivityModel.SubEventItem
import com.mcr.active.model.loginModel.LoginModel
import com.mcr.active.network.Api
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.timeTable.DayAdapter
import com.mcr.active.ui.fragment.home.bottomSheetFragment.favourit.FavouritBottomSheetFragmentScrool
import com.mcr.active.ui.fragment.home.bottomSheetFragment.location.LocationBottomSheetFragmentSplash
import com.mcr.active.ui.fragment.home.bottomSheetFragment.search.SearchBottomSheetFragmentScrool
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.timeTable.TimeBottomSheetFragment
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.timeTable.TimeTableAdapter
import com.orhanobut.hawk.Hawk
import com.mcr.active.base.BaseViewModel
import com.mcr.active.model.EventDetailsModel.SubEvent1Item
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.accessibility.AccessAdapter
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.accessibility.AccessBottomSheetFragment
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.info.InfoBottomSheetSplashFragment
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.timeTable.WebView
import retrofit2.Call
import retrofit2.Response
import java.util.*
import javax.inject.Inject


class LocationWhiteViewModel(application: Application) : BaseViewModel(application) {
    @Inject
    lateinit var api: Api
    private var adapter: PlaceAdapter? = null
    private var dayAdapter: DayAdapter? = null
    var fragmentManager: FragmentManager? = null
    var webLoad: SingleLiveEvent<Boolean>? = null
    private var accessAdapter: AccessAdapter? = null
    var permission: SingleLiveEvent<Boolean>? = null
    var callButton: SingleLiveEvent<Boolean>? = null
    var workingDays: SingleLiveEvent<Boolean>? = null
    private var adapterBlue: PlaceBlueAdapter? = null
    var timeTableLoad: SingleLiveEvent<Boolean>? = null
    var locationClose: SingleLiveEvent<Boolean>? = null
    private var faciltiAdapter: PlaceInfoAdapter? = null
    var telephone_number: SingleLiveEvent<String>? = null
    private var adapterTimeTable: TimeTableAdapter? = null
    var callVisibility: ObservableInt = ObservableInt(View.GONE)
    var positin: Int? = null
    var checckOutPosition = 0
    var location = ""
    val FacList = ArrayList<FacilityIminItemItem>()
    //    var detailsPositin :Int ? =null
    var listFacilit: List<String>? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    var days: ArrayList<String>? = null
    var accessItems: ArrayList<String>? = null
    var visible: ObservableInt = ObservableInt(View.GONE)
    var timeFromTo: ObservableField<String>? = null
    var lat: MutableLiveData<String> = MutableLiveData<String>()
    var log: MutableLiveData<String> = MutableLiveData<String>()

    var list: ArrayList<String>? = ArrayList()
    var WebUrl: ObservableField<String>? = null
    var TodayOpen: ObservableField<String>? = null
    var selectedItem: ArrayList<String>? = ArrayList()
    var NearLocationnull: SingleLiveEvent<Boolean>? = null
    var TimeTableDetailsOpen: SingleLiveEvent<Boolean>? = null
    var refreshDayAdapter: SingleLiveEvent<Boolean>? = null
    var TimeTableBookingOpen: SingleLiveEvent<Boolean>? = null
    var isProgressLoading: ObservableInt = ObservableInt(View.GONE)
    var FacilityFechersList: MutableLiveData<List<FavLeisureListModel?>> =
        MutableLiveData<List<FavLeisureListModel?>>()
    var FacilityFeature: MutableLiveData<List<AmenityFeatureItem?>> =
        MutableLiveData<List<AmenityFeatureItem?>>()
    var LocationiList: List<IminItemItem> = ArrayList<IminItemItem>()
    private var EventDetailsList: List<IminItemItemDetails> = ArrayList<IminItemItemDetails>()
    var LocationiListLive: List<FacilityIminItemItem> = ArrayList<FacilityIminItemItem>()
    private var LocationiListLiveActivity: List<IminItemItem> = ArrayList<IminItemItem>()
    private val LocationisubLive: MutableLiveData<List<SubEventItem>> =
        MutableLiveData<List<SubEventItem>>()
    private var FacilityList: List<FacilityIminItemItem> = ArrayList<FacilityIminItemItem>()
    var FacilityList1: MutableLiveData<List<FacilityIminItemItem>> =
        MutableLiveData<List<FacilityIminItemItem>>()

    init {
        locationClose = SingleLiveEvent<Boolean>()
        webLoad = SingleLiveEvent<Boolean>()
        telephone_number = SingleLiveEvent<String>()
        permission = SingleLiveEvent<Boolean>()
        timeTableLoad = SingleLiveEvent<Boolean>()
        workingDays = SingleLiveEvent<Boolean>()
        timeFromTo = ObservableField("TIME")
        WebUrl = ObservableField("")
        TodayOpen = ObservableField("")
        TimeTableDetailsOpen = SingleLiveEvent<Boolean>()
        TimeTableBookingOpen = SingleLiveEvent<Boolean>()
        NearLocationnull = SingleLiveEvent<Boolean>()
        refreshDayAdapter = SingleLiveEvent<Boolean>()
        callButton = SingleLiveEvent<Boolean>()
    }

    // *******************************Location List*****************************************

    fun NearLocation() {

        NearLocationnull?.value = false
        isProgress.set(View.VISIBLE)

        val call = api.getPlaceFacility(
            "51.588382973396,-0.074243442870947,5",
            "10",
            "1",
            Util.getTime(),
            Util.getIminToken()
        )
//        val call = api.getPlaceFacility(lat.value.toString()+","+log.value.toString()+",5","10","1",Util.getTime(),Util.getIminToken())
        call.enqueue(object : retrofit2.Callback<FindMapModel> {
            override fun onFailure(call: Call<FindMapModel>?, t: Throwable?) {
                Log.e("response", t?.message)
                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<FindMapModel>?,
                response: Response<FindMapModel>?
            ) {
                if (response?.isSuccessful!!) {
                    val gson = Gson()
                    val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                    Log.e("pkb_facility", jsonElement.toString())
                    LocationiListLiveActivity = response?.body()?.iminItem!!
                    Hawk.put("nearByLocation", LocationiListLiveActivity)
                    if (LocationiListLiveActivity.size == 0) {
                        FacilityList = FacilityList1.value!!
                        Hawk.put("nearByFacility", FacilityList)

                        isProgress.set(View.GONE)
                        NearLocationnull?.value = true
                    } else {
                        FacilityFilter(0)
                    }
                } else {
                    Hawk.put("nearByFacility", FacilityList)

                    isProgress.set(View.GONE)
                    NearLocationnull?.value = true
                }
            }

        })
    }

    fun FacilityFilter(position: Int) {

        val count = position
        if (count + 1 != LocationiListLiveActivity.size) {
            location =
                LocationiListLiveActivity.get(position).geo?.latitude.toString() + "," + LocationiListLiveActivity.get(
                    position
                ).geo?.longitude.toString() + ",10"
            val call = api.getFacility(
                location!!,//"51.588302,-0.074138,5",//location!!,
                "1",
                "20",
                Util.getDateTime(),
                "upcoming-slots",
                "dwlR7D50eTzWYGQnmzUZ4hD7Gu2KtxIj"
            )
            call.enqueue(object : retrofit2.Callback<FacilityModel> {
                override fun onFailure(call: Call<FacilityModel>?, t: Throwable?) {
                    Log.e("response", t?.message)
                    isProgress.set(View.GONE)
                    Hawk.put("nearByFacility", FacilityList)
                    isProgress.set(View.GONE)
                    NearLocationnull?.value = true
                }

                override fun onResponse(
                    call: Call<FacilityModel>?,
                    response: Response<FacilityModel>?
                ) {
                    val gson = Gson()
                    val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                    Log.e("list_facility1", jsonElement.toString())
                    if (response?.isSuccessful!!) {
                        if (response?.body()?.iminTotalItems != 0) {
                            FacList.addAll(response?.body()?.iminItem!!)
                            if (FacList.size < 3)
                                FacilityFilter(count + 1)
                            else {
                                FacilityList1.value = FacList
                                FacilityList = FacList
                                LocationiListLive = FacList
                                Hawk.put("nearByFacility", FacilityList)
                                isProgress.set(View.GONE)
                                NearLocationnull?.value = true
                            }
                        } else {
                            FacilityFilter(count + 1)
                        }
                    }

                }
            })
        } else {
            Hawk.put("nearByFacility", FacilityList)
//            isProgress.set(View.GONE)
            NearLocationnull?.value = true
        }

    }


    fun getPlace(index: Int?): FacilityIminItemItem? {
        Log.e("details", LocationiListLive.size.toString() + "\n" + index.toString())
        return if (LocationiListLive.get(index!!) != null && index != null && LocationiListLive.size > index
        ) {
            LocationiListLive.get(index)
        } else null
    }

    fun getDistance(index: Int?): String? {
        return if (LocationiListLive.get(index!!) != null && index != null && LocationiListLive.size > index
        ) {
            "Your are " + LocationiListLive.get(index)?.location?.geo?.iminDistanceFromGeoQueryCenter?.value + " Miles From"
        } else null
    }

    fun getTime(index: Int?): String? {
        return if (LocationiListLive.get(index!!) != null && index != null && LocationiListLive?.get(
                index
            )?.event?.size != 0
        ) {
            "Open Today   " + Util.todyOpen(
                LocationiListLive?.get(index)?.event?.get(0)?.startDate!!,
                LocationiListLive?.get(index)?.event?.get(0)?.endDate!!
            )
        } else null
    }

    fun call(index: Int) {
        FacilityList = Hawk.get("nearByFacility", FacilityList)
        if (!Util.setupPermissions(getApplication())) {
            permission?.value = true
        }
        if (FacilityList.get(index!!) != null && index != null && FacilityList.get(index)?.location?.telephone != null) {
            Log.e("Telephone", FacilityList.get(index)?.location?.telephone.toString())
            telephone_number?.value = FacilityList.get(index)?.location?.telephone
        }
    }

    fun callButtons(index: Int): SingleLiveEvent<Boolean>? {
        FacilityList = Hawk.get("nearByFacility", FacilityList)
        println(FacilityList.get(index)?.location?.telephone.toString())
        if (FacilityList.get(index)?.location?.telephone == null) {
            callButton?.value = false
        } else {
            callButton?.value = true
        }
        return callButton
    }

    fun setLocationInAdapter(
        noti: List<FacilityIminItemItem>?,
        fragmentManager: FragmentManager?
    ) {
        this.fragmentManager = fragmentManager
        adapter?.setLocation(noti)
        adapter?.notifyDataSetChanged()
    }

    fun setLocationBlueAdapter(
        noti: List<FacilityIminItemItem>?,
        fragmentManager: FragmentManager?
    ) {
        this.fragmentManager = fragmentManager
        adapterBlue?.setLocation(noti)
        adapterBlue?.notifyDataSetChanged()
    }


    fun showLocationDialogFragment(fragmager: FragmentManager) {
        val bottomSheetFragment =
            LocationBottomSheetFragmentSplash()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    fun showSearchDialogFragment(fragmager: FragmentManager) {
        val bottomSheetFragment =
            SearchBottomSheetFragmentScrool()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    fun showFavouritDialogFragment(fragmager: FragmentManager) {
        val bottomSheetFragment =
            FavouritBottomSheetFragmentScrool()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    fun showLocatioInfoDialogFragment1(index: Int) {
        positin = index
        locationClose?.value = true
        val bottomSheetFragment =
            InfoBottomSheetSplashFragment()
        fragmentManager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }

    }


    //********************************Accessibility****************************************


    fun showLocationAccessDialogFragment() {
        val bottomSheetFragment =
            AccessBottomSheetFragment()
        fragmentManager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }


    fun setAccessAdapter(noti: ArrayList<String>) {
        accessAdapter?.setAccessAdapter(noti)
        accessAdapter?.notifyDataSetChanged()
        this.accessItems = noti
    }

    fun accessBind(position: Int): String? {

        return accessItems?.get(position)

    }


    //********************************Time Table*******************************************
    fun showLocatioTimeDialogFragment1(index: Int) {
        positin = index
        locationClose?.value = true
        val TimeSheetFragment =
            TimeBottomSheetFragment()
        fragmentManager?.let { TimeSheetFragment.show(it, TimeSheetFragment.tag) }
        TimeTable()

    }

    fun showLocatioTimeDialogFragment1() {
//        positin = index
        locationClose?.value = true
        val TimeSheetFragment =
            TimeBottomSheetFragment()
        fragmentManager?.let { TimeSheetFragment.show(it, TimeSheetFragment.tag) }
        TimeTable()

    }

    fun TimeTable() {
        LocationiList = Hawk.get("nearByLocation", LocationiList)
        FacilityList = Hawk.get("nearByFacility", FacilityList)
        var call: Call<EventDetailsModel>
        val location =
            "" + FacilityList.get(positin!!)?.location?.geo?.latitude + "," + FacilityList.get(
                positin!!
            )?.location?.geo?.longitude + ",0"
        timeTableLoad?.value = false
        isProgress.set(View.VISIBLE)
        val listTime = timeFromTo?.get().toString().split("-")
//        val call = api.getTimeTable(Util.getIminUrl()+"geo[radial]="+location,"discovery-geo", Util.getTime(),"dwlR7D50eTzWYGQnmzUZ4hD7Gu2KtxIj")
        if (selectedItem?.size == 0 && timeFromTo?.get().toString().equals("TIME"))
            call = api.getTimeTable(
                Util.getIminEventUrl() + "geo[radial]=" + location + "&mode=discovery-geo&startTime[gte]=" + Util.getTime(),
                Util.getIminToken()
            )
        else if (selectedItem?.size != 0 && timeFromTo?.get().toString().equals("TIME"))
            call = api.getTimeTableFilterWeek(
                Util.getIminEventUrl() + "geo[radial]=" + location + "&mode=discovery-geo&startTime[gte]=" + Util.getTime(),
                selectedItem!!,
                Util.getIminToken()
            )
        else if (selectedItem?.size != 0 && !timeFromTo?.get().toString().equals("TIME"))
            call = api.getTimeTableFilterWeek(
                Util.getIminEventUrl() + "geo[radial]=" + location + "&mode=discovery-geo&startTime[gte]=" + listTime[0].replace(
                    " ",
                    ""
                ) + "&startTime[lte]=" + listTime[1].replace(" ", ""),
                selectedItem!!,
                Util.getIminToken()
            )

//        else if (selectedItem?.size==0 && !timeFromTo?.get().toString().equals("TIME"))
        else
            call = api.getTimeTable(
                Util.getIminEventUrl() + "geo[radial]=" + location + "&mode=discovery-geo&startTime[gte]=" + listTime[0].replace(
                    " ",
                    ""
                ) + "&startTime[lte]=" + listTime[1].replace(" ", ""), Util.getIminToken()
            )
        call.enqueue(object : retrofit2.Callback<EventDetailsModel> {
            override fun onFailure(call: Call<EventDetailsModel>?, t: Throwable?) {
                Log.e("response", t?.message)
                timeTableLoad?.value = true
                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<EventDetailsModel>?,
                response: Response<EventDetailsModel>?
            ) {
                if (response?.isSuccessful!!) {
                    if (response?.body()?.iminTotalItems != 0) {
                        val gson = Gson()
                        val jsonElement: JsonElement = gson.toJsonTree(response?.body())
//                        Log.e("event_details", jsonElement.toString())
                        EventDetailsList = response?.body()?.iminItem!!

                    } else {
                        EventDetailsList = emptyList()
//                        EventDetailsList?.c
                    }

                } else {
                    EventDetailsList = emptyList()
                }
                timeTableLoad?.value = true
                isProgress.set(View.GONE)


            }

        })
    }

    fun checkOutUrlcheck(position: Int): Int {
        if (!EventDetailsList.isEmpty() && EventDetailsList.size != 0 && !EventDetailsList.get(
                position
            ).subEvent?.isEmpty()!!
        ) {

            return View.VISIBLE
        } else {
            return View.GONE
        }
    }

    fun checkOutUrlcheckList(position: Int): Int {
        if (EventDetailsList.isNotEmpty() && EventDetailsList[positin!!].subEvent!![0]?.subEvent1?.size != 0 && !EventDetailsList.get(
                positin!!
            ).subEvent!!.get(0)?.subEvent1?.get(position)?.iminCheckoutUrlTemplate.toString().equals(
                "null"
            )
        ) {

            return View.VISIBLE
        } else {
            return View.GONE
        }
    }

    fun checkOutday(position: Int): String {
        return getcheck_out_time(
            EventDetailsList.get(positin!!).subEvent!!.get(0)?.subEvent1?.get(position)?.startDate!!,
            EventDetailsList.get(positin!!).subEvent!!.get(0)?.subEvent1?.get(position)?.endDate!!
        )
    }

    fun checkOutDuration(position: Int): String? {
        return EventDetailsList.get(positin!!).subEvent!!.get(0)?.subEvent1?.get(position)
            ?.duration?.replace("PT", "").toString()

    }

    fun checkOutcapacity(position: Int, type: Int): String {
        if (type == 1)
            return EventDetailsList.get(positin!!).subEvent!!.get(0)?.subEvent1?.get(position)
                ?.maximumAttendeeCapacity.toString()
        else
            return EventDetailsList.get(positin!!).subEvent!!.get(0)?.subEvent1?.get(position)
                ?.remainingAttendeeCapacity.toString()

    }

    //    fun webViewLoad(): String {
//
//        return
//    }
    fun webVieOpen(position: Int) {

        checckOutPosition = position

        val WebView =
            WebView()
        fragmentManager?.let { WebView.show(it, WebView.tag) }
        TimeTable()
        WebUrl?.set(
            EventDetailsList.get(positin!!).subEvent!!.get(0)?.subEvent1?.get(
                checckOutPosition
            )?.iminCheckoutUrlTemplate.toString().replace(
                "{checkoutBaseUrl}",
                "https://checkout.kineticinsight.org/api/checkout-sessions"
            )
        )
        webLoad?.value = true

    }


    fun setTimeTableAdapter() {
        adapterTimeTable?.setTimeTable(EventDetailsList)
        adapterTimeTable?.notifyDataSetChanged()
    }

    fun gettimeTable(): List<IminItemItemDetails> {
        return EventDetailsList
    }

    fun getItemName(index: Int): IminItemItemDetails? {
        Log.e("details", EventDetailsList.size.toString() + "\n" + index.toString())
        return if (EventDetailsList.get(index!!) != null && index != null && EventDetailsList.size > index
        ) {
            EventDetailsList.get(index)
        } else null
    }

    fun openDetailsDialog(): IminItemItemDetails {
        return EventDetailsList.get(positin!!)
    }

    fun openBookingDialog(): List<SubEvent1Item?>? {
        return EventDetailsList.get(positin!!).subEvent?.get(0)?.subEvent1
    }

    fun getdetails(index: Int) {
        positin = index
        if (EventDetailsList.get(index!!) != null && index != null && EventDetailsList.size > index
        ) {
            TimeTableDetailsOpen?.value = true
        } else null
    }

    fun getBooking(index: Int) {
        positin = index
        TimeTableBookingOpen?.value = true
    }

    fun getdayName(index: Int): String? {
        return if (EventDetailsList.get(index!!) != null && index != null && EventDetailsList.size > index
        ) {
            var date: String
            date = Util.getDayname(
                EventDetailsList.get(index)?.subEvent?.get(0)?.subEvent1?.get(0)?.startDate?.split("T")!!
            )
//            if (EventDetailsList.get(index)?.subEvent?.get(0)?.subEvent1?.size!! > 1){
            date += " - " + Util.getEndTime(
                EventDetailsList.get(index)?.subEvent?.get(0)?.subEvent1?.get(
                    0
                )?.endDate?.split("T")!!
            )
//            }
            return date
        } else null
    }

    fun getdayNameNextSession(index: Int): String? {
        return if (EventDetailsList.get(index!!) != null && index != null && EventDetailsList.get(
                index
            )?.subEvent?.get(0)?.subEvent1?.size!! > 1
        ) {

            return Util.getDayname(
                EventDetailsList.get(index)?.subEvent?.get(0)?.subEvent1?.get(1)?.startDate?.split(
                    "T"
                )!!
            )
        } else null
    }

    fun getendTime(index: Int): String? {
        return if (EventDetailsList.get(index!!) != null && index != null && EventDetailsList.size > index
        ) {

            return Util.getDayname(
                EventDetailsList.get(index)?.subEvent?.get(0)?.subEvent1?.get(0)?.startDate?.split(
                    "T"
                )!!
            )
        } else null
    }

    fun getCategory(index: Int): String? {
        return if (EventDetailsList.get(index!!) != null && index != null && EventDetailsList.size > index
        ) {

            return EventDetailsList.get(index)?.subEvent?.get(0)?.category?.get(0)
        } else null
    }

    fun setDayInAdapter(noti: ArrayList<String>) {
        dayAdapter?.setDayInAdapter(noti)
        dayAdapter?.notifyDataSetChanged()
        this.days = noti
    }


    fun daybind(position: Int): String? {

        return days?.get(position)

    }

    fun timeRange() {
        if (visible.get() != 0) visible.set(View.VISIBLE)
        else visible.set(View.GONE)
//        TimeTable()
    }

    fun timeRange_submint() {
        if (visible.get() != 0) visible.set(View.VISIBLE)
        else visible.set(View.GONE)
        TimeTable()
    }

    @SuppressLint("Range")
    fun getResultColor(Index: Int): Int {
        var color = Color.WHITE
        if (selectedItem?.contains(list?.get(Index))!!) {
            color = Color.RED
        } else {
            color = Color.GRAY
        }
        return color
    }

    fun weekSelect(Index: Int) {
        if (selectedItem?.contains(list?.get(Index))!!) {
            selectedItem!!.remove(list?.get(Index)!!)

        } else {
            selectedItem!!.add(list?.get(Index)!!)
        }
        refreshDayAdapter?.value = true
        TimeTable()
    }

    //*****************************Location info**************************************************
    fun placename(): String? {
        return this!!.positin?.let { getPlace(it)?.location?.name }
    }

    fun distance(): String? {
        return if (LocationiListLive.get(positin!!) != null && positin != null && LocationiListLive.size > positin!!
        ) {
            LocationiListLive.get(positin!!)?.location?.geo?.iminDistanceFromGeoQueryCenter?.value.toString() + " Miles Away"
        } else null
//        return getDistance(positin)
    }

    fun address(): String? {
        return if (LocationiListLive.get(positin!!) != null && positin != null && LocationiListLive.size > positin!!
        ) {
            LocationiListLive.get(positin!!)?.location?.address?.iminFullAddress
        } else null
//        return getDistance(positin)
    }

    fun locationOpen(index: Int?) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse(
                "google.navigation:q=" + LocationiListLive.get(index!!)?.location?.geo?.latitude + "," + LocationiListLive.get(
                    index!!
                )?.location?.geo?.longitude
            )
        )
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        MCRApp.instance?.startActivity(intent)
    }

    fun FacilityList() {

        isProgressLoading.set(View.VISIBLE)
        workingDays?.value = false
        val call = api.getFavouriteLeisureListImin(
//            "led-FacilityUse-WU40137D",
//            Util.getIminToken()
            LocationiListLive.get(this!!.positin!!).identifier!!,
            Util.getIminToken()
        )

        call?.enqueue(object : retrofit2.Callback<FavLeisureListModel> {
            override fun onFailure(call: Call<FavLeisureListModel>?, t: Throwable?) {
                Log.e("response", t?.message)
                isProgressLoading.set(View.GONE)
                workingDays?.value = false
            }

            override fun onResponse(
                call: Call<FavLeisureListModel>?,
                response: Response<FavLeisureListModel>?
            ) {

                if (response?.isSuccessful!!) {
                    FacilityFechersList.value = listOf(response?.body())
                    FacilityFeature.value = response?.body()?.location?.amenityFeature
                    for (n in FacilityFechersList.value?.get(0)?.hoursAvailable?.indices!!) {
                        if (getCurrentWeeknae().equals(
                                FacilityFechersList.value?.get(0)?.hoursAvailable?.get(
                                    n
                                )?.dayOfWeek!!
                            )
                        ) {
                            TodayOpen?.set(
                                FacilityFechersList.value?.get(0)?.hoursAvailable?.get(n)?.opens + " - " + FacilityFechersList.value?.get(
                                    0
                                )?.hoursAvailable?.get(n)?.closes
                            )
                            selectedItem?.add(
                                FacilityFechersList.value?.get(0)?.hoursAvailable?.get(
                                    n
                                )?.dayOfWeek!!
                            )

                            break
                        }

                    }
                } else {
                    Toast.makeText(getApplication(), "Network Error!!", Toast.LENGTH_SHORT).show()
                }
                isProgressLoading.set(View.GONE)
                workingDays?.value = true
            }
        })


    }

    fun dayClick(position: Int, dayNme: String) {
        selectedItem?.clear()
        TodayOpen?.set(
            FacilityFechersList.value?.get(0)?.hoursAvailable?.get(position)?.opens + " - " + FacilityFechersList.value?.get(
                0
            )?.hoursAvailable?.get(position)?.closes
        )
        selectedItem?.add(dayNme)
    }

    fun setFacilitAdapter(noti: List<AmenityFeatureItem?>?) {
        faciltiAdapter?.setLocation(noti)
        faciltiAdapter?.notifyDataSetChanged()
    }


    fun displayFacility(position: Int): String? {

        return FacilityFeature.value?.get(position)?.name

    }

    fun imageRes(position: Int): Int {

        if (FacilityFeature.value?.get(position)?.value!!)
            return R.drawable.ic_check_white
        else
            return R.drawable.ic_close_white

    }

    fun locationNavigate() {
        locationOpen(positin)
    }

//    fun setVisiblityEnterDraw(index: Int?): Int {
//        if (LocationiList.get(index!!) != null && index != null && LocationiList.get(index)?.telephone != null
//        ) {
//            return View.VISIBLE
//        }
//        else{
//            return View.INVISIBLE
//        }
//
//    }

//    fun setVisiblityPhoneBlue(index: Int?): Int {
//        if ( FacilityList.get(index!!)!= null && FacilityList.get(index!!)?.telephone!=null){
//            return View.VISIBLE
//        }
//        else {
//            return View.GONE
//        }
//    }


    /******************Add To Favourite**********************/
    fun addFav(page: Int) {

        if (Hawk.get("LoginStatus", false)) {

            val user_dat = Hawk.get("userData", LoginModel())
            if (page == 0)
                isProgressLoading.set(View.VISIBLE)
            else
                isProgress.set(View.VISIBLE)
            val call = api.addFavouriteList(
                Util.getBaseUrl() + "addfavourite_activity",
                LocationiListLive.get(positin!!)?.identifier.toString(),
                "L",
                user_dat.memberId.toString()
            )

            call?.enqueue(object : retrofit2.Callback<RemoveFavModel> {
                override fun onFailure(call: Call<RemoveFavModel>?, t: Throwable?) {
                    Log.e("response", t?.message)
                    if (page == 0)
                        isProgressLoading.set(View.GONE)
                    else
                        isProgress.set(View.GONE)
                }

                override fun onResponse(
                    call: Call<RemoveFavModel>?,
                    response: Response<RemoveFavModel>?
                ) {

//                if (response?.isSuccessful!!){
                    if (response?.code() == 200) {

                        Toast.makeText(
                            getApplication(),
                            response.body()?.message,
                            Toast.LENGTH_SHORT
                        ).show()

                    } else {

                        Toast.makeText(getApplication(), "already added", Toast.LENGTH_SHORT).show()

                    }

                    if (page == 0)
                        isProgressLoading.set(View.GONE)
                    else
                        isProgress.set(View.GONE)

                }

            })
        }
    }


}