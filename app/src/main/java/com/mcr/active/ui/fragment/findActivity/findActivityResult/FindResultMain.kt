package com.mcr.active.ui.fragment.findActivity.findActivityResult

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.mcr.active.R
import com.mcr.active.databinding.FindActivitiesBinding
import com.mcr.active.databinding.FindResultBinding
import com.mcr.active.ui.MainActivity
import com.mcr.active.ui.fragment.findActivity.FindDirections
import com.mcr.active.ui.fragment.findActivity.FindViewmodel
//import com.mcr.active.ui.fragment.findActivity.findActivityResult.FindResultMainDirections.actionNavFindactivityResultToActivityList
import com.mcr.active.ui.fragment.findActivity.findActivityResult.findActivitiesMap.FindMap
import com.mcr.active.ui.fragment.findActivity.findActivityResult.findActivityList.FindActivityList
import com.mcr.active.ui.fragment.rewarsChallenge.subFragment.reward.Reward

class FindResultMain : Fragment() {

    var viewmodel: FindResultViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewmodel = ViewModelProviders.of(activity!!).get(FindResultViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater, R.layout.find_result,container,false)
        binding?.viewModel = viewmodel
        initObservables()
        return binding?.getRoot()
    }

    private fun initObservables(){
        val transaction = getFragmentManager()?.beginTransaction()
        transaction?.replace(R.id.find_activity_fragment, FindMap())
        transaction?.commit()

        binding?.btShowonMap?.setOnClickListener(View.OnClickListener {
            binding?.btShowonMap?.setBackgroundResource(R.color.colorRed)
            binding?.btShowasList?.setBackgroundResource(R.drawable.hash_line_box)
//            val transaction = getFragmentManager()?.beginTransaction()
//            transaction?.replace(R.id.find_activity_fragment, FindMap())
//            transaction?.commit()
        })

        binding?.btShowasList?.setOnClickListener(View.OnClickListener {
            binding?.btShowasList?.setBackgroundResource(R.color.colorRed)
            binding?.btShowonMap?.setBackgroundResource(R.drawable.hash_line_box)
//            val transaction = getFragmentManager()?.beginTransaction()
//            transaction?.replace(R.id.find_activity_fragment, FindActivityList())
//            transaction?.commit()
        })
        binding?.btMapviewClose?.setOnClickListener {
//            Navigation.findNavController(requireView()).navigate(R.id.nav_location)
            (activity as MainActivity).onBackPressed()
        }

    }


    companion object {
        var binding: FindResultBinding? = null
            private set
        @JvmStatic
        fun couuntTotal(count: Int) {

            binding?.tvM?.text = count.toString() + " RESULT"
        }
    }

}