package com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.timeTable

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.mcr.active.databinding.EventBookingItemBinding
import com.mcr.active.model.EventDetailsModel.SubEvent1Item
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.LocationWhiteViewModel
import kotlin.collections.ArrayList


class BookingAdapter(private val viewModel: LocationWhiteViewModel,
                     private var items: List<SubEvent1Item?>?
)
    : RecyclerView.Adapter<BookingAdapter.ViewHolder>() {

    var selectedItem : ArrayList<String>?= ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = EventBookingItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(
            binding
        )
    }

//    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position,viewModel)
    }

    override fun getItemCount(): Int = items!!.size




    fun setBookInAdapter(book: List<SubEvent1Item?>?) {
        this.items = book
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: EventBookingItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("ResourceAsColor")
        fun bind(
            position: Int?,
            viewModel: LocationWhiteViewModel
        ) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

