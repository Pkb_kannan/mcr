package com.mcr.active.ui.activity.webView

import android.Manifest
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mcr.active.R
import com.mcr.active.Util.Util
import com.mcr.active.databinding.SplashBinding
import com.mcr.active.databinding.WebViewBinding
import com.mcr.active.ui.MainActivity
import com.mcr.active.ui.fragment.splashScrool.bottomSheetFragment.location.LocationWhiteViewModel
import com.orhanobut.hawk.Hawk


class WebView : AppCompatActivity() {

    var binding: WebViewBinding? = null
    var viewmodel: LocationWhiteViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Util?.hawk(applicationContext)
        Hawk.put("url","imin")
        binding = DataBindingUtil.setContentView(this, R.layout.splash)
        viewmodel = ViewModelProviders.of(this).get(LocationWhiteViewModel::class.java)
        binding?.viewModel = viewmodel
        initObservables()

//        setContentView(R.layout.splash)


    }
    private fun initObservables() {


    }



}