package com.mcr.active.ui.activity.splash

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.*
import com.mcr.active.R
import com.mcr.active.Util.GpsUtils
import com.mcr.active.Util.Util
import com.mcr.active.databinding.SplashBinding
import com.mcr.active.ui.MainActivity
import com.orhanobut.hawk.Hawk


class Splash : AppCompatActivity() {

    var isGPS: Boolean = false
    var isContinue: Boolean = false
    var binding: SplashBinding? = null
    var viewmodel: SplashViewModel? = null
    var locationCallback: LocationCallback? = null
    var mFusedLocationClient: FusedLocationProviderClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Util?.hawk(applicationContext)
        Hawk.put("url", "imin")
        Log.e("url", Hawk.get("url", "imin"))
        binding = DataBindingUtil.setContentView(this, R.layout.splash)
        viewmodel = ViewModelProviders.of(this).get(SplashViewModel::class.java)
        binding?.viewModel = viewmodel
        initObservables()

    }

    private fun initObservables() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        Util.hawk(application)
        if (Util.setupPermissionsLocation(this)) {
//            if (isLocationEnabled()) {
//                getLocation()
//            } else {
//                enableGPS()
//            }
            viewmodel?.NearLocation()
        } else {
            Util.makeRequestLocation(this)
        }

//        Handler()
//            .postDelayed({
//
//                val i = Intent(this, MainActivity::class.java)
//                startActivity(i)
//                //
//            }, SPLASH_DISPLAY_LENGTH.toLong())
//        viewmodel?.search()

//        val crashButton = Button(this)
//        crashButton.text = "Crash!"
//        crashButton.setOnClickListener {
//            throw RuntimeException("Test Crash") // Force a crash
//        }
//
//        addContentView(crashButton, ViewGroup.LayoutParams(
//            ViewGroup.LayoutParams.MATCH_PARENT,
//            ViewGroup.LayoutParams.WRAP_CONTENT))
        viewmodel?.launchMainActivity?.observeForever(Observer {
            if (it!!) {
                if (Hawk.get("LoginStatus", true)) {
                    Hawk.put("FirstLogin", false)
                    val i = Intent(this, MainActivity::class.java)
                    startActivity(i)
                }
            }
        })
//        if (viewmodel?.search()!!){
//            val i = Intent(this, MainActivity::class.java)
//            startActivity(i)
//        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            100 -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Log.i(ContentValues.TAG, "Permission has been denied by user")
                } else {
//                    isLocationEnabled()
                    viewmodel?. NearLocation()
                }
            }
        }
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    fun enableGPS() {


        val locationRequest = LocationRequest.create()
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        locationRequest.setInterval(10 * 1000.toLong()) // 10 seconds

        locationRequest.setFastestInterval(5 * 1000.toLong()) // 5 seconds


        GpsUtils(this).turnGPSOn(object : GpsUtils.onGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) { // turn on GPS
                isGPS = isGPSEnable
            }
        })

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        val wayLatitude = location.latitude
                        val wayLongitude = location.longitude
                        if (!isContinue) {
                            Toast.makeText(
                                application, wayLatitude.toString()
                                        + wayLongitude.toString(), Toast.LENGTH_SHORT
                            ).show()
//                           val txtLocation.setText(
//                                String.format(
//                                    Locale.US,
//                                    "%s - %s",
//                                    wayLatitude,
//                                    wayLongitude
//                                )
//                            )
                        } else {
//                            stringBuilder.append(wayLatitude)
//                            stringBuilder.append("-")
//                            stringBuilder.append(wayLongitude)
//                            stringBuilder.append("\n\n")
//                            txtContinueLocation.setText(stringBuilder.toString())
                        }
                        if (!isContinue && mFusedLocationClient != null) {
                            mFusedLocationClient?.removeLocationUpdates(locationCallback)
                        }
                    }
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1001) {
                isGPS = true // flag maintain before get location
//                mFusedLocationClient?.lastLocation?.addOnCompleteListener(this) { task ->
//                    var location: Location? = task.result
//                    location?.latitude
//
//                    viewmodel?.latitius?.value = location?.latitude.toString()
//                    viewmodel?.logitud?.value = location?.longitude.toString()
//                }
                getLocation()

            }
        }
    }

    fun getLocation() {
        mFusedLocationClient?.lastLocation?.addOnCompleteListener(this) { task ->
            var location: Location? = task.result
            location?.latitude

            viewmodel?.latitius?.value = location?.latitude.toString()
            viewmodel?.logitud?.value = location?.longitude.toString()
            Hawk.put("lat", location?.latitude.toString())
            Hawk.put("log", location?.longitude.toString())
            viewmodel?.NearLocation()
        }
    }

}