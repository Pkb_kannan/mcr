package com.mcr.active.model.picUload


import com.google.gson.annotations.SerializedName


data class PicUpload(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)