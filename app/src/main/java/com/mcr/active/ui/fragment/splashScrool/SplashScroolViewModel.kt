package com.mcr.active.ui.fragment.splashScrool

import android.annotation.SuppressLint
import android.app.Application
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.Navigation
import com.mcr.active.R
import com.mcr.active.Util.SingleLiveEvent
import com.mcr.active.ui.MainActivity
import com.mcr.active.ui.MainViewModel
import com.mcr.active.ui.fragment.home.bottomSheetFragment.favourit.FavouritBottomSheetFragmentScrool
import com.mcr.active.ui.fragment.home.bottomSheetFragment.location.LocationBottomSheetFragmentSplash
import com.mcr.active.ui.fragment.home.bottomSheetFragment.search.SearchBottomSheetFragmentScrool
import com.orhanobut.hawk.Hawk
import com.mcr.active.base.BaseViewModel

class SplashScroolViewModel(application: Application) : BaseViewModel(application) {

    var btnSelected: ObservableBoolean? = null
    var launchSinInActivity: SingleLiveEvent<Boolean>? = null
    var launchRegisterActivity: SingleLiveEvent<Boolean>? = null
    var launchHome: SingleLiveEvent<Boolean>? = null

    init {
        btnSelected = ObservableBoolean(false)
        launchSinInActivity = SingleLiveEvent<Boolean>()
        launchRegisterActivity = SingleLiveEvent<Boolean>()
        launchHome = SingleLiveEvent<Boolean>()
        MainViewModel(getApplication()).TopHide?.value = false
        MainViewModel(getApplication())?.ToolBar(false)
        loginstatus()
//        MainViewModel(getApplication())?.ToolBar(false)
//        MainActivity()?.binding?.appBarBind?.constrain?.setBackgroundColor(R.color.colorSplash)

//        if (Hawk.get("LoginStatus",false)) {
//            Navigation.findNavController(View(application)).navigate(SplashScroolDirections.actionNavSplashToNavHome())
//        }
    }

    fun loginstatus(){
        if (Hawk.get("LoginStatus",false)){
            launchHome?.value = true
        }
        else{
            launchHome?.value = false
        }
    }

    fun login() {
        launchSinInActivity?.value = true

    }
    fun register(){
        launchRegisterActivity?.value = true
    }
    fun  showLocationDialogFragment(fragmager : FragmentManager) {
        val bottomSheetFragment =
            LocationBottomSheetFragmentSplash()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }
    fun  showSearchDialogFragment(fragmager : FragmentManager) {
        val bottomSheetFragment =
            SearchBottomSheetFragmentScrool()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }
    fun  showFavouritDialogFragment(fragmager : FragmentManager) {
        val bottomSheetFragment =
            FavouritBottomSheetFragmentScrool()
        fragmager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }
}