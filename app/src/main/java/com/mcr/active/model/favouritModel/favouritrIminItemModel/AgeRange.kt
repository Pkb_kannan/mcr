package com.mcr.active.model.favouritModel.favouritrIminItemModel


import com.google.gson.annotations.SerializedName


data class AgeRange(

	@field:SerializedName("minValue")
	val minValue: Int? = null,

	@field:SerializedName("maxValue")
	val maxValue: Int? = null,

	@field:SerializedName("type")
	val type: String? = null
)