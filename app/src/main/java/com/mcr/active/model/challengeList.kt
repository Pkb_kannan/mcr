package com.mcr.active.model

import androidx.databinding.BaseObservable
import retrofit2.Callback

class challengeList : BaseObservable() {
    var title: String? = null
    var content: String? = null
    var image:Int?=null
    var seekVisible: Boolean? = null
    var redseek:Int?=null
    var yelloseek:Int?=null

    fun getTitlee(): String? {
        return title
    }

    fun setTitlee(titlee: String) {
        title = titlee
    }
    fun getConten(): String? {
        return content
    }
    fun setConten(contents: String) {
        content = contents
    }

    fun setimage(images: Int) {
        image = images
    }
    fun getimage(): Int? {
        return image
    }
    fun setseekVisible(seekVisibles: Boolean) {
        seekVisible = seekVisibles
    }
    fun getseekVisible(): Boolean? {
        return seekVisible
    }
    fun setredseek(redseekval: Int) {
        redseek = redseekval
    }
    fun getredseek(): Int? {
        return redseek
    }
    fun setyelloseek(yelloseekval: Int) {
        yelloseek = yelloseekval
    }
    fun getyelloseek(): Int? {
        return yelloseek
    }



}