package com.mcr.active.model.LocationDetailModel


import com.google.gson.annotations.SerializedName


data class SubEventItem(

	@field:SerializedName("offers")
	val offers: List<OffersItem?>? = null,

	@field:SerializedName("duration")
	val duration: String? = null,

//	@field:SerializedName("identifier")
//	val identifier: Int? = null,

	@field:SerializedName("subEvent")
	val subEvent: List<SubEventItem?>? = null,

	@field:SerializedName("location")
	val location: Location? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("category")
	val category: List<String?>? = null,

	@field:SerializedName("@context")
	val context: List<String?>? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("imin:checkoutUrlTemplate")
	val iminCheckoutUrlTemplate: String? = null,

	@field:SerializedName("beta:sportsActivityLocation")
	val betaSportsActivityLocation: List<BetaSportsActivityLocationItem?>? = null,

	@field:SerializedName("endDate")
	val endDate: String? = null,

	@field:SerializedName("maximumAttendeeCapacity")
	val maximumAttendeeCapacity: Int? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("startDate")
	val startDate: String? = null,

	@field:SerializedName("remainingAttendeeCapacity")
	val remainingAttendeeCapacity: Int? = null
)