package com.mcr.active.model.EventDetailsModel


import com.google.gson.annotations.SerializedName


data class LeaderName(

	@field:SerializedName("imin:item")
	val iminItem6: List<IminItem6Item?>? = null,

	@field:SerializedName("imin:totalItems")
	val iminTotalItems: Int? = null,

	@field:SerializedName("imin:source")
	val iminSource: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)