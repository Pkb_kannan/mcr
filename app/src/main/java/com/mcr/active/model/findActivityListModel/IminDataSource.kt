package com.mcr.active.model.findActivityListModel


import com.google.gson.annotations.SerializedName


data class IminDataSource(

	@field:SerializedName("identifier")
	val identifier: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)