package com.mcr.active.model.favouritModel.favLeisureModel


import com.google.gson.annotations.SerializedName


data class HoursAvailableItem(

	@field:SerializedName("dayOfWeek")
	val dayOfWeek: String? = null,

	@field:SerializedName("opens")
	val opens: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("closes")
	val closes: String? = null
)