package com.mcr.active.ui.fragment.splashScrool

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.mcr.active.R
import com.mcr.active.model.viewPagerModel.ViewPagerModel


class ViewPagerAdapter(private val context: Context, private val imageModelArrayList: ArrayList<ViewPagerModel>) : PagerAdapter() {
    private val inflater: LayoutInflater


    init {
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return imageModelArrayList.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.view_pager_item, view, false)!!

        val imageView = imageLayout.findViewById(R.id.img) as ImageView
        val text = imageLayout.findViewById(R.id.txt_head) as TextView

        text.setText(imageModelArrayList[position]?.getConten())
        imageView.setImageResource(imageModelArrayList[position]?.getimg()!!)

        view.addView(imageLayout, 0)

        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

}