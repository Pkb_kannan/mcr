package com.mcr.active.model.favouritModel.favouritrIminItemModel


import com.google.gson.annotations.SerializedName


data class SubEventItem1(

    @field:SerializedName("endDate")
	val endDate: String? = null,

    @field:SerializedName("startDate")
	val startDate: String? = null


)